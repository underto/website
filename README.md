[![pipeline status](https://gitlab.com/underto/website/badges/master/pipeline.svg)](https://gitlab.com/underto/website/commits/master)


# Underto website

Questo repository contiene i sorgenti del sito attuale dell'hacklab
underscore (https://autistici.org/underscore). 
Il sito e' statico ed e' gestito da [pelican](https://getpelican.com)
quindi per fare delle modifiche devi installarlo in locale assieme al repo dei 
plugin come descritto di seguito

## Installazione
```

# Installare python e pelican
apt install python3 
pip install pelican==3.7.1

# clonare questo repo
git clone git@gitlab.com:underto/website.git

# entrare nella dir e clonare pelican-plugins
cd website/
git clone https://github.com/getpelican/pelican-plugins
```

## Vedere il sito in locale prima di pubblicarlo
Tutte le cose di fanno con `make`

- per far partire un webserver locale:

`make devserver`

- per interromperlo 

`make stopserver`

## Per far funzionare i link e la grafica

per funzionare in locale e' necessario ricordarsi di modificare il file
`pelicanconf.py` come di seguito:

`RELATIVE_URLS = True`


## Modificare il tema

Tutte le cose del tema si trovano dentro `/themes/`
il tema usato e':

`THEME = 'themes/underscore'`

## Creare un post:

per creare un nuovo post e' possibile usare make 

`make newpost NAME='titolo'`

una volta creato il nuovo post e' possibile modificarlo in:

`website/content/posts/titolo-ciao.md`

usare la sintassi markdown e salvare per applicare le modifica in locale

## Pubblicare

una volta soddisfatti di come appare in locale e' possibile effettuare un commit
per salvare il lavoro fatto e quindi fare un git push per pubblicare le modifiche
che verranno inviate in automagico su a/i (vedi `.gitlab-ci.yml`)

```
git commit -m 'nuovo articolo'
git push
```

```

*NB; chiedete le password in lista nel caso non abbiate le credenziali*


```
