Title: Progetti
Date: 2019-03-08 10:20
Modified: 2016-03-12 19:30
Slug: progetti
Authors: underto
Summary: Progetti


## [Cisti](https://cisti.org)
Un server autogestito torinese, per la riappropriazione degli strumenti digitali.

>
> ## [Gancio](https://gancio.cisti.org)
>
>  Un'agenda condivisa dei movimenti tra Torino e dintorni.

## [Manuale autodifesa digitale](https://facciamo.cisti.org)
Ci capita spesso di parlare di autodifesa digitale, di paranoia e
sicurezza, abbiamo scritto in proposito queste slides.

>
> ## [orjail](https://orjail.github.io/)
> Crediamo fortemente che la privacy e l'anonimato siano diritti da mettere
> in pratica, per questo abbiamo sviluppato per noi e per i più smanettoni orjail.

