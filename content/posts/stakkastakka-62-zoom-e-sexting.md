Title: Stakkastakka #62 - Zoom e Sexting
Slug: zoom-e-sexting
Date: 2020-04-01 13:11:30
Tags: stakkastakka

Ascolta il podcast dell'intera puntata o scegli qua sotto l'approfondimento
che preferisci da condividere con chi vuoi!

<audio controls>
<source src='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-62.mp3' type='audio/mp3'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-62.mp3'/>Puntata completa</a>



## Sexting

Ormoni al silicio al tempo di pandemia. Approfondimento su come mandare i
sessaggini dalla quarantena con gli amati consigli di Zia Valentine e Dj
Sbrok.

<audio controls>
<source src='https://radioblackout.org/wp-content/uploads/2020/03/stakkastakka-sexting.mp3' type='audio/mp3'>
</audio>


## Posta del ♥ dell'Internet

### Zoom e alternativa

Zoom, un applicazione per video-conferenze che sta facendo molto parlare di
se in questo periodo. Parliamo delle ombre che stanno rovinando la corsa
alla ribalta di questo nuovo unicorno della Silicon Valley.

- [Zoom iOS App Sends Data to Facebook Even if You Don’t Have a Facebook Account](https://www.vice.com/en_us/article/k7e599/zoom-ios-app-sends-data-to-facebook-even-if-you-dont-have-a-facebook-account)
- [La chat Zoom condivide i dati con Facebook (anche se non abbiamo un profilo)](https://www.corriere.it/tecnologia/20_marzo_27/chat-zoom-condivide-dati-facebook-anche-se-non-abbiamo-profilo-2b1eb70a-700c-11ea-82c1-be2d421e9f6b.shtml)

Ma citiamo molte alternative tra cui [il pad per la scrittura collaborativa su cisti.org](pad.cisti.org/) e [video-conferenze su autistici.org](https://vc.autistici.org/). Ma la prossima settimana avremo una novità made with ♥ nell'hacklab underscore.

<audio controls>
<source src='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-62-zoom.mp3' type='audio/mp3'>
</audio>


### Hackingteam

Dopo anni torniamo a parlare di hackingteam e delle spoglie che rimangono
dell'azienda di malware milanesi. Una storia di [lotta tra
miserabili](https://www.vice.com/en_us/article/xgq3qd/memento-labs-the-reborn-hacking-team-is-struggling).

<audio controls>
<source src='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-62-hackingteam.mp3' type='audio/mp3'>
</audio>


### PacoLeak

Nome in codice: Piñera conchetumare

L'esercito cileno viene colpito da una serie di fughe di notizie che ne
mostrano i crimini.

La prima il 09 settembre 2019:

- [art](https://www.fayerwayer.com/2019/10/pacoleaks/)
- [info](https://data.ddosecrets.com/file/PacoLeaks/informativo.txt)
- [leak](https://pacoleaks.rebelside.pw/)

E l'ultima proprio in questi giorni:

- [art](https://ciperchile.cl/2019/12/15/hackeo-al-ejercito-viajes-de-oviedo-acuerdo-de-inteligencia-con-ee-uu-y-nexos-con-empresarios/)
- [tech](https://www.vice.com/en_us/article/wxeykb/phineas-fisher-says-they-paid-dollar10000-bounty-to-person-who-hacked-chilean-military)
- [leak](https://hunter.ddosecrets.com/datasets/65)

<audio controls>
<source src='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-62-cileleaks.mp3' type='audio/mp3'>
</audio>

