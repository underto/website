Title: presentazioni
Slug: presentazioni
Date: 2019-10-26 00:44:17

Nei prossimi mesi andremo in giro nei peggiori posti di torino e dintorni a
fare un po' di chiacchiere sui progetti che stiamo portando avanti, sui ragionamenti che ci stiamo
facendo nell'ultimo periodo e soprattutto per raccogliere feedback, critiche e suggerimenti.


<br/><br/>

## Facciamo cisti! @ Asti
Domenica 27 Ottobre saremo a L.A. Miccia di Asti dalle 17 in poi
parleremo di social network e presenteremo [cisti.org](https://cisti.org)
<iframe src='https://gancio.cisti.org/embed/468' class='embedded_gancio'></iframe>
<style>
.embedded_gancio {
  border: none;
  width: 450px;
  height: 220px;
}</style>
<br/>
## 27 anni di Barocchio Squat!
Per il compleanno del Barocchio tra una birretta e un'altra
faremo una chiacchiera meno strutturata e piu' aperta a chiarire dubbi e domande.
<iframe src='https://gancio.cisti.org/embed/528' class='embedded_gancio'></iframe>
<br/>
## Laboratori Aperti @ Gabrio!
Siete tutti invitati ai festeggiamenti per la riapertura dei laboratori del Gabrio, con la new entry Pole Dance Ribelle!

Vogliamo farvi conoscere i collettivi nati in questi 6+ anni di occupazione di via Millio, condividendo con voi il frutto delle nostre gioie e fatiche, con esposizioni, attività aperte e dimostrazioni pratiche.
<iframe src='https://gancio.cisti.org/embed/514' class='embedded_gancio'></iframe>

## Cisti.org
E questa invece la prepariamo per bene quindi non potete mancare il 17
Novembre dalle 17.30 al Gabrio!
<iframe src='https://gancio.cisti.org/embed/529' class='embedded_gancio'></iframe>


## Internet, mon amour
Il 18 dicembre saremo invece a radio blackout per un imperdibile
radiospettacolo per presentare "Internet, mon amour" scritto da Agnese
Trocchi di [circe](https://circex.org)
<iframe src='https://gancio.cisti.org/embed/530' class='embedded_gancio'></iframe>



