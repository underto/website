Title: stakkastakka #36 - I gabbiani jaunes
Slug: stakkastakka-36-i-gabbiani-jaunes
Date: 2019-07-08 17:23:10
Tags: stakkastakka


#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/07/stakkastakka-36-gabbiani-gialli.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/07/stakkastakka-36-gabbiani-gialli.ogg'/>Puntata completa</a>

Purtroppo gli ultimi minuti della puntata non sono stati registrati a causa
di un surriscaldamento dei nostri turbo-sistemi!

# Posta del cuore dell'Internet

- [Gabbiani contro polizia](https://www.tio.ch/dal-mondo/attualita/1377629/droni-della-polizia-attaccati-dai-gabbiani)
- [Could a neuroscientist understand a microprocessor?](https://www.biorxiv.org/content/10.1101/055624v1)


