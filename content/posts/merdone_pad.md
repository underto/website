Title: il pad e' qui, il pad e' li', te guardi laggiu' e il pad non c'e' piu'
Slug: 
Date: 2022-02-18 12:36:33


Beh dopo 3 anni senza intoppi abbiamo finalmente fatto una cazzata col pad \o/  

Lunedi' mattina abbiamo cancellato per errore gran parte dei pad: aggiornando il plugin che rende possibile l'eliminazione degli stessi dopo 6 mesi di inattivita', questo ha deciso di cambiare le sue impostazioni di default e usare un solo giorno di inattivita' come condizione per l'eliminazione.  

Dopo numerose bestemmie abbiamo fatto un restore dal backup notturno ma su alcuni pad qualcosa non ha funzionato come previsto: se il vostro pad e' uno dei pochi sfortunati potete fare in due modi:  



**modo 1: contenuto statico dell'ultima versione del pad**  
cercate il vostro pad al suo indirizzo ma sostituendo `/p` con `/o`, quindi se il nome del pad era `pippo` e il link dello stesso `https://pad.cisti.org/p/pippo` ora ne troverete una versione statica al link `https://pad.cisti.org/o/pippo`

**modo 2: valido per ogni stagione**  
ci scrivete all'indirizzo info@cisti.org

