Title: stakkastakka #15 Asilo
Slug: stakkastakka-15-asilo
Date: 2019-02-18 17:16:27
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/02/stakkastakka-15-asilo.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/02/stakkastakka-15-asilo.ogg'/>Puntata completa</a>


#### Posta del cuore dell'INTERNET

- [amazon non costruirà una nuova sede nel queens](https://www.nytimes.com/2019/02/14/nyregion/amazon-hq2-queens.html)

#### Diretta mutuo soccorso

Per ulteriori informazioni sull'associazione di mutuo soccorso si può fare
riferimento al loro [sito](https://mutuosoccorso.noblogs.org/).


#### Approfondimento

Nell'approfondimento di questa puntata abbiamo spiegato alcuni dei consigli
riportati nel [nostro comunicato](https://www.autistici.org/underscore/asilo.html) in solidarietà all'Asilo Occupato.

