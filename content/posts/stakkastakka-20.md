Title: stakkastakka #20
Slug: stakkastakka-20
Date: 2019-03-11 17:16:00
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/03/stakkastakka-20.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/03/stakkastakka-20.ogg'/>Puntata completa</a>


#### Posta del cuore dell'INTERNET

- [Firefox aggiungere strumenti anti fingerprinting](https://www.zdnet.com/article/firefox-to-add-tor-browser-anti-fingerprinting-technique-called-letterboxing/)
- [Tutte le startup di AI sono fuffa](https://www.forbes.com/sites/parmyolson/2019/03/04/nearly-half-of-all-ai-startups-are-cashing-in-on-hype/)
- L'NSA [chiude il programma per la registrazione delle telefonata](https://www.nytimes.com/2019/03/04/us/politics/nsa-phone-records-program-shut-down.html)
  forse perché [non gli serve più](https://twitter.com/RidT/status/1102930458139131906)
- [Venezuela netblock](https://netblocks.org/reports/venezuela-knocked-offline-amid-nationwide-power-outage-PW801YAK)
- Diretta con la scuola di musica del gabrio sul perché dobbiamo
  intervenire a [salvare l'internet](https://saveyourinternet.eu/)
- [Trento e cimici](https://roundrobin.info/2019/03/trento-sei-microspie-e-una-telecamera-immagini-pesanti/)
