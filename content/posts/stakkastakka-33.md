Title: stakkastakka #33
Slug: stakkastakka-33
Date: 2019-06-10 17:20:10
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/06/stakkastakka-33.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/06/stakkastakka-33.ogg'/>Puntata completa</a>

# Di Software bug

- [Boeing 737](https://spectrum.ieee.org/aerospace/aviation/how-the-boeing-737-max-disaster-looks-to-a-software-developer)
- [La sconfitta di Kasparov](https://www.cnet.com/news/did-a-bug-in-deep-blue-lead-to-kasparovs-defeat/)

