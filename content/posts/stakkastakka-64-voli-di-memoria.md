Title: Stakkastakka #64 - Voli di Memoria
Slug: voli-di-memoria
Date: 2020-04-15 12:28:41
Tags: stakkastakka

<audio controls>
<source src='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-64.mp3' type='audio/mp3'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-64.mp3'/>Puntata completa</a>

## Approfondimento malware

<audio controls>
<source src='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-64-malware.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-64-malware.ogg'/>Malware</a>

Super approfondimento tecnico sul mercato degli expoit e sistemi difensivi.
Cosa può mettere in difficoltà le aziende più ferrate nella produzione di
malware? Scopriamolo con un contributo di pregio.

## Bug Boeing 787

<audio controls>
<source src='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-64-volare-trallala.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-64-volare-trallala.ogg'/>Bug Boeing 787</a>

[Nuovo bug per il boeing
787](https://www.theregister.co.uk/2020/04/02/boeing_787_power_cycle_51_days_stale_data/)
Compriamo la notizia direttamente dalle piste.

## Abemus mumble

<audio controls>
<source src='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-64-farma-cisti.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-64-farma-cisti.ogg'/>Abemus Mumble</a>

Un nuovo attrezzo per cisti.org In queste ultime settimane in molte e molti
ci avete chiesto delle dritte per comunicare a distanza cercando di non
usare strumenti che controllassero anche il colore delle vostre mutande.
Abbiamo passato un po' di tempo a sperimentare varie soluzioni, abbiamo
mannaggiato tanto su jitsi meet, la piattaforma per videoconferenze messa a
disposizione da molti collettivi in questo periodo (tra cui citiamo
vc.autistici.org, ma anche disroot), ma non ci ha convinto perche' soffre
di alcune problematiche che ne pregiudicano l'uso in piu' di 5/6 persone
(ne abbiamo comunque scritto una guida [qua](https://cisti.org/docs/jitsi/intro/))

Ma alla fine non la vogliamo alzare un po' questa asticella? Ci siamo resi
conto che non tutte abbiamo la fibra a casa e che ci piacerebbe poter
parlare dalla nostra scassata rete di campagna o dalla rete autogestita che
verra'. Ma la nostra lista dei desideri continua: un numero di telefono per
collegare le "vecchie" linee alle "nuove" così da non lasciare fuori dalle
nostre assemblee chi l'accesso ad internet non ce l'ha o magari non ci
tiene ad averlo.

Insomma non ci vogliamo accontentare di un cosetto che fa più o meno quello
che ci si aspetta, vogliamo di più! Ci siamo quindi buttati alla riscoperta
di mumble, un po' oldschool ma molto versatile e che con affetto e coccole
siamo riusciti a far funzionare con tutte le richieste che avevamo (per
ora...).

Ci troviamo a consigliare mumble tra tutte le soluzioni perchè è il
compromesso migliore per avere un aggeggio autogestito che copre un sacco
di utilizzi particolari. È vero, è un po' macchinoso da imparare, per
questo ci abbiamo anche scritto una guida sopra, ma pensiamo che in questo
periodo dovremmo incominciare a prendere decisioni lungimiranti.

Insomma nasce farma.cisti.org, il nostro balsamo contro l'isolamento, ma
non ci andate via web se non proprio necessario, usate invece uno dei
programmi suggeriti nella  documentazione scritta con grande fatica e che
potete leggere [qua](https://cisti.org/docs/mumble/intro/).

Ricordiamo anche altri due servizi che in questo periodo risultano presenze
utili nel coltellino svizzero di cisti.org: l'usatissimo pad per la
scrittura di testi collaborativi che potete trovare su
https://pad.cisti.org e il nostro nodo mastodon che si sta rivelando un
buon posto per comunicare al di fuori della paura e della rincorsa alla
notizia.
