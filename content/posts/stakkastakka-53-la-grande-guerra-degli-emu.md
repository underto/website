Title: stakkastakka #53 - La Grande Guerra degli emu
Slug: stakkastakka-53-la-grande-guerra-degli-emu
Date: 2020-01-22 15:28:17
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka-53.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka-53.ogg'/>Puntata completa</a>
<br/>

## La Grande Guerra degli Emu

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka-53-bits-guerra-emu.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka-53-bits-guerra-emu.ogg'/>Approfondimento emu</a>
<br/>

[How we lost the great emu war](http://www.emugigs.com/emuwar/) grande
storia coperta da [wikipedia](https://en.wikipedia.org/wiki/Emu_War).


## My Stepdad’s Huge Data Set

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka-53-bits-data-driven-porn.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka-53-bits-data-driven-porn.ogg'/>Approfondimento data-driven porn</a>
<br/>

A partire da un articolo di Gustavo Turnar su
[LogicMag](https://logicmag.io/play/my-stepdad's-huge-data-set/).

