Title: zapruder hack the system
Slug: zapruder-hack-the-system
Date: 2018-11-15 00:07:14
Tags: gabrio, zapruder, hack the system, eventi

![zapruder hack the sytem](images/zapruder-hack-the-system.jpg)

Lunedì 19 Novembre alle ore 21.00~ presso il [CSOA
Gabrio](https://gabrio.noblogs.org/) in via Millio 42.

> Se non potete essere presenti con noi alla serata sintonizzatevi su
> *#stakkastakka* sulle libere frequenze di radio
> [blackout](https://radioblackout.org/) tra le 13.00 e le 15.00 per una
> diretta con i redattori.

La storia dell’hacking come lo conosciamo oggi ha origine negli anni
ottanta e nei fenomeni controculturali che li attraversarono: di questa
esperienza abbiamo tentato in questo numero di offrire una delle prime
forme di narrazione corale disponibili nel contesto italiano (e non).

Partiti con l’intenzione di affrontare la storia dell’hacking in una
prospettiva globale, è diventata per noi dominante l’idea di affrontare la
nascita e l’evoluzione dell’hacktivism, cioè del nesso delle pratiche
hacker con l’attivismo e la militanza politica. Questo perché abbiamo
preferito concentrarci su un percorso profondamente politico e peculiare
del contesto italiano: nonostante la consapevolezza della non neutralità
della tecnica, i movimenti italiani infatti riflettono da almeno tre
decenni sull’uso sociale delle tecnologie digitali, dimostrando un
particolare interesse per i nuovi media e per il loro uso ai fini di una
comunicazione e una (contro)informazione libere e indipendenti. Nella
costruzione del numero abbiamo seguito le reti dei protagonisti
dell’hacking italiano e ci siamo avvicinati a coloro che, prima di noi, si
erano interessati di queste storie. La linea di demarcazione tra gli uni e
gli altri si è rivelata labile: per questo ci è impossibile distinguere con
certezza quanto ci sia di emico (proveniente dall’interno) e quanto di
etico (frutto di interpretazione) in questo numero.

