Title: About Government Malware
Date: 2018-01-18
Category: hacking
Tags: ht, malware, hacking team, galileo, rcs
Slug: about-gov-malware
Authors: underto
gallery: {photo}ht, {photo}ht2, {photo}ht3, {photo}ht4

<iframe src="https://player.vimeo.com/video/214338653" height="583" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>  

<br>

this scripts is long, confused and full of notes, links and questions, if you
have the coffee on, it is better to stop it.  This work was hard and we hope you
like it.

We are talking about malware, offensive security and the attempt to legalize
this institutional malware.

### Shall we to start?

As we like to touch with our hands the stuff about what we talk about, we got
one of this "malware" and we start do create a documentation about the setup of
this. For everybody who wants to get hands dirty with us.

Then we get infected voluntarily using a device with his malware in order to
study it closer, understanding how it works and which bugs may have.

Last, as many and many people are trying to create some rule and law about this
very dangerous stuff we need __strong guarantees in order to not accused of some
false data gathered from our devices__<sup>[1]</sup>, we have tried to generate
some fake proof in order to understand how reliable are this gov-malware.

The conclusion we got is that there is *no way to be sure technically that the
evidence are real*

We documented all the steps in a [detail post](di-trojan-di-stato-details.html), here
instead, we want to analyze the ongoing proposal to legalize this malwares.

### They're into my pc

the concrete threat about the abuse of this very powerful tool move us to bring
some lights on this foggy situation.

We are not law maker, but seems the malware in italy are already regulated. The
crime is well now for hacker like us, 615-ter C.P. aka "Abusive Access into a
System device" which shows also a specific part if the crime is done by an
official.

But, the fact this tools are daily used from the police as investigation tool
and nobody is showing any problem about that, is the demonstration about how a
law is a perfect repressive tools, but is not working very well as justice and
social tool. Which Police station will investigate about their own investigation
tools ?

What we are trying to do for the future is to try to avoid that our (and yours)
devices will spy us.  Then, We would like spread some good security pratice in
order to avoid to get infected from this and other trojans: we will surely need
help because we know that won't be not exactly easy.

### Regulating armed tanks: ddl malware

how do you explain the use of a tank ? We just need to talk about terrorism,
about pedoporn? We just need to talk shit about some politician? Or maybe act
against some big national operation ? We feel absolutely NOT protected from the
italian law to regulate malware written and showed by "Civici Innovatori"
(law:Quintarelli) which show a lot of issues; we just stopped a second reading
tecnical operative law proposal<sup>[2]</sup> how to rule and limit this and we
discover they are trying to create similitudes with the malware functionality
and the some regulated police pratice, like: tag after someone, intercept
location data or real confiscation of the data into a device.

Trying to create similitudes between classic pratice and malware features it's
absurd from every point of view and for every kind of justice act you may
consider:

- compare a confiscation to a remote file acquisition of a device is ridiculous.
  During a confiscation there's the presence of the person investigated which
  can verify what is happening, can request the presence of a lawyer and at the
  end should receive a list of the confiscated material.  We can't imagine how
  this can be possible during a remote confiscation using a malware.

- a real confiscation aim to cut off the availability of a specific tool, like
  a gun. The malware doesn't cut off this availability, because is not made for
  this.

- Compare the GPS tracking of a real tagging is ridiculous. Get the digital data
  and easy to be analyzed from invasive algorithm is not comparable to a real
  shadowing, the quality of the data is completely another level.

- But most of all there's a quantitative question about costs: do a shadowing
  from the police desk to 1000 people doesn't have the same cost to do it for
  real, it's clear that if yesterday doing a shadowing was a matter of time and
  money, but now, with a malware all you need is just to focus clic and press a
  button. The consequences of this is quite obvious. Just trying to imagine that
  this two operations are the same is quite ridiculous.

- Then we start from the idea that a device is owned by the person investigated,
  but this is not always true. For example all the "public" device, the internet
  point, the libraries, the university. Reading the email from an internet point
  tapped where a person under surveillance used the pc before of us, bring us a
  risk because the pc is bugged (so is an infected pc) our email will go to the
  police. Even if we with a used device, our email will be delivered to the
  police.

- Then there is a temporal problem. With a classical mobile interception, the
  SMS are grabbed starting from a specific day and the interception has a time
  limit for legal reason. The trojan is another story, it can read the data with
  no time limit, sending all the conversation on the device from the beginning of
  it's history, because it is all saved there.

- During a shadowing there's the guarantee (except to hire a lookalike) that the
  position is the effective location of the investigated person. But the device
  may not travel with the person, it can be stolen or forgotten in a taxi. So
  basically the device owned by the person is not the person.

- To believe as we read in the document, that the installation of a malware
  doesn't lower the security level where is installed is ridiculous, the base idea of
  the malware is exactly to let open some vulnerability in order to use it and
  doing so keep insecure the device of everybody in order to infect them.

- To certify the impossibility to alterate the evidence seems impossible to
  obtain and later we prove it in two different way. But there's more. How is
  possible to certify the producer is not compromised? How is possible to verify
  that behind the massive architecture there's not a backdoor for someone else
  being able to use it? No, there's no way to verify it.

- Lastly, seems a paradox for us that the government want to use digital weapons
  bought from a shadow market and very less transparent as the zero-day market


### Homeworks

during our experiment we notice that the input of this 'objects' are considered
trusted (not modified by the user), which is clearly erroneous. What may happen
if a skype username is AAAA' DROP ALL TABLES--? and if it's length is 10million
chars? And what if instead of an image we put something different and the
malware breaks ? (yes it broke very badly).
How the law will consider this social behaviour? self-defense? evidence
occultation?

### What we want

For us this 'objects' can't be regulated. For us there's a danger hidden into
the secret action of the government over the citizen and this danger is way more
unsafe than any other threats, fullstop.

We want to know all, not just the stats about how many malware are sold or
exported (as recently requested by Hermes to the italian government), but we
want to know specifically how exactly are used these new surveillance technique, like IMSI-Catcher or
Government Malware  and how many of them are used

If someone want to tell us, fell free to write us an email:

> Underscore _TO* Hacklab // underscore chiocciola autistici.org  
> Key fingerprint = 5DAC 477D 5441 B7A1 5ACB  F680 BBEB 4DD3 9AC6 CCA9  
> gpg2 --recv-keys 0x9AC6CCA9  
>  

[1]: http://www.civicieinnovatori.it/wp-content/uploads/2017/02/Motivazionie-Contenuti-del-Disciplinare-Tecnico.pdf
[2]: http://www.civicieinnovatori.it/wp-content/uploads/2017/02/DisciplinareTecnicoPropostadiLeggeCaptatore.pdf
