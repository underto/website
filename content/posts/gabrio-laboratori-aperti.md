Title: Gabrio laboratori aperti
Slug: gabrio-laboratori-aperti
Date: 2018-11-15 12:51:11
Tags: gabrio, eventi

![gabrio laboratori aperti](images/gabrio-laboratori-aperti.jpg)

> Siete tutti invitati ai festeggiamenti per la riapertura dei laboratori del
> Gabrio!
> 
> Vogliamo farvi conoscere i collettivi nati in questi 5 anni di occupazione
> di via Millio, condividendo con voi il frutto delle nostre gioie e fatiche,
> con esposizioni, attività aperte e dimostrazioni pratiche.
> 
> Vi aspettiamo al secondo piano con il bar e un buffet offerto dai vari
> collettivi, il tutto condito con Dj set a cura di Black Dynamite Trio, e
> dalle 17.30 concerti con Vandisky, Yuma e Muddy Mama Davis.
> 
> Collettivo Orto
> Gila arti grafiche
> Gabrio school of music
> Hacklab
> Cinespritz
> Microclinica

Dentro e fuori dalle stanze del nostro bellissimo hacklab metteremo a
disposizione le nostre menti migliori per chiacchere e divertimento.

# 10 minutes

Cosa sono? Delle piccole presentazioni di dieci minuti su un argomento a
scelta. Tutti potranno segnarsi e prendere uno spazio per raccontare un
progetto su cui stanno lavorando o un idea o quello che vi pare.
Abbiamo già qualche asso nella manica!

# > 10 minutes

Avremo una presentazione su mastodon e social network che potrebbe durare
un po' più di dieci minuti.
