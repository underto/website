Title: stakkastakka #4 - Sicurezza telefoni
Slug: sicurezza-telefoni
Date: 2018-11-15 13:51:11
Tags: stakkastakka, telefoni

#### POSTCAST
<audio controls>
  <source src='https://stakkastakka.fugadalcontrollo.org/stakkastakka-4-smartphone.ogg' type='audio/mpeg'>
</audio>
<a href='https://stakkastakka.fugadalcontrollo.org/stakkastakka-4-smartphone.ogg'>Approfondimento telefoni</a>

#### Posta del cuore dell'internet

- [Guarda un telegiornale cinese condotto da una intelligenza artificiale](https://motherboard.vice.com/it/article/gy7bqq/guarda-un-telegiornale-cinese-condotto-da-una-intelligenza-artificiale)
- [Iran accusa Israele per un cyberattacco](https://arstechnica.com/information-technology/2018/11/iran-accuses-israel-of-cyber-attacks-including-new-stuxnet/)
- [La polizia danese decifra Ironchat](https://arstechnica.com/information-technology/2018/11/police-decrypt-258000-messages-after-breaking-pricey-ironchat-crypto-app/)

#### APPROFONDIMENTO

Super approfondimento sulla sicurezza dei telefonini!

- [Here's What TfL Learned From Tracking Your Phone On the Tube](http://www.gizmodo.co.uk/2017/02/heres-what-tfl-learned-from-tracking-your-phone-on-the-tube/)




