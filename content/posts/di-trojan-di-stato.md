Title: Di trojan di stato
Date: 2017-04-20
Category: hacking
Tags: ht, captatori informatici, hacking team, galileo, rcs, trojan di stato
Slug: di-trojan-di-stato
Authors: underto

questo scritto è lungo, confuso e denso di appunti, link e domande, se avete il caffè sul fuoco,
toglietelo. è stato un lavoro sudato, speriamo vi piaccia.

parliamo di captatori informatici, sicurezza offensiva e il tentativo di normare 
questi trojan di stato.


### per iniziare

siccome ci piace toccare con mano gli oggetti dei nostri ragionamenti, ci siamo
procurati uno di questi "captatori informatici" e ne abbiamo documentato il setup per chi volesse
sporcarsi le mani con noi.  

successivamente abbiamo volontariamente infettato un dispositivo con il malware per vederne il 
funzionamento da vicino, quali funzionalità presenta e quali difetti.

in ultimo, dal momento che nel tentativo di normare questi pericolosi strumenti leggiamo della
"_[necessità di forti garanzie per essere inattacabile sul piano della veridicità dei dati acquisiti](http://www.civicieinnovatori.it/wp-content/uploads/2017/02/Motivazionie-Contenuti-del-Disciplinare-Tecnico.pdf)_",  abbiamo provato a generare delle prove non valide per capire quanto sono affidabili questi captatori.

la conclusione a cui siamo giunti è che **non esiste modo di garantire tecnicamente che le prove
raccolte siano veritiere**.

abbiamo documentato tutto in questo [dettagliato post](di-trojan-di-stato-details.html),
qui invece ci proponiamo di fare un'analisi alle proposte di normare questi captatori informatici.


### mi sono entrati nel computer

la concreta minaccia dell'abuso di questo potente strumento ci muove a far luce su questa fumosa
questione.

non siamo giuristi ma ci sembra che i captatori informatici siano già normati, il reato lo 
conosciamo bene noi acari, il 615-ter c.p. "accesso abusivo ad un sistema informatico" che 
presenta anche un comma specifico se il reato è commesso da un pubblico ufficiale.
il fatto che questi strumenti siano, però, quotidianamente usati dalle forze di polizia come
strumento di indagine e che nessuno stia storcendo il naso per questo, è la dimostrazione di come
la legge sia un'ottimo strumento repressivo ma non funzioni un granchè come strumento di giustizia
sociale. quale procura potrebbe indagare sui suoi stessi strumenti di indagine?

quello che ci proponiamo di fare per il futuro è cercare di evitare che i nostri (e i vostri) 
dispositivi ci spiino.
vogliamo inoltre diffondere delle buone pratiche di sicurezza per evitare di farsi infettare da 
questi e altri trojan: avremo sicuramente bisogno di aiuto perchè sappiamo che non sarà affatto
semplice.


### regolamentare carri armati: ddl captatori

come si giustifica l'uso di un carro armato? basta parlare di terrorismo, di pedofilia? 
è sufficiente parlare male di un parlamentare? o magari opporsi alla costruzione di una 
grande opera? non ci sentiamo per niente tutelati dal decreto di legge presentato dai 
civici e innovatori (ddl quintarelli) dalla quale emergono moltissime criticità;
ci siamo soffermati sulla "[disciplinare tecnico operativa](http://www.civicieinnovatori.it/wp-content/uploads/2017/02/DisciplinareTecnicoPropostadiLeggeCaptatore.pdf)"

la tesi di fondo del documento è quella di fare un paragone tra le funzionalità del captatore e le
corrispondenti azioni di polizia giudiziaria già attualmente normate, per esempio paragonando 
pedinamento reale e intercettazione dati di posizionamento, o sequestro reale con acquisizione
file da dispositivo.

l'equiparazione tra le modalità classiche e quelle implementate dal captatore ci sembra assurdo da 
ogni punto di vista e per qualunque tipologia di azione giudiziaria considerata:

- paragonare un sequestro all'acquisizione remota di file sul dispositivo è ridicolo. durante un
sequestro c'è la presenza dell'indagato che può verificare quello che accade, può richiedere la
presenza di un legale e gli viene rilasciata una lista del materiale sequestrato. non riusciamo ad
immaginarci come questo sia possibile tramite un sequestro da remoto tramite captatore.

- il sequestro reale è teso a togliere la disponibilità di un oggetto, una pistola ad esempio.
il captatore non toglie la disponibilità dell'oggetto del sequestro, perchè non è quello il suo
scopo.

- paragonare il tracciamento gps ad un pedinamento è ridicolo.
avere i dati digitalizzati e facilmente analizzabili da algoritmi invasivi non è paragonabile
ad un pedinamento, la qualità del dato è proprio di un altro livello.

- ma soprattutto c'è una questione quantitativa e di costi: fare un pedinamento dalle scrivanie
della questura verso 1000 persone non ha lo stesso costo che farlo per davvero, è evidente che se
fino a ieri pedinare gli indagati richiedeva un certo dispiegamento di forze e denaro, con
l'utilizzo di un captatore il tutto si riduce a premere qualche tasto su un'interfaccia punta e
clicca. la conseguenza di questo è abbastanza ovvia. immaginare l'equivalenza delle due situazioni
è ridicolo.

- si parte poi dal presupposto che un dispositivo sia di un indagato, mentre non è sempre vero. 
prendiamo come esempio tutti i dispositivi "pubblici", gli internet point, le biblioteche, le università. leggendo le mail da un internet point usato da un indagato
prima di noi (e quindi su un computer infetto) le stesse finirebbero in questura. comprando un
dispositivo usato, le nostre mail finirebbero in questura.

- c'è inoltre un problema temporale. all'avvio di un'intercettazione telefonica classica, gli sms
intercettati partono appunto da un giorno e l'intercettazione ha poi un limite di tempo per ovvi
motivi. il trojan invece può leggere i dati senza limiti temporali, inviando tutte le conversazioni
avvenute sul tuo dispositivo dall'inizio dei tempi (perchè sono tutte salvate lì).

- durante un pedinamento c'è la garanzia (a meno di assumere un sosia?) che la posizione sia quella
effettiva dell'indagato. il dispositivo invece potrebbe non viaggiare con l'indagato, potrebbe essere stato
rubato o potrebbe essere stato lasciato sul sedile di un taxi. insomma il dispositivo dell'indagato
non è l'indagato.

- credere, come si legge nel documento, che l'installazione di un captatore non abbassi il livello
di sicurezza dei dispositivi su cui è installato è ridicolo, l'idea del captatore si basa proprio
sul fatto di lasciare aperte delle falle di sicurezza e quindi di mantenere appositamente insicuri i
dispositivi di tutti, per avere la possibilità di poterli infettare.

- certificare la non modificabilità delle prove acquisite ci sembra impossibile da ottenere e
l'abbiamo provato in due modi diversi. inoltre come è possibile certificare che il produttore non
sia stato compromesso? come è possibile certificare che nella imponente architettura non ci sia una
backdoor? no, non è possibile farlo.

- e ancora, ci sembra paradossale che lo stato utilizzi armi digitali acquistate su un mercato 
opaco e poco trasparente come quello degli zero day.


### compiti a casa

durante gli esperimenti abbiamo notato che gli input di questi oggetti vengono considerati trusted
(cioè non modificabili dall'utente), cosa ovviamente non vera. cosa succede se un contatto skype si
chiama AAAA' DROP ALL TABLES--? e se è lungo 10mila caratteri? e se al posto di un'immagine
mettiamo qualcosa di altro e il captatore si rompe? (si, si rompe male). 
come la legge vedrebbe un comportamento simile? legittima difesa? occultamento di prove?


### cosa vogliamo

per noi questi oggetti non sono normabili. per noi c'è un pericolo insito nell'azione segreta
di una parte dell'apparato dello stato sul cittadino e questo pericolo sovrasta ogni altro
pericolo.   
punto.

vogliamo sapere non solo le statistiche di esportazione di questi strumenti (come richiesto di
recente dal centro hermes al ministero dello sviluppo economico), ma soprattutto come e quanto
vengono utilizzate in italia oggi queste tecnologie di sorveglianza, che si chiamino imsi catcher o
trojan di stato. se qualcuno vuole comunicarcelo, ci scriva una mail.

> 
> Underscore _TO* Hacklab // underscore chiocciola autistici.org  
> Key fingerprint = 5DAC 477D 5441 B7A1 5ACB  F680 BBEB 4DD3 9AC6 CCA9  
> gpg2 --recv-keys 0x9AC6CCA9  
>  