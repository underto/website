Title: Warmup Hackmeeting 0x14
Date: 2017-01-21 00:00
Category: news
Tags: 
Slug: warmup-hackit
Authors: underto
Summary:21 Gennaio @Gabrio, chiacchieriamo di trojan di stato e hackmeeting 2017

Il 21 Gennaio dalle 16 saremo al Gabrio (via Millio 42) per il [warmup](https://hackmeeting.org/hackit17/warmup.html#gennaio-switch-the-inputs-csoa-gabrio-torino) di [Hackmeeting 0x14](http://it.hackmeeting.org) (che quest'anno si terrà in Val Susa) e faremo due chiacchiere su hackmeeting e [trojan](https://it.wikipedia.org/wiki/Trojan_(informatica)) di stato con il collettivo [tracciabi.li](http://www.tracciabi.li/).

![SwitchTheInputs](http://switchtheinputs.org/files/2016/10/loca-swwitch.jpg)
