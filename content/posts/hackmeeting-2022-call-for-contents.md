Title: Hackmeeting 2022 - Call for Contents
Slug: hackmeeting-2022-call-for-contents
Date: 2022-07-13 22:37:44
Tags: hackit

<center>![logo_hackit]({static}/images/logo_hackit.png)</center>

Hackmeeting è l'incontro annuale delle controculture italiane legate
all'hacking, comunità che vivono in maniera critica il rapporto con la
tecnologia.

Quest'anno si terrà a [Torino al C.S.O.A. Gabrio](https://hackmeeting.org/hackit22) dal venerdì 9 a domenica 11 settembre 2022.

La nostra idea di hacking investe ogni ambito della nostra vita e non si
limita al digitale: crediamo nell'uso del sapere e della tecnologia per
conoscere, modificare e ricreare quanto ci sta attorno.

Purtroppo la realtà è difficile da leggere, complicata da modificare e
interagire con essa è impegnativo.

Vogliamo riprenderne il controllo.

Non è un idea vaga, una generica linea guida o aspirazione, è invece una
pragmatica capacità organizzativa basata sulla solidarietà, complicità e la
messa in comune di conoscenze, metodi e strumenti per riprenderci tutto e
riprendercelo insieme, un tassello per volta.

In sintesi se pensi di poter arricchire l'evento con un tuo contributo manda
la tua proposta iscrivendoti alla [mailing list di hackmeeting](https://www.autistici.org/mailman/listinfo/hackmeeting) o all'indirizzo di
posta [infohackit@autistici.org](mailto:infohackit@autistici.org) mandando una mail con oggetto `[talk]
titolo_del_talk` oppure `[laboratorio] titolo_del_lab` e queste informazioni:

* Durata (un multiplo di 30 minuti, massimo due ore)
* Eventuali esigenze di giorno/ora
* Breve Spiegazione, eventuali link e riferimenti utili
* Nickname
* Lingua
* Necessità di proiettore
* Disponibilità a farti registrare (solo audio)
* Altre necessità

#### Nel concreto

Allestiremo tre spazi all'aperto, o al coperto in caso di pioggia, muniti di
amplificazione e proiettore.
Se pensi che la tua presentazione non abbia bisogno di tempi cosi lunghi, puoi
proporre direttamente ad hackmeeting un `ten minute talk` di massimo 10
minuti.
Questi talk verranno tenuti nello spazio più capiente al termine della
giornata di sabato; ci sarà una persona che ti avviserà quando stai per
eccedere il tempo massimo.

Se invece vuoi condividere le tue scoperte e curiosità in modo ancora piu
informale e caotico, potrai sistemarti con i tuoi ciappini, sverzillatori,
ammennicoli ed altre carabattole sui tavoli collettivi del `LAN space`.
Troverai curiosità morbosa, corrente alternata e rete via cavo (portati una
presa multipla, del cavo di rete e quel che vuoi trovare).

Hai una domanda estemporanea o una timidezza? Scrivici a
[infohackit@autistici.org](mailto:infohackit@autistici.org)

Ti piacerebbe che si parlasse di un argomento che non è stato ancora proposto?
Aggiungilo insieme al tuo nick in [questo pad](https://pad.cisti.org/p/hackit-desiderata) o manda una mail in lista
hackmeeting e spera che qualcun* abbia voglia di condividere le sue
conoscenze e si faccia avanti.

Per più info visita il sito [hackmeeting.org](https://hackmeeting.org/hackit22)


### English

Hackmeeting is the yearly meeting of the Italian countercultures related to
hacking, communities which have a critical relationship with technology.

This year it will be held in [Turin at C.S.O.A. Gabrio](/info) from Friday, September 9 to Sunday, September 11, 2022.

Our idea of hacking touches every aspect of our lives and it doesn't stop at
the digital realm: we believe in the use of knowledge and technology to
understand, modify and re-create what is around us.

Unfortunately reality is hard to read, hard to modify and interacting with it
is a hard task.

We want to take control.

It's not a vague idea, generic guidelines or aspiration, it's a pragmatic
organization effort based on solidarity, complicity and sharing of knowledge,
methods and tools to take back everything and take it back together, one piece
at a time.

If you think you can enrich the event with a contribution, send your proposal
joining the [hackmeeting mailing list](https://www.autistici.org/mailman/listinfo/hackmeeting) or sending a message to
[infohackit@autistici.org](mailto:infohackit@autistici.org) with subject `[talk] title` or `[lab]
lab_title` and this info:

* Length (multiple of 30 minutes, max 2 hours)
* (optional) day/time of the day requirements
* Abstract, optional links and references
* Nickname
* Language
* Projector
* Can we record you? (audio only)
* Anything else you might need

#### More concretely

We will set up three outdoor locations (indoor in case of
rain), with microphone/speakers and projector.
If you think your presentation will not need this much time, you can propose
something directly at hackmeeting: a `ten minute talk` of a maximum of 10
minutes.
These talks will be held in the largest available space at the end of of the
day on Saturday. There will be someone who will warn you if you are about to
exceed the allocated time.

If you want to share your discoveries or curiosities in an even more informal
and chaotic way, you will be able to to get a place with your hardware on the
shared tables in the `LAN space`. You will find morbid curiosity, power and
wired connection (bring a power strip, network cables and whatever you might
find useful).

Have any questions or you are shy? Write us at [infohackit@autistici.org](mailto:infohackit@autistici.org)

Would you like to talk about something that hasn't yet been brought up? Add
it, along with your nick, to [this pad](https://pad.cisti.org/p/hackit-desiderata) or send a mail to the
hackmeeting list and hope that somebody else wants to share their knowledge
and steps up.
