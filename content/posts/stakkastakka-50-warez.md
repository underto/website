Title: stakkastakka #50 - Warez
Slug: stakkastakka-50-warez
Date: 2020-01-08 12:20:19
Tags: stakkastakka
https://radioblackout.org/wp-content/uploads/2020/01/50-warez.ogg
<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/01/50-warez.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/01/50-warez.ogg'/>Puntata completa</a>


#### PODCAST + Approfondimento WAREZ

Posta del ♥ dell'Internet
--------------------------

-   Monopattini che passione

=> [monopattini-vandalizzati-e-rubati-la-
micro-mobilita-in-sharing-si-trasforma-in-un-incubo](https://www.lastampa.it/torino/2019/12/15/news/monopattini-vandalizzati-e-rubati-la-%20micro-mobilita-in-sharing-si-trasforma-in-un-incubo-1.38214130)

I monopattini non convincono neanche un po' una gestione un po' troppo
facile per il comune di torino, una narrativa ecologista che nasconde un
consumismo ed una profilazione smart. Insomma questi monopattini non ci
conviscono, sembrano accessori per la mobilita' ma si trasformano in
spazzatura cittadina si poteva prevedere ? Certo, ma e' stato piu'
facile fare l'accordo commerciale

-   Le migliori 20 password del 2019

=> [worst-password-2019](https://securityaffairs.co/wordpress/95222/reports/worst-passwords-2019.html)

12345, 123456, password, 1312, qwerty, test1, iloveyou, abc123,
qwertyuiop le password che palle, le password che passione!

Se usate la prima password che vi viene in mente allora sapete gia' che
questa password la potrebbe indivinare anche qualcun'altro.. Ecco la
lista delle migliori e peggiori password del 2019!

Ecco alcune raccomandazioni per aumentare la sicurezza:

1.  controlla su [monitor.firefox.com](https://monitor.firefox.com) se
    la tua password e' stata leakata e scoperto in seguito al hack di
    qualche sito web
2.  Aggiorna le tue password costantemente e usa una parola diversa per
    ogni account
3.  Usare un autenticazione a due fattori (2fa) come app consigliamo
    [FreeOTP+](https://f-droid.org/en/packages/org.liberty.android.freeotpplus/)
4.  Usare un password manager sul tuo computer, noi consigliamo
    [keepassxc](https://keepassxc.org)
5.  Fai sempre attenzione alle email che ti arrivano e se noti qualche
    movimento sospetto nel tuo account cambia la password immediatamente

Interviste
----------

Intervista a CIRCE per la presentazione del libro Internet Mon Amour

=> [Internet Mon Amour Libro](https://ima.circex.org)

Approfondimento: WAREZ
----------------------

**Parliamo di warez, che cosa sono i Warez e come e' partito tutto
questo?**

Warez e' il software piratato. La scena warez ha cominciato a emergere
nel 1970, nata dai predecessori di cracking software e gruppi di
ingegneria inversa. Il loro lavoro era reso disponibile sui sistemi
privati di bacheca (BBS).

**Cosa vuol dire BBS?**

I primi BBS si trovavano negli Stati Uniti, ma simili cominciarono a
comparire in Canada, Regno Unito, Australia ed Europa continentale.
All'epoca, l'installazione di una macchina in grado di distribuire
dati non era banale e richiedeva una certa capacità tecnica. La ragione
per cui veniva fatto di solito era per sfida tecnica. I BBS in genere
ospitavano diversi megabyte di materiale. Le migliori schede avevano più
linee telefoniche e fino a cento megabyte di spazio di archiviazione,
che era molto costoso al tempo. Le release erano per lo più giochi. Man
mano che il mondo dello sviluppo software si evolveva per contrastare la
distribuzione di materiale "piratato" e che il software e l'hardware
necessari per la distribuzione diventavano prontamente disponibili a
chiunque. La Scena si è adattata ai cambiamenti e si è trasformata da
una semplice distribuzione a un effettivo cracking delle protezioni e di
reverse engineering non commerciale.

**Cosa intendi per Reverse Engineering?**

Poichè molti gruppi di persone volevano partecipare è emersa l'esigenza
di promuoversi. Il che ha spinto l'evoluzione della Scena, che si è
specializzata nella creazione di arti grafiche associate ai singoli
gruppi (crack intro).

I gruppi promuovono le loro abilità con software sempre più sofisticato
e avanzato, aggiungendo le proprie intro e più tardi anche aggiungendo
la propria musica (si crea la scena demo). Quindi appunto si va alla
vera e propria modifica del gioco per creare un opera d'arte. Un
evoluzione dal semplice "sbloccare" al carpirne ogni singola parte e
modificarla a piacimento utilizzando tutte le potenzialita' della
macchina.

La scena Demo ha poi iniziato a separarsi dalla "scena warez" nel
corso degli anni '90 ed è ora considerata come una sottocultura
completamente diversa. Con l'avvento di internet i Cracker iniziarono
ad organizzarsi all'interno di "Bande" segrete formate online. Nella
seconda metà degli anni novanta, una delle fonti d'informazione più
rispettate in materia di Ingegneria Inversa (Reverse) era il sito web
Fravia^[1](#fn:1){.footnote-ref}^.

**Parliamo delle prime crack e delle prime protezioni, perche' e come
sono state sconfitte o bypassate e chi erano questi cracker?**

Le prime protezioni anticopia sono state applicate da: AppleII, Atari800
e Commodore64. Nel tempo ovviamente le tipologie di protezione anticopia
si sono fatte man mano più complesse. La maggior parte dei primi cracker
software spesso formavano gruppi che gareggiavano l'uno contro l'altro
nel cracking e la diffusione di software. La rottura di un nuovo sistema
di protezione anticopia il più rapidamente possibile era considerata
come un'opportunità per dimostrare la propria superiorità tecnica. E lo
e' ancora, Inoltre tutto questo non lo si fa per una questione
economica.

I cracker erano (e sono) programmatori che "sconfiggono" la protezione
anticopia sul software come hobby, per aggiungere il loro alias alla
schermata del titolo, e poi distribuire il "cracking" prodotto alla
rete di warez BBS o siti Internet specializzati nella distribuzione di
copie non autorizzate (es: torrent, ftp, emule,..)

I gruppi di cracker degli anni '80 iniziarono a pubblicizzare loro
stessi e le loro abilità aggiungendo schermi animati noti come crack
intros nei programmi software che bucavano. Una volta che la
competizione tecnica si era estesa dalle sfide del cracking alle sfide
della creazione di intros visivamente stupefacenti, le basi per una
nuova sottocultura conosciuta come scena demo erano state stabilite.

La maggior parte dei gruppi di cracking continuano questo lavoro non per
guadagnare soldi ma per curiosita', per accrescere il rispetto del
gruppo e per sfidare le softwaree house che producono nuovi software
sempre piu' protetti.

Agli inizi I software crackati venivano pubblicati sulla rete tramite
archivi di distribuzione FTP, mentre oggi e' possibile scaricarli via
torrent o tramite siti di reverse specializzati.

**Ok, ma chi c'e' dietro oggi?**

La scena oggi è formata da piccoli gruppi di persone qualificate, che
informalmente competono per avere i migliori cracker, metodi di
cracking, e reverse engineering.

Per esempio esiste la High Cracking University (+HCU), che è stata
fondata da Old Red Cracker (Stan Kelly-Bootle), un
musicista-accademico-informatico inglese, considerato un genio della
reverse engineering e una figura leggendaria in Reverse Code
Engineering(RCE), per aver fatto progredire la ricerca nel RCE. OldRed
aveva anche insegnato e scritto diversi testi sull'argomento, infatti
questi testi sono ora considerati dei classici per chi studia appunto il
reverse engineering. Al giorno d'oggi la maggior parte dei laureati di
+HCU sono quasi tutti su Linux e pochi sono rimasti come craccatori di
Windows. Le informazioni presso l'università sono state riscoperte da
una nuova generazione di ricercatori e operatori di RCE che hanno
avviato nuovi progetti di ricerca nel settore. Restano inoltre
indovinelli creati da OldRed nel '97 e non ancora risolti che appaiono
nascosti nella rete disponibili per chi si appassiona al cracking
software^[2](#fn:2){.footnote-ref}^

**Come funzionava in pratica il sw cracking ?**

Durante gli anni '80 e '90, i videogiochi venduti su cassette audio e
floppy disk erano talvolta protetti con un metodo esterno che richiedeva
all'utente di avere il pacchetto originale, il manualo o una parte di
esso. La protezione anticopia veniva attivata non solo durante
l'installazione, ma ogni volta che il gioco veniva eseguito. es

**Ma il tempo passa e anche le corporation imparano a proteggersi.. Come
si sono evolute le protezioni delle copie nel tempo?**

Quando la Sega Dreamcast è stata rilasciata nel 1998, è uscita con un
nuovo formato di disco, chiamato GD-ROM. Utilizzando un lettore CD
modificato, si può accedere alla funzionalità del gioco. L'utilizzo di
uno speciale CD, con la modifica, consente di leggere un gioco
attraverso un CD-ROM utilizzando il Boot del CDRom per poi caricare il
gioco piratato bypassando la protezione.

Le console di nuova generazione usano dischi Blu-ray. Oltre alla
protezione offerta dalle console stesse, le specifiche del formato
Bluray consentono di ottenere un marchio (BD-Mark) che non può essere
duplicato senza l'accesso da amministratore di sistema. Il formato
BD-ROM fornisce una notevole capacità (fino a 100 GB per disco con
potenziale revisione per fornire di più), maggiore disponibilità di
banda dei consumatori combinata con l'aumento delle dimensioni dei
giochi distribuiti attraverso i canali online (avvicinandosi a 100
gigabyte per alcuni titoli) sta rendendo la distribuzione di software
crackato difficile. Per evitare che le console stesse vengano hackerate
e usate come mezzo per sconfiggere queste protezioni (come è successo
con la WII e in parte con la PlayStation 3), le console contemporanee
come Xbox o PS4, usano strumenti hardware di fiducia (firmati con una
chiave) che autenticano l'hardware interno e bloccano qualsiasi
software precedente.

**In conclusione: crack, crackers e craccare che significa al giorno
d'oggi ?**

Se parliamo di cracking, parliamo della modifica del codice binario di
un'applicazione per causare o impedire una parte specifica
nell'esecuzione del programma. Un esempio specifico di questa tecnica è
una crack che rimuove la scadenza del periodo di prova di
un'applicazione o saltare il passaggio laddove richiede una chiave.

Le crack sono di solito programmi che alterano l'eseguibile del
programma e a volte le librerie come le .dll o .so.

Al giorno d'oggi uno dei metodi utilizzati e' quello di utilizzare
programmi specifici per scannerizzare il programma usato per proteggere
la copia. Dopo aver scoperto il software utilizzato per proteggere
l'applicazione un programma specifico può essere utilizzato per
rimuovere la protezione anticopia dal software sul CD o DVD (detto anche
effettuare il crack). In altri casi piu' fortunati è possibile
"decompilare" un programma per ottenere il codice sorgente, come ad
esempio se volessimo craccare le app della GTT o del bike sharing, ma
questa e' un altra storia...

------------------------------------------------------------------------

::: {.footnote}

------------------------------------------------------------------------

1.  ::: {#fn:1}
    Francesco Vianello (30 agosto 1952 -- 3 maggio 2009), meglio
    conosciuto con il suo soprannome Fravia (a volte +Fravia o Fravia+),
    è stato un noto ingegnere del software inverso e hacker. Noto per il
    suo archivio web di reverse engineering. È noto anche per il suo
    lavoro sulla steganografia. Ha insegnato su argomenti quali
    l'estrazione di dati, l'anonimato, l'inversione pubblicitaria e
    l'ad-busting.
    *NB*: il [subvertising](https://en.wikipedia.org/wiki/Subvertising)
    non e' stato inventato da Bansky ma da Fravia!
    <https://en.wikipedia.org/wiki/Fravia> [↩](#fnref:1 "Jump back to footnote 1 in the text"){.footnote-backref}
    :::

2.  ::: {#fn:2}
    <http://www.home.aone.net.au/~byzantium/orc.html> [↩](#fnref:2 "Jump back to footnote 2 in the text"){.footnote-backref}
    :::
:::

