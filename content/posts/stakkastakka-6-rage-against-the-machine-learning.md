Title: stakkastakka #6 - Rage Against The Machine Learning
Slug: stakkastakka-6-rage-against-the-machine-learning
Date: 2018-11-28 15:20:53
Tags: stakkastakka, machine learning, AI

#### POSTCAST
<audio controls>
  <source src='https://stakkastakka.fugadalcontrollo.org/stakkastakka-6-machinelearning.ogg' type='audio/mpeg'>
</audio>
<a href='https://stakkastakka.fugadalcontrollo.org/stakkastakka-6-machinelearning.ogg'/>Puntata completa</a>
<audio controls>
  <source src='https://stakkastakka.fugadalcontrollo.org/stakkastakka-6-approfondimento.ogg' type='audio/mpeg'>
</audio>
<a href='https://stakkastakka.fugadalcontrollo.org/stakkastakka-6-approfondimento.ogg'/>Approfondimento Machine Learning</a>


<br/>
#### Posta del cuore dell'Internet:
- [Social score cinese a Pechino](https://www.ilpost.it/2018/11/25/punteggio-cittadini-pechino/)
- [Nack](https://nack.msack.org/)
- [compromessi 500mila account PEC](https://www.repubblica.it/tecnologia/sicurezza/2018/11/19/news/dopo_l_attacco_hacker_ai_tribunali_cambiate_subito_la_password_della_vostra_pec_-212086305)
- [Protonmail hack/scam](https://www.bleepingcomputer.com/news/security/hacker-say-they-compromised-protonmail-protonmail-says-its-bs/)

#### APPROFONDIMENTO

Un preparatissimo contributo al dibattito sul machine learning.
