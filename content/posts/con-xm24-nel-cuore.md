Title: Con XM24 nel cuore
Slug: con-xm24-nel-cuore
Date: 2019-08-08 00:43:40


![Underscore love XM24]({static}/images/underscore_love_xm24.png)

XM24 resta nel cuore dell'underscore ebbasta.

Dopo lo sgombero annunciato di XM24 da parte del comune di Bologna.
Lo storico centro sociale xm24 e' stato smantellato e con lui gli spazi, l'hacklabbo
i bagni costruiti per l'hackmeeting, la ciclofficina, il cinema e il bar.

Quello che non sono riusciti a smantellare e' la forza e la determinazione delle persone
che a Bologna continueranno a organizzarsi insieme e a gridare forte contro il nulla che avanza.

Tutta la nostra solidarieta' alle compagne ed i compagni di xm24!

[#xm24resiste](https://mastodon.cisti.org/tags/xm24resiste)


