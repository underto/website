Title: stakkastakka #23 exodus
Slug: stakkastakka-23-exodus
Date: 2019-04-02 15:39:13
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/04/stakkastakka-23.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/04/stakkastakka-23.ogg'/>Puntata completa</a>

#### Posta del cuore

- [russi e GPS](https://www.c4reports.org/aboveusonlystars)
- windows calculator
- Asus [update hack](https://www.wired.com/story/asus-software-update-hack/)

#### Exodus

Abbiamo parlato del [malware
exodus](https://securitywithoutborders.org/blog/2019/03/29/exodus-ita.html) con
chi ha realizzato l'[analisi
tecnica](https://motherboard.vice.com/it/article/7xnyy9/malware-exodus-infettati-1000-italiani-app-nascosta-google-play-store).
