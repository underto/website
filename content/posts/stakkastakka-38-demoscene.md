Title: stakkastakka #38 - Demo Scene
Slug: stakkastakka-38-demoscene
Date: 2019-09-09 17:23:10
Tags: stakkastakka


#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/09/38.demo_scene_commodore64.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/09/38.demo_scene_commodore64.ogg'/>Puntata completa</a>
<br/>
<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/09/scena_demo_commodore64.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/09/scena_demo_commodore64.ogg'/>Solo approfondimento</a>


## Posta del ♥ dell'Internet

Su droni, assessore che diventano ministre e attacco alla drone race  

- [thread su reddit](https://www.reddit.com/r/italy/comments/ccnsis/attacco_informatico_alla_gara_di_droni_di_torino/)
- [il comunicato dell'O.U.F.O.O.](https://zerobin.net/?1a618bcb4305732a#DHiwn9Zs4FMHZROLdVZGFMCVlSAxbyVaiT5ZQCbZpb0=)
- [un mese dedicato ai droni](https://www.lastampa.it/tecnologia/news/2019/06/24/news/a-torino-un-mese-dedicato-ai-droni-1.36542813)

altre notiziole

- [come google, amazon e microsoft anche apple e' stata pizzicata con le
  mani nella marmellata](https://www.punto-informatico.it/chi-ascolta-conversazioni-apple-siri/)
- [la meta' delle ricerche fatte su google rimane li'](https://www.punto-informatico.it/chi-ascolta-conversazioni-apple-siri/)
- [allora la AI serve!](https://www.wired.it/economia/business/2019/09/06/truffa-software/)
- [in danimarca devono rifare 10mila processi del 2012 perche' i dati della
  geolocalizzazione usati come prove erano sbagliati](https://www.nytimes.com/2019/08/20/world/europe/denmark-cellphone-data-courts.html)



