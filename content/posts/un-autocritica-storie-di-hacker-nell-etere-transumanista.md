Title: Un autocritica: storie di hacker nell'etere transumanista
Slug: un-autocritica-storie-di-hacker-nell-etere-transumanista
Date: 2019-05-09 18:33:09


Questa storia incomincia quando come hacklab veniamo invitati a [librincontro](https://librincontro.noblogs.org)
per presentare il nostro progetto [cisti.org](https://cisti.org). Ovviamente siamo molto felici di raccontare il
 lavoro degli ultimi mesi in una tre giorni ricca di presentazioni da condividere con amic\* e compagn\*.  

Una settimana fa, a latere di una riunione di librincontro, nasce la possibilità di ospitare l'editore Nautilus nel nostro
programma [stakkastakka](/tag/stakkastakka.html) sulle libere frequenze di [radio blackout](https://radioblackout.org/). 
Sembriamo i più indicati ad ospitare questa
intervista considerando i temi della nostra trasmissione ed il libro di Nautilus "Critica al transumanesimo".

Non ci facciamo molte domande, anzi nessuna, fissiamo un'intervista per lunedì nel nostro programma. Ci fidiamo, ci
fidiamo con leggerezza ed ingenuità della bontà della presentazione dato che era già inserita nel programma ufficiale
 di librincontro.  

Ci scambiamo qualche email con Claudio (l'ospite ed editore di Nautilus) per concordare i tempi e solo domenica riceviamo
 l'indice del libro. Non nascondiamo di non esserci mai molto interessati ai discorsi sul transumanesimo, l'unica chiave
 di lettura  che ci ha coinvolto sul tema è una "critica al transumanesimo" di stampo anticapitalista,
 una critica al transumanesimo come forza economica ed egemonica che prova a legittimare il dominio dei grandi monopolisti della
 Silicon Valley sulla nostra società, finanziando master universitari e lobby politiche.  


Sicuramente il nostro ospite era a conoscenza dei temi spinosi sollevati dalle persone all'ascolto, certamente piu' sgamate
 di noi sul tema, domande a cui ha preferito non rispondere e a cui noi abbiamo dato poco seguito, su questo
dobbiamo fare autocritica.  

A causa della nostra leggerezza nel preparare l'intervista, non potevamo
immaginare le idee sostenute da chi questo libro l'ha scritto. Cosi' nel pomeriggio arrivano le prime segnalazioni e critiche,
allora ci mettiamo a cercare e arriviamo sul sito di "urlo della terra". Quello che leggiamo, a partire dall'indice, ci fa
accapponare la pelle:


 _"Ecologismo e transumanismo connessioni contro natura, dove trans-xeno-femminismo, queer e antispecismo incontrano la tecnoscienza,
 Il cyborg: una metafora che si incarna, un dispositivo di potere e la fine di ogni liberazione,
 Vaccini: armi di distruzione di massa._

Andando avanti nella lettura le cose peggiorano e vogliamo fare alcune considerazioni.

Ci assumiamo la nostra responsabilità nell'essere stati cosi' ingenui e poco informati nel preparare questa
intervista e ci scusiamo con tutte le persone, cyborg e programmi che si sono sentiti offesi.

Come spesso sottolineiamo, la tecnologia non e' neutra ma rispecchia le idee, i percorsi e la visione del mondo di
chi la produce, la ricerca, la finanzia, e nel contesto del capitalismo, del razzismo e del patriarcato la tecnologia
spesso significa sorveglianza, sfruttamento e coercizione.
Ma per noi la tecnologia e' anche uno strumento di liberazione, ed e' per questo che suggeriamo di usare i server
autogestiti, di utilizzare software libero, per questo ci sbattiamo per creare tecnologie alternative alle piattaforme
del capitalismo con progetti come cisti.org, per questo partecipiamo ogni anno alla costruzione di hackmeeting.

La riappropriazione dei saperi, delle tecniche e degli strumenti a beneficio delle liberta' personali
e collettive e' uno dei punti chiave del nostro agire.

Per essere chiari e per rispondere meglio alle domande ricevute in trasmissione, consideriamo ovviamente
la possibilita' di abortire e la possibilita' di effettuare una transizione di genere come ottimi
esempi di tecniche di autodeterminazione ed emancipazione personale.

Ci auguriamo che quanto scritto sopra chiarisca la nostra posizione, ed è alla luce di questo che abbiamo
deciso (non senza difficolta') di annullare la nostra presentazione a librincontro programmata alle 16:00
di sabato.


underscore hacklab / stakkastakka