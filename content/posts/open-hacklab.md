Title: Open Hacklab
Date: 2016-08-19 10:00
Modified: 2016-08-19 10:00
Category: news
Tags: 
Slug: open-hacklab
Authors: underto
Summary: Perquisizione al CSOA Gabrio, qualche danno all'hacklab.
gallery: {photo}danni_hacklab

Volevamo un hacklab più aperto, ma non in questo senso.

![#refugeewall]({static}/images/perquisa-agosto.jpg)

Durante la perquisizione al CSOA Gabrio di ieri le divise blu hanno deciso di accanirsi anche contro _TO*hacklab, tentando di forzare la porta blindata per poi distruggere il muro a fianco, entrare e constatare che quello spazio era un'officina per computer come effettivamente poteva sembrare dalla breccia nella finestra.

Per giustificare l'impegno speso nel distruggere questo muro è stata staccata la spina all'armadio hub/gateway; come conseguenza un alimentatore si è sentito male ed è stato soccorso nelle ore successive, quando tutto è tornato operativo.

Ci fa sorridere la miseria di queste indagini e di questi piccoli personaggi ancora a cavalcare un modello proibizionista che nessuno più intende perseguire perché ampiamente fallimentare, decidendo di colpire una delle poche esperienze di autoproduzione nello stesso giorno in cui un magistrato di rilevo occupa le prime pagine dei quotidiani schierandosi a favore della legalizzazione.

Siamo solidali con i due compagni colpiti dalla repressione, continueremo ad essere complici del CSOA Gabrio nell'antiproibizionismo come nelle diverse lotte.

_TO*hacklab


Altre informazioni:

* [CSOA Gabrio: comunicato perquisizione](https://gabrio.noblogs.org/post/2016/08/18/proibizionismo-dagosto-di-laboratori-della-droga-e-muri-sfondati/)
* [Radio Blackout: approfondimenti](http://radioblackout.org/tag/cannabis/)
* [InfoShock Torino](https://infoshocktorino.noblogs.org/)

![#perquisa]({lightbox}danni_hacklab/IMG_20160818_160926.jpg)
![#perquisa]({lightbox}danni_hacklab/IMG_20160818_160941.jpg)
![#perquisa]({lightbox}danni_hacklab/IMG_20160818_174842.jpg)
![#perquisa]({lightbox}danni_hacklab/IMG_20160818_182807.jpg)
![#perquisa]({lightbox}danni_hacklab/IMG_20160818_182836.jpg)
![#perquisa]({lightbox}danni_hacklab/IMG_20160819_151628.jpg)
![#perquisa]({lightbox}danni_hacklab/IMG_20160819_151701.jpg)
![#perquisa]({lightbox}danni_hacklab/IMG_20160819_151827.jpg)
![#perquisa]({lightbox}danni_hacklab/IMG_20160819_151928.jpg)
![#perquisa]({lightbox}danni_hacklab/IMG_20160819_151948.jpg)
