Title: stakkastakka #46 - Tor
Slug: stakkastakka-46-tor
Date: 2019-11-20 10:19:37
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/11/stakkastakka46.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/11/stakkastakka46.ogg'/>Puntata completa</a>
<br/>

## Notizie amiche

- Una [nuova trasmissione acara](https://unit.abbiamoundominio.org/bitume/index.html) nella pianura padana
- Compleanno dell'hacklab avana a Dicembre in quel di Roma
- A Napoli ci sarà il [nack](https://msack.org/2019/11/13/1206/)

## Posta del ♥ dell'Internet

- [Torino accordo google - tim - comune di torino](http://www.ansa.it/piemonte/notizie/2019/11/11/gubitosia-torino-data-center-tim-google_8024b6e5-1a55-4af9-ab5c-b607d8075b65.html)
- [Mastodon is trending in India](https://www.thehindubusinessline.com/info-tech/social-media/social-network-mastodon-is-trending-in-india-should-twitter-worry/article29908054.ece)
- [Antifa-data](https://archive.org/details/@antifa-data)
- [Lasera Attacks!](https://lightcommands.com/)
- [The Economic Inequality of Mobile Security](https://nex.sx/blog/2019/11/11/the-economic-inequality-of-mobile-security.html)

## Approfondimento

Intervista a Gus del Tor Project!

