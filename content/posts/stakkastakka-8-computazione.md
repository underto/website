Title: stakkastakka #8 - Computazione
Slug: stakkastakka-8-computazione
Date: 2018-12-10 17:30:19
Tags: stakkastakka, computazione

#### POSTCAST
<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2018/12/stakkastakka-8-computazione.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2018/12/stakkastakka-8-computazione.ogg'/>Puntata completa</a>

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2018/12/stakkastakka-computazione.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2018/12/stakkastakka-computazione.ogg'/>Approfondimento
sulla computazione</a>


<br/>

#### NEWS, abbiamo parlato di:

- [Flicker limita gli account gratuiti](https://arstechnica.com/gaming/2018/11/free-flickr-accounts-slashed-to-1000-pictures-the-rest-will-be-deleted/)
- [Super hacker della Feltrinelli](https://www.bufale.net/home/attacco-hacker-alla-pagina-facebook-della-feltrinelli-di-bologna/)
- [{3,4,5}G vulnerabili](https://www.rtl-sdr.com/usrp-sdrs-used-to-break-3g-to-5g-mobile-phone-security/)
- [Arrestata dirigente Huawei](https://tinyletter.com/carolafrediani/letters/guerre-di-rete-newsletter-il-caso-huawei-sotto-la-lente-poi-khashoggi-riconoscimento-facciale-e-altro)
- [Intelligenze che compilano no-fly-list](https://theintercept.com/2018/12/03/air-travel-surveillance-homeland-security/)
- [L'Australia approva le backdoor di stato]( https://www.bbc.com/news/world-australia-46463029)
- [GHCQ vanifica la cifratura di Whatsapp](https://www.lawfareblog.com/principles-more-informed-exceptional-access-debate)

#### APPROFONDIMENTO

In nome della trasparenza ed honestà mettiamo qua direttamente gli appunti
che erano stati scritti per questa puntata. Non è assolutamente un modo per
evitarci del lavoro ;)

FATE UNA INTRO!!!! NON SI SA DI COSA STATE PARLANDO!

1. Spieghiamo perché ci interessiamo di computazione, in  particolare
   riportiamo le differenze più interessanti, abbondando di spiegoni:
   sistemi lineari- complessi - caotici, problemi computabili -
   intrattabili, modelli discreti - continui

2. La discussione su cui partiva,  tipo Modellizzare un fenomeno continuo
   in un gruppo discreto, citando gli esempi che riporta sulla
   meteorologia, eq di Navier Stokes ecc.

3. un interessante contributo all'investigazione dei sistemi complessi è
   stato dato da steven wolfram, fisico e creatore di Mathematica, che agli
   inizi degli anni 2000 pubblica "a new kind of science", un malloppone
   che parte dagli automi cellulari per arrivare a decretare la nascita di
   una nuova scienza, la "computazione sperimentale". Questo libro ha
   richiesto circa dieci anni per essere scritto, è lungo 846 pagine con
   349 pagine di note. La prima frase è già indicativa dello stile, da
   molti reputato arrogante, del libro. "Tre secoli fa la scienza è stata
   trasformata dalla nuova, drammatica idea che le regole basate su
   equazioni matematiche potevano essere usate per descrivere il mondo
   naturale" riferendosi a Newton. Questo riferimento piuttosto
   imbarazzante definisce il palcoscenico: Wolfram mette il suo opus magnum
   sullo stesso piano del più grande lavoro nella scienza. Sostiene di aver
   fatto una scoperta importante che apre la strada al progresso in varie
   aree della scienza, dove quella che lui chiama "matematica tradizionale"
   non è riuscita a portare progressi significativi.

4. Il papero già citato mi intriga
   https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005268

5. Possiamo anche uscire on qualcosina di più recente tipo sugli oracoli,
   intesi come funzioni non computabili e troverebbero un corrispettivo
   nella mech quantistica nell'operatore di misura.  Seguono pipponi vari
   sulle teorie di penrose :
   http://nautil.us/issue/47/consciousness/roger-penrose-on-why-consciousness-does-not-compute

6. Computazione non convenzionale: i computer tradizionali sono calcolatori
   basati sull'architettura di Von Neumann, resi possibili dall'invenzione
   del transistor e dal ridimensionamento della legge di Moore. Ci sono
   ricercatori che tentano di implementare estensioni di questa
   architettura, architetture alternative ad essa, ma anche architetture
   che non fanno uso di silicio. Qualche esempio:

7. **biocomputer**: Un biocomputer consiste in un percorso o una serie di
   percorsi metabolici che coinvolgono materiali biologici progettati per
   comportarsi in un certo modo in base alle condizioni (input) del
   sistema.  Il percorso risultante delle reazioni che si verificano
   costituisce un output, che si basa sulla progettazione ingegneristica
   del biocomputer e può essere interpretato come una forma di analisi
   computazionale. Tre tipi di biocomputer distinguibili includono computer
   biochimici, computer biomeccanici e computer bioelettronici.  I computer
   biochimici utilizzano l'immensa varietà di cicli di feedback che sono
   caratteristici delle reazioni chimiche biologiche al fine di ottenere
   funzionalità computazionale. I cicli di feedback nei sistemi biologici
   assumono molte forme e molti fattori diversi possono fornire un feedback
   sia positivo che negativo ad un particolare processo biochimico,
   causando rispettivamente un aumento della produzione chimica o una
   diminuzione della produzione chimica.  I computer biomeccanici sono
   simili ai computer biochimici in quanto entrambi eseguono un'operazione
   specifica che può essere interpretata come un calcolo funzionale basato
   su condizioni iniziali specifiche che fungono da input.  Differiscono,
   tuttavia, in ciò che serve esattamente come segnale di uscita.  Nei
   computer biochimici, la presenza o la concentrazione di determinati
   prodotti chimici funge da segnale di uscita.  Nei computer biomeccanici,
   tuttavia, la forma meccanica di una specifica molecola o di una serie di
   molecole in un insieme di condizioni iniziali funge da output.  I
   biocomputer possono anche essere costruiti per eseguire il calcolo
   elettronico.  Anche in questo caso, come i computer biomeccanici e
   biochimici, i calcoli vengono eseguiti interpretando uno specifico
   output basato su un insieme iniziale di condizioni che fungono da input.
   Nei computer bioelettronici, l'uscita misurata è la natura della
   conduttività elettrica osservata nel computer bioelettronico.
8. **DNA-computing**: Il DNA computing è una forma di calcolo parallelo in
   quanto sfrutta le molte diverse molecole del DNA per provare molte
   diverse possibilità contemporaneamente. Per alcuni problemi
   specializzati, i computer del DNA sono più veloci e più piccoli di
   qualsiasi altro computer costruito finora. In generale sono lenti (il
   tempo di risposta è misurato in minuti, ore o giorni, anziché
   millisecondi) ma c'è una compensazione con la parallelizzazione,
   ottenuto dal fatto che milioni o miliardi di molecole interagiscono
   l'una con l'altra contemporaneamente.  Tuttavia, è molto più difficile
   analizzare le risposte fornite da un computer DNA piuttosto che da una
   digitale.  Questo campo fu inizialmente sviluppato da Leonard Adleman
   della University of Southern California , nel 1994, che fece una PoC di
   soluzione del problema del sentiero Hamiltoniano in sette punti. Mentre
   l'interesse iniziale era nell'usare questo nuovo approccio per
   affrontare i problemi NP-hard , si è presto reso conto che potrebbero
   non essere i più adatti per questo tipo di calcolo, e diverse proposte
   sono state fatte per trovare una " killer application " per questo
   approccio. Nel 1997, l'informatico Mitsunori Ogihara, che lavorava con
   il biologo Animesh Ray, suggerì di valutare i circuiti booleani e ne
   descrisse un'implementazione. Qualche passo avanti*: Nel 2002, i
   ricercatori del Weizmann Institute of Science di Rehovot, in Israele,
   hanno presentato una macchina di calcolo molecolare programmabile
   composta da enzimi e molecole di DNA anziché microchip al silicio. Il 28
   aprile 2004 sempre loro hanno annunciato su Nature di aver costruito un
   computer per il DNA accoppiato con un modulo di input e output che
   sarebbe teoricamente in grado di diagnosticare l'attività cancerosa
   all'interno di una cellula e di rilasciare un farmaco anticancro al
   momento della diagnosi. Nel gennaio 2013, ricercatori sono stati in
   grado di memorizzare una fotografia JPEG, un set di sonetti
   shakespeariani e un file audio del discorso di Martin Luther King, Jr. I
   Have a Dream sul deposito di dati digitali DNA. Nel marzo 2013, i
   ricercatori hanno creato un trascrittore (un transistor biologico).
   Nell'agosto 2016, ricercatori hanno utilizzato il sistema di modifica
   genica CRISPR per inserire una GIF di un cavallo al galoppo e un
   cavaliere nel DNA dei batteri viventi.
9. **QUANTUM**: C'È BISOGNO DI UN'INTRODUZIONE?!??! Da un'idea di Feynman
   del 1981: "come simulare un sistema quantistico? --> usando delle
   particelle quantistiche" il quantum ha anche delle interessanti
   implicazioni matematiche sulla teoria della computazione e sulla
   crittografia, l'algoritmo di shor fattorizza numeri interi in tempo
   polinomiale (n³) rompendo possibilmente RSA in futuro. Ma ci sono grosse
   difficoltà ingegneristiche, è difficile tenere insieme tanti qubit
   isolati dall'ambiente, memorizzare l'informazione (problemi di
   decoerenza) ed è difficile correggere gli errori su questi stati.
10. **chaos computing**: Il caos computing è l'idea di usare sistemi
    caotici per il calcolo.  In particolare, i sistemi caotici possono
    essere realizzati per produrre tutti i tipi di porte logiche e
    consentire ulteriormente che si trasformino l'uno nell'altro. I sistemi
    caotici generano un gran numero di modelli di comportamento e sono
    irregolari perché cambiano tra questi modelli.  Esse mostrano una
    sensibilità alle condizioni iniziali che, in pratica, significa che i
    sistemi caotici possono passare da uno schema all'altro in modo
    estremamente rapido. Una porta logica di morphing caotico consiste in
    un circuito generico non lineare che esibisce dinamiche caotiche
    producendo vari modelli.  Un meccanismo di controllo viene utilizzato
    per selezionare modelli che corrispondono a porte logiche diverse.  La
    sensibilità alle condizioni iniziali viene utilizzata per passare da un
    pattern all'altro estremamente veloce (ben al di sotto del ciclo di
    clock del computer).  Come esempio di come funziona il morphing
    caotico, considera un sistema caotico generico noto come la mappa
    logistica .  Questa mappa non lineare è molto ben studiata per il suo
    comportamento caotico. In questo caso, il valore di x è caotico quando
    r > ~ 3.57 ... e passa rapidamente tra i diversi pattern nel valore di
    x come uno itera il valore di n.  Un semplice controllore di soglia può
    controllare o dirigere la mappa o il sistema caotico per produrre uno
    dei molti modelli.  Il controller imposta fondamentalmente una soglia
    sulla mappa in modo tale che se l'iterazione ("aggiornamento caotico")
    della mappa assume un valore di x che si trova al di sopra di un
    determinato valore di soglia, x *, l'uscita corrisponde a 1, altrimenti
    corrisponde a 0. Si può quindi decodificare la mappa caotica per
    stabilire una tabella di ricerca di soglie che produca in modo
    affidabile una qualsiasi delle operazioni della porta logica. Dal
    momento che il sistema è caotico, possiamo quindi passare tra le varie
    porte ("pattern") in modo esponenziale velocemente.

Un radioascoltatore ci ha richiesto una guida semplice di introduzione alle
reti neurali. Nel fuori onda abbiamo chiesto al nostro super esperto di
fuffa e ha partorito [questo](http://ml4a.github.io).
