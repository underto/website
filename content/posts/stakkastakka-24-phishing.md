Title: stakkastakka #24 phishing
Slug: stakkastakka-24-phishing
Date: 2019-04-08 17:19:13
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/04/stakkastakka-24-phishing.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/04/stakkastakka-24-phishing.ogg'/>Puntata completa</a>

#### Posta del cuore

- [Carcerati finlandesi vs Amazon Turk](
  https://www.theverge.com/2019/3/28/18285572/prison-labor-finland-artificial-intelligence-data-tagging-vainu)
- [Come hackerare le
  tesla](https://boingboing.net/2019/03/31/mote-in-cars-eye.html)
- [Ex dipendente mozilla contro le guardie di
  frontiera](https://www.theregister.co.uk/2019/04/02/us_border_patrol_search_demand_mozilla_cto/)

#### Phishing

Abbiamo parlato di phishing a partire da
[questa](https://securitywithoutborders.org/resources/guide-to-phishing.html)
guida di security without borders.

