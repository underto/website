Title: stakkastakka #25 Assange
Slug: stakkastakka-25-assange
Date: 2019-04-16 10:18:13
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/04/stakkastakka-25-assange.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/04/stakkastakka-25-assange.ogg'/>Puntata completa</a>

# nuova rubrica "regà mi son preso il malware"

- [spie e chiavette usb](https://arstechnica.com/tech-policy/2019/04/chinese-woman-arrested-at-trump-resort-had-hidden-camera-detector-8000-in-cash/)

# nuova rubrica "premio hackerino del mese"

- [il nuovo eroe](https://medium.com/@chrisbuetti/how-i-eat-for-free-in-nyc-using-python-automation-artificial-intelligence-and-instagram-a5ed8a1e2a10)
- [il vecchio eroe](https://www.vice.com/en_uk/article/434gqw/i-made-my-shed-the-top-rated-restaurant-on-tripadvisor)

# rubrica GAC (grazie al cazzo)

- [il voto su internet è rotto](https://www.schneier.com/crypto-gram/archives/2019/0415.html#cg1) [davvero](https://boingboing.net/2019/03/13/principal-agent-problems.html)

# nuova rubrica "intelligenza artificiale a pedali"

- [l'akkollo di ascolare Alexa](https://www.bloomberg.com/news/articles/2019-04-10/is-anyone-listening-to-you-on-alexa-a-global-team-reviews-audio)

# Assange

- [da guerre di rete](https://tinyletter.com/carolafrediani/letters/guerre-di-rete-newsletter-il-caso-assange-uk-e-i-contenuti-dannosi-i-meccanismi-ingannevoli-dei-social-e-altro)
