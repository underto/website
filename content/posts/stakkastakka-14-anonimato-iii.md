Title: stakkastakka #14 - Anonimato III
Slug: stakkastakka-14-anonimato-iii
Date: 2019-01-30 22:19:45
Category: stakkastakka
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/01/2019.01.28-stakka-stakka.mp3' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/01/2019.01.28-stakka-stakka.mp3'/>Puntata completa</a>


#### Posta del cuore dell'INTERNET

- [hackerare le gru](https://motherboard.vice.com/it/article/vbwg7m/trend-micro-come-hackerare-gru-cantiere-radiocomandi)
- [facebook e le "truffe amiche"](https://www.revealnews.org/article/facebook-knowingly-duped-game-playing-kids-and-their-parents-out-of-money/)
- [bug in apt](https://www.zdnet.com/article/nasty-security-bug-found-and-fixed-in-linux-apt/)
- [Citizen Lab](https://www.nytimes.com/aponline/2019/01/25/us/ap-cybersecurity-undercover-operatives.html)

#### Anonimato

Anonimato III, ripartiamo dal beneamato Ross.

- [Tutta la storia di Ulbricht](https://antilop.cc/sr/)


