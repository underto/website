Title: Stakkastakka #63 - INPS
Slug: inps
Date: 2020-04-08 12:26:02
Tags: stakkastakka

<audio controls>
	<source src='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-63.mp3' type='audio/mp3'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-63.mp3'/>Puntata completa</a>

## Posta del ♥ dell'Internet

Abbiamo fatto due chiacchere su coronavirus e soluzioni tech leggendo dei pezzi di un articolo curato da Carola Frediani su [Valigia Blu](https://www.valigiablu.it/coronavirus-emergenza-tecnologia/)

## INPS

<audio controls>
	<source src='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-63-inps.mp3' type='audio/mp3'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-63-inps.mp3'/>Puntata completa</a>


### iniziamo con una breve spiegazione di quanto successo:

come molte e molti di voi sapranno, una delle misure incluse nel decreto cura italia e' un bonus di 600 euro per partite iva, autonomi, artigiani, commercianti, etc. l'ente che in questi casi si deve occupare di queste questioni operativamente e' l'inps, che ha comunicato l'apertura di queste richieste di indennita' per l'1 aprile. solo che ha capito male la storia degli assembramenti e ha lanciato l'operazione #noassembramentidigitali visto che per tutta la notte
il sito e' andato a singhizzi, fino a chiudere del tutto i battenti in tarda mattinata. 
questo perche' ad un certo punto iniziano ad arrivare delle segnalazioni sul fatto che il sito mostra dati di altre persone. cioe' entranto per la richiesta di indennita' sul sito dell'inps, mettendo il pin e facendo due preghiere, venivano mostrati dati personali di altre persone.
se questo gia' sarebbe grave in generale, e' particolarmente grave perche' ha interessato il sito di un ente che gestisce e maneggia un'enorme mole di informazioni personali e che mannaggia dovrebbe garantire rigorose misure di sicurezza e non affidare al cuggino dello stagista tutte cose. L'ipotesi di gran lunga più probabile sull'accaduto è che ci sia stato un banale errore di configurazione sul sito, ma di questo ne parliamo dopo questo pezzo per darvi il buongiorno e mettervi nel giusto mood. buon ascolto.


### dichiarazioni pubbliche e reazioni istituzionali

- sul sito dell'inps, ad un certo punto si spegne tutto e viene mostrato un messaggio:
Al fine di consentire una migliore e piu' efficace canalizzazione delle richieste di servizio, il sito e' temporaneamente non disponibile. Si assicura che tutti gli aventi diritto potranno utilmente presentare la domanda per l'ottenimento delle prestazioni.
il tutto con un html raffazzonato, accrocchiatissimo con dei tag tipo il center che non si vedevano dal '94. con gli apostrofi sbagliati.
https://blog.mestierediscrivere.com/2020/04/01/in-tempi-difficili-parole-che-graffiano

comunque il succo dei messaggi e' come sempre, e' colpa vostra che siete arrivati tutti insieme, mica nostra. in generale non c'e' mai un'assunzione di responsabilita', un dire che ne so, raga scusate abbiamo fatto una cagata, c'era poco tempo, abbiamo dovuto arrangiarci e non abbiamo potuto fare delle prove. 
e' sempre il solito approccio per cui i cittadini sono visti come sospetti e potenziali criminali o alla meglio come bambini, più che come soggetti responsabili, capaci di collaborazione, dotati di neuroni, quando gli unici atteggiamenti infantili sono proprio quelli per cui si cerca sempre qualcuno a cui addossare la colpa, un capro espiatorio, il runner, quello che porta a pisciare il cane, in questo caso gli hacker.


ebbene si', hanno stato gli hacker!@@!@!1  "Abbiamo avuto nei giorni scorsi e anche stamattina violenti attacchi hacker", ha detto  Pasquale Tridico " questa la dichiarazione ufficiale di quanto accaduto dal presidente dell'INPS ripresa anche da conte. cosi' senza un minimo di pudore o vergogna, sparare cazzate per cercare di coprire la propria inettitudine. anche i bambini vi hanno sgamato. e' talmente grande e palese come cazzata che non ci hanno creduto neanche i giornali e infatti abbiamo fatto fatica a trovare l'audio perche' pochissimi giornali l'hanno ripresa come notizia. Un credulone che invece ci ha creduto e' il vicesegretario del Partito democratico, Andrea Orlando, in merito alla falla del sito dell'Inps denuncia che "alcune infrastrutture strategiche sono state sotto attacco di hacker" e su Twitter sollecita: "Bisogna subito convocare il Copasir per chiedere al Dis quale reazione è in atto. Questi sciacalli vanno fermati immediatamente". 
A questo punto piovono messaggi di scherno, anonymous italia scrive "vorremmo prenderci il merito, ma la verita' e' che siete incapaci e avete fatto tutto da soli togliendoci il divertimento", addirittura pornhub (per la sua immagine ovviamente) offre aiuto all'inps per potenziare il sito grazie ai suoi server.

### dopo il breach l'altro comunicato e' stato il seguente

"L’INPS informa gli utenti di avere prontamente notificato il data breach al Garante per la protezione dei dati personali ed assicura che, fin dal momento in cui si è avuta conoscenza della possibilità che vi sia stata violazione di dati personali, sta assumendo tutte le misure atte a porre rimedio alla situazione di rischio, attenuare i possibili effetti negativi e tutelare i diritti e le libertà delle persone fisiche. In tale ambito è istituita la seguente casella di posta elettronica violazionedatiGDPR@inps.it utilizzabile, esclusivamente dai soggetti i cui dati siano stati interessati dalla violazione, per le segnalazioni all’INPS, allegando eventuali evidenze documentali. L’Istituto si impegna a verificare tutte le segnalazioni ricevute e ad adottare ogni ulteriore misura tecnica e organizzativa adeguata di protezione dei dati personali che dovesse rendersi necessaria."

a parte che il sito dell'inps ha una normativa della privacy ferma al 2013, ancora non c'era la gdpr, forse non lo sanno che in questi casi sono loro a dover informare chi ha subito un leak e non viceversa. infatti le normative, in particolare la gdpr prevede sicuramente il nominativo di un responsabile dei dati personali e raga, l'inps non ce l'ha. l'inps non ha un responsabile dei dati personali di cosa stiamo parlando. anche il tabacchino sotto casa vostra deve averlo per le registrazioni di videosorveglianza. e invece no, il Data Protection Officer e' andato in pensione. Una delle altre questioni e' relativa alle risposte dopo un merdone cosi' che ad oggi si sono limitate alla creazione di una casella di posta dove poter inviare le segnalazioni.
Come scrive il giornalista Raffaele Angius su Wired citando un avvocato specializzato in tutela della privacy:
"Sembra quasi che la creazione della casella di posta elettronica sia essa stessa la misura adottata per risolvere il problema Di fatto mancano tutti i contenuti previsti dal Gdpr, che prescrive chiaramente l’identificazione di un responsabile e le soluzioni proposte per la risoluzione dell’incidente” Ma ancora di più qua si inverte l’onere dell’identificazione degli interessati coinvolti nella violazione: sarebbero infatti questi ultimi a dover ricevere un avviso nel quale si informa che i loro dati personali sono stati visibili a una quantità imprecisata di persone”.


### cosa e' successo in realta'

c'e' nel merito un succosissimo [thread su reddit](https://www.reddit.com/r/italy/comments/fuyvwd/sito_inps_niente_hacker_solo_una_scusa_per/)
da cui rubero' una spiegazione introduttiva per spiegare il problema:

Esistono principalmente due tipi di pagine web: quelle statiche e quelle dinamiche
La classica pagina statica potrebbe essere https://www.corriere.it/. Anche se può cambiare più volte nel corso della giornata, tutte le volte che la carichi tra una modifica e l'altra sia tu sia chiunque altro vedranno la stessa cosa
Una pagina dinamica invece è per esempio quella di gestione del tuo conto in banca. L'impaginazione e l'URL son gli stessi per tutti, ma il contenuto varia per ogni utente.
Per ottimizzare le performance di un sito il materiale statico può essere messo in cache, ovvero una memoria esterna più performante del server normale perchè non deve fare nessun controllo, semplicemente ti restituisce i dati che ha già visto. Una rete di cache viene generalmente definita CDN, o Content Delivery Network

Ecco il ragionamento che fa:
Tu: "Salve vorrei una mascherina per favore"
CDN: "Aspetti che controllo nel retro"
CDN: "Server di backend, abbiamo mascherine?"
Server Backend: "No"
CDN: "Mi dispiace abbiamo finito le mascherine"
D'ora in poi CDN sa che non ci sono mascherine e risponderà "no" subito a chiunque venga a chiederne
Quale è il problema? Che Server Backend non ha mai informato CDN che tra 10 minuti arriva il camion delle consegne che potrebbe consegnare altre mascherine, quindi CDN andrà beatamente avanti fino a sera a dire che "non abbiamo mascherine" anche se potrebbero essercene
Questo è quello che è successo con le pagine dell'INPS. Gli URL erano uguali per tutti, Server Backend non ha mai informato CDN "prossima volta che te lo chiedono torna comunque da me" e CDN semplicemente ha fornito la pagina di dettaglio a Mario Rossi, il primo in coda la mattina... e anche a tutti gli altri, che fossero Mario Rossi o no.

La mattina del 1 aprile, per qualche ora (e non "5 minuti" come ha dichiarato l'INPS), è stata impostata la CDN Microsoft Azure Edge davanti al server del sito dell'INPS.
www.inps.it era diventato un CNAME a inps-cdn-a.azureedge.net. Poi hanno ripristinato l'IP diretto senza CDN, "risolvendo l'attacco hacker" e il "data breach" (autoinflitti?!?!)
Ecco svelato il mistero, una volta per tutte. Niente "hacker", e nessun database fallito in modi improbabili e assurdi, come sosterrebbero alcuni sedicenti "esperti" (non esiste che si possano "confondere" le richieste al database...)
Ci sta solo un'incredibile (e ingiustificabile) incompetenza da parte sia di chi ha realizzato il portale dell'INPS senza nemmeno saper impostare gli HTTP Cache Headers, che anche da parte di chi l'altro giorno vi ha impostato una CDN davanti senza accorgersene prima.
Non è possibile che i dati personali di tutti gli italiani vengano affidati a gente come questa. Soprattutto sapendo che gli appalti (anche per il "nuovoportaleinps") sono costati addirittura 360 milioni di euro di soldi dell'INPS.

[Lista  dettagliata delle società che hanno vinto appalti da INPS](http://www.lanotiziagiornale.it/un-maxiappalto-da-170-milioni-ecco-la-cuccagna-dellinps/)


### Il sistema degli appalti nella storia dell'INPS

E torniamo sulle libere frequenze di radio blackout 105.25, siamo su stakkastakka e ricordiamo i contatti.
Come abbiamo detto in precedenza quindi le cause del collasso del sito INPS sono puramente tecniche, ma la politica istituzionale ha deciso comunque di inventarsi un  nemico, un gruppo di cattivi, i fantomatici hacker su cui far partire l'ennesima caccia alle streghe.
Questo ovviamente ricalca il copione vecchio e stantio di una politica che deve scaricare le proprie responabilità su un nemico invisibile.

Ma ancora più divertente della politica c'è Pornhub, che in un tweet ha chiesto all'INPS se avesse bisogno di un paio di server in prestito, o magari di una mano a configurarli.
Sicuramente a livello di marketing avrà funzionato, anche grazie al fatto che poggia su un assunto comune alla società capitalista, ovvero che in ogni caso il privato è molto più efficiente ed innovativo del pubblico.
In quest'ultimo pezzo vorremmo cercare di portare i dati degli appalti dell'INPS per sfatare un attimo il mito dell'eccellenza, perché se scaviamo un attimo in fondo scopriamo a fare i milioni sull'INPS non è stato il cugino con partita IVA del consigliere comunale.
Proprio nell'ottica di arginare il clientelismo nella spesa pubblica si stanno obligando gli enti ad emettere bandi di gara per affidare a terzi la propria infrastruttura, visto che assumere del personale competente sarebbe troppo inefficiente.
E allora via a lanciare gare pubbliche, l'INPS negli ultimi 20 anni ha pubblicato bandi nel settore informatico per quasi mezzo miliardo di euro.
Sembra una cifra non da poco, ma se il sito e tutta l'infrastruttura che c'è dietro fa tanto schifo, a chi sono andati tutti questi soldi?

Beh, possiamo iniziare a vedere uno dei bandi più ricchi, un maxiappalto da 170 milioni del 2013, diviso in sette lotti:

Finmeccanica. Il lotto 4, per complessivi 29,7 milioni di euro, è andato per esempio a un drappello di imprese in cui spicca Selex Elsag, del gruppo Finmeccanica. Al suo fianco troviamo la Hp Enterprise Services, che fa capo al colosso americano dell’informatica Hewlett Packard, e la multinazionale della consulenza Deloitte.

Il lotto 5, il cui valore finale si è attestato sui 24 milioni di euro, è stato conquistato da un raggruppamento in cui è presente Telecom Italia, il gruppo al tempo guidato da Franco Bernabè che raramente manca all’appuntamento delle grosse commesse di stato.


 Il lotto 2, che l’Inps remunererà con 33,8 milioni, ha visto tra i vincitori un gruppo particolarmente folto all’interno del quale si segnala la presenza degli spagnoli di Indra Sistemas (attraverso la Indra Italia). Accanto a loro compaiono la multinazionale Accenture, che ha sede a Dublino, e la Avanade Italy, ossia la sussidiaria italiana della joint venture che vede insieme la stessa Accenture e gli americani di Microsoft. 
 Accenture che si vanta anche sul suo sito di aver aiutato l'Istituto Nazionale della Previdenza Sociale (INPS) a modernizzare le applicazioni per promuovere le prestazioni e ridurre i costi, nella pagina (CASI DI SUCCESSO DEI CLIENTI),  consentendo all'ente di registrare una netta riduzione dei costi operativi e di sviluppo e di migliorare sia l'efficienza che la produttività.

Nomi di assoluto rilievo internazionale li troviamo anche all’interno del raggruppamento che ha incamerato il lotto 3, per un totale di 21,7 milioni di euro. In questo caso troviamo gli americani di Ibm, in compagnia dei francesi di Sopra Group e della multinazionale della consulenza Ernst & Young.

E visto che sono riusciti a spuntare un bella fetta di torta tutti i principali gruppi della consulenza e dei servizi Ict, nel novero non poteva mancare Kpmg, presente nel raggruppamento con Exprivia e Sintel Italia.

Alla cuccagna del superbando Inps del 2013, tra l’altro, hanno partecipato anche società di estrazione editoriale. Può sembrare un po’ strano, me è proprio così. Basta guardare alla storia del lotto 1, assegnato alla fine per 35,6 milioni di euro.
Al suo interno spunta Innovare 24 spa, che da qualche mese ha cambiato ragione sociale in 24 Ore Software e che altro non è se non la società informatica del Sole 24 ore, e quindi in ultima analisi del sistema Confindustria. Chiudono la composizione del lotto 1 Engineering spa e Inmatica spa. 
Insomma, solo per partecipare a questo bando la controllata del Sole24 ore ha preferito tuffarsi nel'informatica, e creare una divisione apposita, vedi come si scoprono le proprie passioni.

Insomma vediamo tante grandi società, multinazionali e aziende di consulenza quotate in borsa, non esattamente la municipalizzata in mano al cognato del consigliere comunale.
E queste mega aziende le ritroviamo nuovamente al banchetto dell'INPS, infattoi nel 2016 poi ci sarà un altro bando dell'INPS, stavolta con un valore stimato molto più succoso, di ben 359,9 milioni di euro:

- Leonardo, ex Finmeccanica, sarà nuovamente protagonista del bando del 2016, con i lotti 3 e 4, che comprendono la gestione di tutte le altre prestazioni dell’Inps (come sussidi, assegni di maternità, indennità), insieme ad Ibm, la società di revisione contabile e consulenza Ernst & Young e Sistemi informatici srl.

- Il gruppo di consulenza e revisione Kpmg, ritorna anche nel bando del 2016, con le società informatiche Exprivia, Wemake e Inmatica, e vince per 26 milioni di euro il lotto 5 (valore sulla carta: 41 milioni). A loro spetta tenere in piedi le reti dell’istituto: quindi integrazione dei dati, gestione del personale e fiscale, potenziamento dei servizi antifrode nonché, si legge nel capitolato, “l’evoluzione dell’infrastruttura di sicurezza e protezione dei dati”.

In questa voce dovrebbe rientrare il rispetto per l'INPS delle direttive europee sulla GDPR, che si preoccupano del rispetto della riservatezza e della privacy utente, la stessa GDPR per cui adesso l'INPS adesso rischia una multa di 20 milioni di euro.
E adesso chi la pagherà la maxi-multa, l'INPS o l'eccellenza della consulenza Kpmg, che ha già dichiarato di non aver lavorato direttamente sul sito in sé?

Eh già perché solitamente, come chiunque lavori nel campo vi portà confermare, le maxi-società che vincono appalti non lavorano mai sulle commesse, al massimo si preoccupano di "Business Intelligence", ovvero licenziare personale per evitare che un ente abbia delle maestranze competenti e sia ancora più dipendente dalla consulenza esterna, ma andranno semplicemente a subappaltarle a compagnie più piccole, le quali a loro volta andranno a subappaltare a terzi a prezzi ancora inferiori, fino a rivendere lo stesso lavoro ad una frazione del prezzo.

Il meccanismo per le grandi multinazionali è ben oliato: nell'emettere un bando si mettono una serie di prerequisiti che sono irragiungibili e ovviamente superlfui, come ad esempio avere qualsiasi certificazioni possibile (es il bando richiede certificazione CEH, anche solo un dipendente ha la CEH, quindi automaticamente l'azienda è certificata, anche se poi tizio non lavorerà al bando), o avere un centinaio di dipendenti laureati in ingegneria informatica o lavorare da 50 anni nel campo.

Ovviamente questi requisiti sono realistici solo per le mega-corporazioni, e dato che sono palesemente superflui, vengono poi sbolognati a compagnie più piccole che quei requisiti semplicemente non li hanno.
Solitamente i bandi includono delle clausole per impedire il secondo o terzo subappalto, ma questo è facilmente aggirabile quando le corporazioni decidono di chiedere a loro volta consulenze esterne, o assumono personale da agenzie di ricollocamento, che solitamente a livello legale risultano come cooperative, con contratti occasionali o a prestazione, che fanno però il lavoro di un consulente esterno.

Quindi, alla fine della catena alimentare, dopo il terzo o quarto subappalto, ci si ritrova con del personale che non ha mai lavorato a quel progetto in particolare, che dovrà reimparare come funziona tutta la bestia senza mai averla toccata, e che probabilmente sarà assunto a tempo determinato da una cooperativa o magari come libero professionista, che se dell'ammontare dell'appalto originale se ne ritrova un millesimo è già fortunato, e non essendo assunto sa già che a breve un'altra persona dovrà lavorare sul suo hackrocchio (RIP :'( insegna agli angeli a smarmellare  )


Questo meccanismo malato non è una peculiarità italiana, ma anzi è una regola perfettamente razionale nella logica capitalista, che se da un lato vede nell'ottimizzazione dei costi e dei tempi il pretesto per impoverire e precarizzare il lavoro, dall'altro accumula le risorse in degli oligopoli inamovibili, che non potranno mai subire una vera concorrenza dato che definiscono le regole del gioco.
A questo scopo potremmo citare altri data breach nel pubblico che sono stati ben più gravi del caso INPS in Italia, come per Equifax, l'agenzia delle entrate statunitense, o nel 2018 del NHS, il sistema sanitario inglese, ma anche no perché...

Come si diceva prima infatti, è palese che il codice oltre ad essere scritto con i piedi è anche  frutto del lavoro di una dozzina di persone che si sono susseguite nel tempo e non si sono mai parlate.
Capite che è ben diverso farsi curare dallo stesso medico o ritrovarsene uno diverso ogni giorno.
Il meccanismo è ben oliato anche per funzionare per mantenere la cricca dell'intermediazione, dove ovviamente se nessuno ha colpe queste al massimo verranno riversato sull'ultimno povero cristo.



