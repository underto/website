Title: stakkastakka #34
Slug: stakkastakka-34
Date: 2019-06-17 17:20:10
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/06/stakkastakka34.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/06/stakkastakka34.ogg'/>Puntata completa</a>

## Posta del cuore dell'internet
- [intel qualcomm against huawei](https://www.techradar.com/news/intel-and-qualcomm-lobby-against-huawei-ban)
- [Cellebrite dice che ti sblocca l'iOS](https://www.wired.com/story/cellebrite-ufed-ios-12-iphone-hack-android/)

## Approfondimento ISP Wiretapping
- [Telecom-Sismi / Tiger Team / dossieraggio all'italiana](https://it.wikipedia.org/wiki/Scandalo_Telecom-Sismi)
- [il garante e la wind](https://www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/1484695)
- [ancora su wind](https://www.ilfattoquotidiano.it/2009/12/15/lo-scandalo-wind-si-allarga-co/12589/)  
- [belgacom hack](https://theintercept.com/2018/02/17/gchq-belgacom-investigation-europe-hack/)
- [gli inglesi gchq spiano il primo ministro belga](https://www.theguardian.com/uk-news/2018/sep/21/british-spies-hacked-into-belgacom-on-ministers-orders-claims-report)
- [...e ancora sui servizi inglesi in belgio](https://www.brusselstimes.com/all-news/business/technology/51476/british-intelligence-hacked-belgacom-then-sabotaged-investigation/)
- [l'affaire greco](https://en.wikipedia.org/wiki/Greek_wiretapping_case_2004%E2%80%9305)
- [sempre sulla grecia](https://badcyber.com/the-great-greek-wiretapping-affair/)
- [...e ancora](https://spectrum.ieee.org/telecom/security/the-athens-affair)


- [(A Surprise Encounter With a Telco APT)](http://conference.hitb.org/hitbsecconf2017ams/materials/D2T4%20-%20Emmanuel%20Gadaix%20-%20A%20Surprise%20Encounter%20With%20a%20Telco%20APT.pdf)


