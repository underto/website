Title: stakkastakka #47 - Panoptico Lisergico
Slug: stakkastakka-47-panoptico-lisergico
Date: 2019-11-27 16:00:18
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/11/stakkastakka-47.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/11/stakkastakka-47.ogg'/>Puntata completa</a>
<br/>

#### Panoptico Lisergico

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/11/stakkastakka-47-bits-panoptico-lisergico.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/11/stakkastakka-47-bits-panoptico-lisergico.ogg'/>Panoptico Lisergico</a>
<br/>

- [Negli archivi dei ricercatori lisergici](https://theintercept.com/2019/11/24/cia-mkultra-louis-jolyon-west/\)
- [Riconoscimento facciale ed intelligenza artificiali](https://www.valigiablu.it/riconoscimento-facciale-intelligenza-artificiale/)

#### Hackback

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/11/stakkastakka-47-bits-hackback.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/11/stakkastakka-47-bits-hackback.ogg'/>HackBack</a>
<br/>

Dalle montagne del sud est cibernetico, una guida DIY per rubare alle
banche.  Torna Phineas Fisher o hackback, l'hacktivista responsabile
dell'attacco ad hacking team l'azienda milanese che si occupa di sicurezza
offensiva e vende trojan di stato a governi di tutto il mondo, anche il
nostro. Del leak di un paio di anni fa eravamo riusciti a rimettere in
piedi l'infrastruttura software alla base di galileo aka remote control
system di cui abbiamo scritto un approfondito documento sul nostro sito.

- [How to rob a bank](https://crimethinc.com/2019/11/17/hacking-back-how-to-rob-a-bank-expropriate-redistribute-and-leak-dispatches-from-the-digital-trenches)
- [Phineas fischer hacker antisistema](https://www.valigiablu.it/phineas-fischer-hacker-antisistema/)
- [Bounty for hacks](https://www.vice.com/en_us/article/vb5agy/phineas-fisher-offers-dollar100000-bounty-for-hacks-against-banks-and-oil-companies)

