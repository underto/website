Title: Nasce cisti.org
Slug: nasce-cisti-org
Date: 2019-03-26 00:33:31


Oggi nasce [cisti.org](https://cisti.org)!

Uno spazio digitale liberato.  
Un server scapestrato e autogestito.  
Anticapitalista, antifascista, antirazzista e antisessista.

Nei laboratori dell'hacklab underscore abbiamo pensato di chiamarlo cosi'
perche' crediamo sia importante prendersi cura innanzitutto delle comunita'
affini sul territorio e noi di questa grande famiglia -con tutti i suoi
scazzi e le sue divergenze- ci sentiamo sicuramente parte.

Cisti è un "contenitore" di strumenti radicali per scrivere, comunicare e
condividere. Radicale perchè vuole essere uno spazio autogestito che mette
al centro la sicurezza ed il benessere delle identità che lo vivono.

Nei prossimi mesi faremo degli eventi per presentare cisti.org in giro così
da discuterne apertamente insieme.
Questo 6 aprile saremo al CELS per [presentare cisti in
valle](https://gancio.cisti.org/event/30), nel mentre iniziamo gia' a
parlarne a grandi linee cosi' da poterne dare un'idea e cominciare a
raccogliere critiche e suggerimenti.

### Pad - [https://pad.cisti.org](https://pad.cisti.org)  
Il pad e' uno strumento che noi usiamo spesso e crediamo possa essere utile
a molte persone. Contiene servizi per gestire 
collaborativamente testi, progetti, consensi e altro.
Il pad ha molte funzionalita', che potete sperimentare
liberamente, ci soffermiamo solo sul fatto che tutto quello scritto
sul pad è cifrato ed impossibile da leggere o modificare per chiunque non
abbia il link.
Questo significa che nessuno puo' accedere ai vostri testi senza il vostro
consenso (nemmeno noi).


### Gancio - [https://gancio.cisti.org](https://gancio.cisti.org)
Gancio nasce da carta canta, ci è sembrata una gran bella idea e
abbiamo pensato che un posto che raccogliesse gli eventi fosse
fondamentale. Ne abbiamo parlato questo lunedi' 18 marzo su stakkastakka,
la trasmissione che curiamo su radioblackout.
Vi hanno cancellato l'evento su facebook? "potete scrivere sull'agendona
la polentata a canicatti' di sabato alle ventuordici?" "diofa ma ancora
nessuno ha mandato le date a carta canta?", avete organizzato due concerti
hardcore nella stessa serata? "cosa faccio stasera?"
gancio risponde a queste e altre domande.
Lo stiamo scrivendo noi da zero e lo adotteranno probabilmente altre citta'
(i bolognesi di bida.im gia' lo vogliono, i romani lo bramano, i milanesi
e i fiorentini li convinciamo facile :P). Insomma lo stiamo pensando
facilmente adottabile/adattabile da altre comunita', che ci sembra sia un
bisogno non solo torinese (e' software libero!).
L'abbiamo scritto in poche notti, quindi, soprattutto nel primo periodo,
se qualcosa non funziona e' normale. Scrivetecelo o aiutateci a risolverlo.


### Mastodon - [https://mastodon.cisti.org](https://mastodon.cisti.org)
Mastodon e' un social network. Sappiamo che per molti e' una brutta parola,
anche per molti di noi che sui social network commerciali non ci sono mai
stati.
I movimenti (e in generale le persone) hanno pero' bisogno di comunicare e
fondare la nostra comunicazione su piattaforme orientate esclusivamente al
profitto, al controllo e alla collaborazione con le forze di polizia non
e' una scelta lungimirante. Nella ricerca di strumenti validi verso altre
direzioni, abbiamo trovato mastodon. Mastodon non ha azionisti, e' della 
comunita', non e' controllato da nessuno centralmente, e' federato, 
non e' facile da censurare, non ti chiede i documenti ne' la
tua vera identita'.
Mastodon nasce circa tre anni fa con una prima forte spinta dalla comunita'
LGBT alla ricerca di uno spazio digitale dove potersi esprimere
serenamente.
Ora i nodi che fanno parte della rete mastodon sono tanti, più di 6mila con
circa due milioni di utenti. 
Spiegare mastodon in due righe non e' immediato, altrimenti non dovremmo 
alzare il culo per venire in giro a raccontarlo. Potete trovare
[qua](https://mastodon.cisti.org/about/more) alcune delle riflessioni che
ci hanno accompagnato in questo percorso. Potete anche guardare un breve
video esplicativo qua
[video](https://www.youtube.com/watch?v=IPSbNdBmWKE).

---
piccolo glossario per fuorisede:


### cisti
_Lo puoi utilizzare come vuoi, tipo "fai **cisti** che ti fai male" oppure
"fai **cisti** al palo mentre fai retro" anche se in realtà il termine
viene utilizzato piu che altro in contesti "drogheggianti"...tipo "fate
**cisti** se arriva qualcuno..." infatti la frase per intero è "**CISTI**
MADAMA (la polizia, i carabinieri....) CI HA VISTI!"_
da [https://www.bruttastoria.it/dictionary/cisti.html](https://www.bruttastoria.it/dictionary/cisti.html)

Famoso anche il servizio di radio blackout "**cisti** viaggiare
informati" per segnalare posti di blocco, controllori e in generale
presenze sgradite sul territorio


### gancio
_Se vieni a Torino e dici: "ehi, ci diamo un **gancio** alle 8?" nessuno
si presenterà con i guantoni per fare a mazzate. ... è un'espressione
tipica del torinese, darsi un **gancio** vuol dire beccarsi alle ore X in
un posto Y_

_A: dov'e' il **gancio** per andare al corteo domani?  
B: non so ma domani non posso venire, ho gia' un **gancio** per caricare la
distro._
liberamente adattato da
[https://forum.wordreference.com/threads/avere-un-gancio.2356288/](https://forum.wordreference.com/threads/avere-un-gancio.2356288/)
