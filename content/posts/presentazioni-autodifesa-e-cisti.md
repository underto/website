Title: Presentazioni autodifesa e cisti
Slug: presentazioni-autodifesa-e-cisti
Date: 2019-04-03 15:56:14
Tags: autodifesa, cisti


![manituana c1](images/manituana-c1.png)

Usciamo dai laboratori dell'hacklab _TO ed entriamo nel camper per lo
straordinario tour di presentazioni di alcuni dei progetti che ci hanno
fatto faticare in questi ultimi mesi!

> Tutti questi incontri e tutti (i numerosi) che verranno sono warm-up in
> vista dello splendido [hackmeeting](https://www.hackmeeting.org/) che
> quest'anno si terrà a Firenze presso il Next Emerson dal 30 Maggio al 2
> Giugno!
>
> Siateci!

#### 6 Aprile, alle 16:30 @CELS

[Presentazione di cisti.org](https://gancio.cisti.org/event/30)

#### 9 Aprile, alle 17:00 @MANITUANA

[Corso di autodifesa digitale](https://gancio.cisti.org/event/139)

#### 11 Aprile, alle 17:00 @AULA C1

[Presentazione di cisti.org](https://gancio.cisti.org/event/119)
