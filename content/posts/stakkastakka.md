Title: stakkastakka
Slug: stakkastakka
Tags: stakkastakka, rbo, radio
Date: 2018-11-06 00:30:32

Abbiamo cominciato a trasmettere sulle mitiche libere frequenze di [radio
blackout](https://radioblackout.org) con una trasmissione di critica 
delle nuove tecnologie e approfondimenti digitali, si chiama stakkastakka.  
Per ora tutti i lunedì dalle 13 alle 15 sui 105.25fm (ovviamente solo
torino e dintorni) o in streaming sempre [qui](https://radioblackout.org/)

Durante le trasmissioni potete interagire con noi in tempo reale, ci trovate su IRC nel canale `#stakkastakka` del server
di autistici ([qui trovate come
fare](https://www.autistici.org/docs/irc/#)).

Ci uniamo quindi alla combricola acaroradiofonica che vede tra le sue fila:

- [le dita nella presa](https://www.ondarossa.info/trx/dita-nella-presa)
- [coccoli al
  silicio](https://wombat.noblogs.org/category/podcast/coccoli-al-silicio/)
- [hack or die](https://hackordie.gattini.ninja/)

e quindi stay tuned, qui trovate i [podcast](/tag/stakkastakka.html)
