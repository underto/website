Title: Un Abbraccio ad EigenLab
Slug: un-abbraccio-ad-eigenlab
Date: 2019-04-18 12:17:15



Vi scriviamo per informarvi della raccolta fondi che eigenLab ha da poco lanciato. Come probabilmente saprete, da quasi un anno l'amministrazione centrale dell'Università di Pisa ha spento l'interruttore che fornisce energia elettrica al nostro laboratorio nel Polo Fibonacci, dopo anni di convivenza fertile in quel luogo. Molte (anche se non tutte) le attività che il collettivo porta avanti hanno subito un brusco arresto: ai link in calce potete leggere i comunicati che abbiamo scritto per raccontare tutta la vicenda [1-3]. La nostra intenzione è quella di continuare a vivere quel posto, perché riteniamo fondamentale mantenere aperti spazi di scambio e autoformazione in una università sempre più chiusa alle esperienze devianti. Per questo motivo vogliamo installare dei pannelli solari sul tetto del nostro casottino che ci permettano di usare almeno un computer e di illuminare il posto di sera: per realizzare ciò ci serve l'aiuto di tutt coloro che possano contribuire per permetterci di raggiungere il nostro obiettivo. Questa [https://eigenlab.org/pannellisolari/](https://eigenlab.org/pannellisolari/) è la pagina relativa alla campagna raccolta fondi con informazioni su come effettuare una donazione, e se ovviamente un aiuto in tal senso è molto gradito, saremmo altrettanto content se decideste di diffondere la chiamata alle vostre conoscenze.

Vi ringraziamo per tutto l'aiuto che darete, Ci vediamo nelle lotte! I ragazzi e le ragazze di eigenLab.

[1] [https://eigenlab.org/2018/06/verranno-al-contrattacco-con-elmi-ed-armi-nuove/](https://eigenlab.org/2018/06/verranno-al-contrattacco-con-elmi-ed-armi-nuove/)  
[2] [https://eigenlab.org/2018/07/che-lignoranza-fa-paura-ed-il-silenzio-e-uguale-a-morte/](https://eigenlab.org/2018/07/che-lignoranza-fa-paura-ed-il-silenzio-e-uguale-a-morte/)   
[3] [https://eigenlab.org/2018/08/eppur-si-muove/](https://eigenlab.org/2018/08/eppur-si-muove/)



