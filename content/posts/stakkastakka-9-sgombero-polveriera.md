Title: stakkastakka #9 Sgombero Polveriera
Slug: stakkastakka-9-sgombero-polveriera
Date: 2018-12-17 11:34:33
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://cdn.radioblackout.org/wp-content/uploads/2018/12/stakkastakka-9-sgombero-polveriera.ogg' type='audio/mpeg'>
</audio>
<a href='https://cdn.radioblackout.org/wp-content/uploads/2018/12/stakkastakka-9-sgombero-polveriera.ogg'/>Puntata completa</a>


#### Posta del cuore dell'INTERNET

- [Attacco Saipem](https://web.archive.org/web/20181215183941/http://www.saipem.com/en_IT/static/documents/PR%20Saipem%2010_1_2018.pdf)
- [Malware distribuiti in progetti open](https://www.schneier.com/blog/archives/2018/11/distributing_ma.html)
- [Teorema della possibilità peggiore](https://www.schneier.com/blog/archives/2018/11/worst-case_thin_1.html)
- [Di Tumblr, polli e Ramsey](https://motherboard.vice.com/it/article/a3mn7p/algoritmo-tumblr-segnala-come-porno-contenuti-a-caso)
- [Bug Google+](https://thehackernews.com/2018/12/google-plus-hacking.html)
- [Bug Facebook API](https://securityaffairs.co/wordpress/78913/social-networks/facebook-photo-api-bug.html)
- [Dati localizzazione da Android](https://www.nytimes.com/interactive/2018/12/10/business/location-data-privacy-apps.html)

