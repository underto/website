Title: stakkastakka #60 - Sopravvivenza digitale
Slug: stakkastakka-60-sopravvivenza-digitale
Date: 2020-03-18 12:20:19
Tags: stakkastakka

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/03/stakkastakka-60.mp3' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/03/stakkastakka-60.mp3'/>Puntata completa</a>

## Didattica al tempo del covid19

Intervista ad il collettivo Circe registrata dall'hacklab unit di Milano.
Parliamo di didattica al tempo di covid19 e l'accelerazione in corso nei
processi educativi.  Consigliamo di ascoltare la [puntata
integrale](https://lost.abbiamoundominio.org/2020/loco19radio.html) di
bitume.

Qua alcuni link utili per continuare ad approfondire il tema:

- [Internet, Mon Amour](https://ima.circex.org)
- [Pillole di informazione digitale, di
  Graffio](http://www.graffio.org/pillole.html)
- [Strumenti per didattica a distanza, di Ferry
  Bite](https://scaccoalweb.wordpress.com/2020/03/04/le-videochat-didattiche-al-tempo-del-coronavirus/)
- [Guida alla sopravvivenza digitale su radio onda
  rossa](https://www.ondarossa.info/newstrasmissioni/2020/03/guida-sopravvivenza-digitale-nellepoca)
  in cui si elencano e si spiegano un po' di alternative alle grandi
  piattaforma da usare in questi periodi.

## Tecnologia da coronavirus

- [App per scovare chi esce](https://www.repubblica.it/economia/2020/03/17/news/app_coronavirus-251422094/)
- [Droni per individuare chi esce di casa](https://corrieredibologna.corriere.it/bologna/cronaca/20_marzo_16/coronavirus-emilia-romagna-forli-droni-individuare-chi-esce-casa-a4b74cc2-67b4-11ea-b58a-1d5170becbeb.shtml)

## Chelsea Manning

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/03/stakkastakka-60-manning.mp3' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/03/stakkastakka-60-manning.mp3'/>Approfondimento Chelsea Manning</a>


In seguito ad un [tentato
suicidio](https://theintercept.com/2020/03/12/chelsea-manning-suicide-attempt/)
è arrivato [l'ordine di scarcerazione per Chelsea
Manning](https://theintercept.com/2020/03/12/chelsea-manning-judge-order-released/)

