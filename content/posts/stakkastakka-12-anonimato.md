Title: stakkastakka #12 - Anonimato
Slug: stakkastakka-12-anonimato
Date: 2019-01-14 18:34:49
Tags: stakkastakka

![tor](images/tor.jpg)

#### POSTCAST

<audio controls>
  <source src='https://cdn.radioblackout.org/wp-content/uploads/2019/01/stakkastakka-12-anonimato.ogg ' type='audio/mpeg'>
</audio>
<a href='https://cdn.radioblackout.org/wp-content/uploads/2019/01/stakkastakka-12-anonimato.ogg '/>Puntata (quasi) completa</a>

#### La posta del cuore dell'INTERNET

- [La gang cinese che scippa i milioni](https://m.economictimes.com/tech/internet/how-chinese-hackers-pulled-off-the-italian-con-job-a-rs-130-crore-heist/amp_articleshow/67464588.cms)
- [Cartografi che creano case degli orrori](https://gizmodo.com/how-cartographers-for-the-u-s-military-inadvertently-c-1830758394)

#### Anonimato

- [Studente di Harvard viene preso usando tor](https://www.theverge.com/2013/12/18/5224130/fbi-agents-tracked-harvard-bomb-threats-across-tor)
- [Duro colpo ad anonymous Italia](http://digitalcorner.altervista.org/2015/05/duro-colpo-per-anonymous-italia/)
- [Usate bene tor](http://privacy-pc.com/articles/dont-fuck-it-up-4-use-tor-the-right-way.html)



