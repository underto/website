Title: stakkastakka #40 - Palantir
Slug: stakkastakka-40-palantir
Date: 2019-09-23 18:40:55
Tags: stakkastakka


#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/09/stakkastakka-40.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/09/stakkastakka-40.ogg'/>Puntata completa</a>
<br/>


## Posta del ♥ dell'Internet

- [Hanno rotto RSA?](https://arstechnica.com/information-technology/2019/09/medicine-show-crown-sterling-demos-256-bit-rsa-key-cracking-at-private-event/)

## Palantir

- [Cos'è Palantir](https://www.theverge.com/2018/2/27/17054740/palantir-predictive-policing-tool-new-orleans-nopd)
- [US Intelligence](https://www.theverge.com/2019/7/31/20746926/sentient-national-reconnaissance-office-spy-satellites-artificial-intelligence-ai)

