Title: stakkastakka #56 - Scienza e Malessere
Slug: stakkastakka-56-scienza-e-malessere
Date: 2020-03-01 14:04:37
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/02/stakkastakka-56.mp3' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/02/stakkastakka-56.mp3'/>Puntata completa</a>
<br/>

  Puntata densa e monografica sul nesso tra lavoro nella ricerca e malessere. Grazie al prezioso contributo di Angelo Piga e Aniello «Nello» Lampo autori di ["Stare male nell'accademia"](https://not.neroeditions.com/accademia-ricerca-malessere-psichico/) attraversiamo le contraddizioni di un segmento del capitalismo cognitivo. Incominciamo con alcuni dati sulla situazione italiana ed internazionale sul mondo accademico per poi spostarci tra le pieghe dei malesseri e finte soluzioni di una professione radicalmente precarizzata, gamificata e prestazionale. Qualche link di approfondimento usato per la preparazione della puntata.

* [L'Inverno caldo dei ricercatori universitari](https://jacobinitalia.it/linverno-caldo-dei-ricercatori-universitari/)
* [Accademia e Depressione di Franco Palazzi](https://www.iltascabile.com/societa/accademia-e-depressione/)
* [L'età dell'inadeguatezza di Francesca Coin](https://www.che-fare.com/eta-inadeguatezza-burnout-ricerca-francesca-coin/)
* [Il flagello dei programmi di wellness aziendali](https://www.dinamopress.it/news/flagello-dei-programmi-wellness-aziendali/)

