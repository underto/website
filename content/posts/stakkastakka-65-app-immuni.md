Title: Stakkastakka #65 - App Immuni
Slug: app-immuni
Date: 2020-04-22 22:30:00
Tags: stakkastakka

<audio controls>
<source src=' https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-65.mp3' type='audio/mp3'>
</audio>
<a href=' https://radioblackout.org/wp-content/uploads/2020/04/stakkastakka-65.mp3'/>Puntata completa</a>

Una panoramica sul funzionamento della app Immuni e le sue innumerevoli impossibilità.
PERCHE' FARLA? SERVE FARE QUALCOSA! LA APP E' QUALCOSA! RISOLTO!
Servirebbe per contenere i contagi, lo scopo non è seguire i movimenti degli individui, né quello di far rispettare prescrizioni.

### ma come funzionerebbe? la teoria
[https://ncase.me/contact-tracing/](https://ncase.me/contact-tracing)


### alcune domande a cui abbiamo cercato di dare una risposta trattate nella puntata:
- Usare il bluetooth come sistema per tracciare un contatto e' un buon modo? 'nsomma
- Si riuscirà ad arrivare al 60% di installazioni? Impossibile
- Ma tecnicamente si può fare? Ad ora no, soprattutto con Apple.
- E la privacy?
- Ma se non serve a limitare il contagio, a che serve?
