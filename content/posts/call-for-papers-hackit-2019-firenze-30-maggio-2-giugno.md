Title: Call for papers HACKIT 2019 - Firenze, 30 Maggio - 2 Giugno
Slug: call-for-papers-hackit-2019-firenze-30-maggio-2-giugno
Tags: hackit
Date: 2019-04-22 22:18:15

####INTRO####

Nell'ultimo hackmeeting ospitato a Firenze nel 2011 i Maya annunciavano
la fine dei tempi. Nonostante l'umanità appaia lavorare alacremente in
questo senso, per ora siamo in stand by: persistiamo con ironia
nell'inferno
dei viventi.
I tempi non somo troppo felici, nè facili, ma non per questo abbiamo perso
la voglia di giocare e smontare il mondo. Grosse nubi di ignoranza e
velleità
autoritarie producono una pioggia acida che corrode la società tutta.
Serve un ombrello robusto, per questo ci prepariamo ad un nuovo
hackmeeting:

30/31 maggio, 1/2 giugno a Firenze,
presso il [csa nEXt Emerson](http://www.csaexemerson.it).

Dobbiamo fare la nostra parte, e giocare dalla parte giusta.

Se una visione critica della tecnologia è sempre stata necessaria,
oggi vorremmo mettere l'accento sul metodo che negli anni abbiamo proposto
come linea guida del nostro agire: l'autogestione.

A un modello sociale edificato sulla paura e l'emergenza, sul profitto
elevato a ago della bilancia dell'umane vicende e incantesimo di guarigione
per qualsiasi male, vorremmo opporre il nostro modo di stare al mondo
condividendo beni e saperi. Non vorremmo parlarne nei termini  di una vaga
idea
o generica linea guida o aspirazione, ma in qualità di pragmatica
capacità organizzativa basata sulla solidarietà e la messa in comune di
conoscenze, metodi e mezzi.
Crediamo fortissimamente nella contaminazione delle discipline:
dall'informatica alla fisica, dalla meccanica alla filosofia,
dall'agricoltura
alla matematica, dalla logica all'arte, dall'architettura all'antropologia,
e continuate voi con gli abbinamenti che preferite.

Siamo persone curiose in ogni campo, e riconosciamo la necessità di
intrecciare
le esperienze per indagare la complessità del reale, senza arroganti
semplificazioni di comodo.

Per farsi un'idea degli argomenti trattati ad hackit puoi leggere i
programmi
degli anni passati su [https://www.hackmeeting.org](https://www.hackmeeting.org)
e [chi siamo](https://www.hackmeeting.org/hackit19/) e [altre
info](https://www.hackmeeting.org/hackit19/info.html).

Se condividi il nostro approccio, e vuoi partecipare a questo gioco
che dura 4 giorni, ma più probabilmente tutta la vita, ti chiediamo
di impegnarti un poco per produrre dei contenuti: più la comunità
contribuisce,
più hackit sarà interessante.

<br/> 
####PROPORRE UN INTERVENTO####

Allestiremo tre sale, più un quarto spazio jolly, per discussioni
improvvisate
o per seminari che sforano sull'orario previsto. Non ci sono rigide
indicazioni riguardo alla durata degli interventi, tieni però presente che
sebbene il tempo sia una dimensione dell'anima, la fisiologia umana a
ricondurlo
a una certa finitezza del medesimo, inoltre incastrare una chiaccherata di
31  minuti e 14 secondi in una griglia oraria è molto più difficile
rispetto
ad una di 1 o 2 ore.

Per orientarsi possiamo offrire qualche indicazione basata sulla più che
ventennale esperienza di hackit.


<br/>
####TALK BREVE####

Se l'intervento è più una suggestione o non te la senti di parlare per
troppo tempo, sono previsti alcuni momenti dedicati ai "ten minutes talks".
Di solito si tengono a fine giornata, nella sala più capiente
e qualcuno si occuperà di segnalare lo sforamento eccessivo dei dieci
minuti.
Puoi segnarti direttamente in loco durante hackit, oppure iscriverti alla
lista

hackmeeting(at)inventati.org

e mandare una mail con subject

[TEN] titolo

nel messaggio una breve spiegazione

<br/>
####TALK LUNGO####

Se invece intendi proporre un intervento più lungo

manda una mail con subjet

[TALK] titolo

e le seguenti informazioni nel messaggio:

- Durata prevista
- Orario/Giorno preferito
- Breve Spiegazione
- Eventuali link e riferimenti utili
- Nickname
- Lingua

La griglia degli orari viene creata, comunicando via via i cambiamenti in
lista
cercando di rispettare le preferenze in termini di orari/giorni.

Durante hackit verrà delineato un percorso di base, quindi non intimidirti:
divulgativo o
tecnoSciamanicoDiIncomprensibileArgomentoDiOscuraMagiaNeraIntriso,
se fatto con passione e impegno tutto può avere un senso e essere utile per
qualcun'altro.


Se non hai un seminario da proporre, ma ti piacerebbe sentire parlare di
qualche argomento in particolare, può seguire gli stessi passaggi indicati
per la proposta dei talk, ma usare come subjet

[TALK RICHIESTA] titolo

e una breve spiegazione della richiesta


