Title: stakkastakka #13 - Anonimato II
Slug: stakkastakka-13-anonimato-ii
Date: 2019-01-21 17:10:41
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/01/stakkastakka-13-anonimato.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/01/stakkastakka-13-anonimato.ogg'/>Puntata completa</a>


#### Posta del cuore dell'INTERNET

- [Nazi honeypot](https://www.dw.com/en/german-artists-reveal-truth-behind-neo-nazi-doxing-campaign/a-46606772)
- [Alter Attack](https://alter-attack.net/)
- [Seawatch cerca hacker](https://motherboard.vice.com/it/article/pa57wn/sea-watch-navi-ong-uso-software-e-hardware-per-salvare-vite-in-mare-35c3)
- [Algoritmi a stormo](https://www.wsj.com/articles/behind-the-market-swoon-the-herdlike-behavior-of-computerized-trading-11545785641)

#### Anonimato

- [Arresto Ross Ulbricht](https://www.ilpost.it/2013/10/04/arresto-ross-ulbricht-silk-road/)
- [La vita di Ulbricht](https://motherboard.vice.com/it/article/qk35ax/la-vita-segreta-di-una-delle-menti-dietro-silk-road-20)
- [Tutta la storia di Ulbricht](https://antilop.cc/sr/)

