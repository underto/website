Title: E tu di che tecnologia ti fai?
Slug: e-tu-di-che-tecnologia-ti-fai
Date: 2019-12-16 15:05:27

Ultimo appuntamento per il ciclo <a href='https://radioblackout.org/?s=pulp'>PULP - La polpa dei libri.</a> alla **Blackout House**.  
**E tu di che tecnologia ti fai? Umani e non umani, storia di una relazione** un radiodramma dal libro:
<a href='https://circex.org/ima/'>Internet, Mon Amour</a>  
cronache prima del crollo di ieri (di Agnese Trocchi)

Mercoledì 18 Dicembre, h 21 alla Blackout House in via Cecchi 21/a

Un libro frutto degli incontri e delle presentazioni di C.I.R.C.E alla ricerca di un rapporto con le macchine non mediato dalle tecnologie del dominio.
Racconti di ordinario (ab)uso tecnologico, raccolti in una cornice di fantascienza speculativa. Commentati e ordinati in cinque giornate: fuoricasa, relazioni, sex, truffe e una conclusiva ricreazione.
Occorrente per partecipare: voglia di mettersi in gioco.

<center>
<a href='https://gancio.cisti.org/event/645'><img src='{static}/images/ima@rbo.png' style='height: 80vh;'
/></a>
</center>
<center>
<br/><br/><br/>
<iframe style='align: center; text-align:center; border: 0; width: 100%; height: 215px;' src="https://gancio.cisti.org/embed/645"></iframe>
</center>






