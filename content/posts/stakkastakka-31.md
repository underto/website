Title: stakkastakka #31
Slug: stakkastakka-31
Date: 2019-05-27 17:10:57
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/05/stakkastakka-30-1.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/05/stakkastakka-30-1.ogg'/>Puntata completa</a>

## Posta del cuore dell'internet

- [Uber e Lyft aumentano il traffico](https://www.theverge.com/2019/5/8/18535627/uber-lyft-sf-traffic-congestion-increase-study)
- [Pure BOSE ci spia dalle cuffia](https://www.reuters.com/article/us-bose-lawsuit-idUSKBN17L2BT)
- [Autisti di Uber e Lyft spengono app per alzare i prezzi](https://wjla.com/news/local/uber-and-lyft-drivers-fares-at-reagan-national)
- [Gli USA accusano Assange di spionaggio](https://www.valigiablu.it/accuse-trump-assange-giornalismo/)
- [La metro di Londra fa cose](https://www.ilpost.it/2019/05/23/metropolitana-londra-smartphone/)
