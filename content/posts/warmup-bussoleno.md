Title: Ancora Warmup Hackmeeting 0x14
Date: 2017-01-21 00:00
Category: news
Tags: 
Slug: warmup-bussoleno
Authors: underto
Summary:25 Febbraio @ Salone Polivalente Bussoleno, WarmUp Hackmeeting 0x14

Vi invitiamo questo sabato 25 febbraio dalle 16 al Salone Polivalente di Bussoleno 
per un evento preparatorio ad [Hackmeeting 0x14](http://hackmeeting.org) (che ricordiamo quest'anno si terrá
 qui vicino in Val Susa, in particolare a Venaus, dal 15 al 18 giugno).

Ci sará il collettivo [Ippolita](http://ippolita.net) che parlerá di social network e tecnologie del dominio,
a seguire un avvocato fará una panoramica sull'utilizzo dei captatori informatici, 
mentre noi come _TO hacklab mostreremo nella pratica l'utilizzo di un captatore informatico.

A seguire domande, chiacchiere e condivisione di idee in preparazione di Hackmeeting.

![locandina](images/bussoleno-25feb-thumb.jpg)

Vi aspettiamo
