Title: stakkastakka #3 - Aaron Swartz
Slug: aaron-swartz
Date: 2018-11-11 23:51:39
Tags: stakkastakka, aaron swartz


#### POSTCAST
<audio controls>
  <source src='https://stakkastakka.fugadalcontrollo.org/stakka-stakka-3-05-11-18.mp3' type='audio/mpeg'>
</audio>
<a href='https://stakkastakka.fugadalcontrollo.org/stakka-stakka-3-05-11-18.mp3'/>Puntata completa</a>

<br/>
#### NEWS, abbiamo parlato di:
- [profilare baby sitter dal web](https://theoutline.com/post/6533/dont-let-the-robot-stalk-your-babysitter)
- [account facebook di senatori](https://news.vice.com/en_us/article/xw9n3q/we-posed-as-100-senators-to-run-ads-on-facebook-facebook-approved-all-of-them)
- [dipartimento di stato US e messaggi cifrati](https://www.washingtonpost.com/news/powerpost/paloma/the-cybersecurity-202/2018/10/18/the-cybersecurity-202-leak-charges-against-reasury-official-show-encrypted-apps-only-as-secure-as-you-make-them/5bc74eaa1b326b7c8a8d1a3a/?utm_term=.58d3873e3276)
- [sistemi di sorveglianza made in israel](https://www.ilpost.it/2018/11/04/israele-sistemi-sorveglianza/)
- [realtà virtuale](https://theoutline.com/post/6443/virtual-reality-dream-is-dead-hype-oculus-rift-facebook-playstation)
- [machine learning blowjob](https://motherboard.vice.com/it/article/pa9nvv/autoblow-ai-sex-toy-studio-machine-learning-sesso-orale)

#### APPROFONDIMENTO

- [Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz)




