Title: HackЯocchio - 10 Giugno 2023 @Mezcal Squat
Slug: hackЯocchio-10-giugno-2023-mezcal-squat
Date: 2023-06-07 09:21:00

> <b><a href='hackrocchio.org'>HackЯocchio</a></b> <small><i>s. m. [der. di hackЯocchiare] (pl. -chi)</i></small>  
> <i>situazione o soluzione raffazzonata, raccogliticcia, approssimativa</i>

<strong>
Sabato 10 Giugno dalle ore 14.
[Mezcal Squat](https://mezcalsquat.net/), Parco della Certosa, Collegno (TO).  </strong>

Costruire comunità resistenti complici nelle lotte, saldare relazioni di fiducia tra le moltitudini sommerse nell'estrattivismo cognitivo, riuscire ad immaginare e a costruire tecnologie conviviali che sostituiscano le megamacchine digitali. Non promettiamo la rivoluzione ma il crepuscolo degli dei, per farlo inietteremo lo shellcode nel paese reale, andando nelle case ma sopratutto nei cuori di chi ancora ci crede, a decifrare le blockchain corrotte di un sistema che nulla ha più di umano se non la scalabilità del vostro frigorifero nel cloud, insomma, in una parola, quantum machine learning :)

se non ci hai capito molto, neanche noi, ma trovi tutte le informazioni e il [progamma](https://hackrocchio.org/programma) su [https://hackrocchio.org/info](https://hackrocchio.org/info).


## __Programma__


### Seminari dalle 14 di sabato

<br/>

#### Radioline

Cosa sono le radioline DMR? Digital Mobile Radio, costi, compromessi, vantaggi, svantaggi e casi d’uso: dalla montagna alla piazza.
A seguire laboratorio SDR, capiamo insieme cosa c’e’ nella banda, usciamo il kraken per vedere le onde.


<br/>

#### Osservatorio nessuno

Cosa hanno in comune l’ISIS, il processo Ruby Ter, una azienda che fa i tornelli per le palestre, una sgangherata cantina e una inconsueta congrega? Breve viaggio attraverso Tor, il (quasi) neonato Osservatorio Nessuno e quello che deve ancora succedere.

<br/>

#### Ok Google, aggira l’autenticazione
Spesso le applicazioni vengono realizzate tramite accrocchi di componenti appiccicati tra loro con lo sputo. A volte, la complessità di queste costruzioni porta con sé la formazione di interstizi digitali tramite i quali un input apparentemente innocuo può raggiungere insperabili profondità. Vedremo come un “apriti sesamo” pronunciato ad alta voce ha permesso di bypassare completamente un’appicazione di gestione delle bollette su Google Voice.

<br/>

#### Tentativi di tecnologie conviviali

Negli ultimi anni ci siamo spesso interrogati su come recuperare spazi di manovra in un contesto digitale su cui non abbiamo alcun potere.
Anche per questo autogestiamo i servizi di cisti.org.
Hackrocchio sembra ottimo per ritagliarci un momento di restituzione su ragionamenti e aggiornamenti fatti sui vari attrezzi che
riteniamo sensati da suggerire e su cui ci piacerebbe ricevere suggestioni.
una carrellata su gancio.org, zecche, plaid 

<br/>

#### App moleste sul lavoro
A volte non solo ci tocca dover lavorare, ma ci costringono pure ad installare un’app per farlo!
Applicazioni moleste che ci controllano, valutano, sanzionano e appioppano mansioni in nome della Produttività.
Ma cosa fanno di preciso sul nostro telefono?
Cercheremo di scoprirlo analizzando alcune app fornite ai rider da delle note aziende, monitorandone il comportamento con i potenti strumenti recuperati dagli anfratti dell’internet.

<br/>


## Laboratori

#### laboratorio bicicultura
La bici è un mezzo che sfida l’urbanistica e la società dei consumi, con il quale riappropiarsi e attraversare diversamente gli spazi cittadini. Ma se oltre a tutto questo ci slegasse anche dal ruolo di clienti? Il suo semplice funzionamento, infatti, permette di fare la maggior parte delle manutenzioni con pochi attrezzi, in questo spazio proporremo una soluzione agli inconvenienti che possono capitare mentre si pedala e alle riparazioni da fare, imparando insieme a conoscere i nostri mezzi. Condividi ciò che sai, apprendi ciò che ti manca

<br/>

#### laboratorio fotovoltaico
panoramica componenti, tecnologie, materiali e dimensionamenti. Esempi pratici e dimostrazioni live: dal fotone all’elettrone.

<br/>

#### laboratorio pizza

dal primo pomeriggio per avviare il forno e poi la sera pizza bellavita* (cena di condivisione).

<br/>



## Ore 20
Pizza [bellavita](https://www.mezcalsquat.net/wp-content/uploads/2023/05/Bellavita-2022-daje-scaled.jpg), porta il tuo condimento preferito.

## Ore 22
Concerto [RWA, Riderz With Attitude](https://soundcloud.com/user-720872153)

## Dalle 23 
Ten minutes talk


(sì, ci si puo' fermare a [dormire](https://hackrocchio.org/info#dormire))
