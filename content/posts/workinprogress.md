Title: Work In Progress
Date: 2016-02-10 18:30
Modified: 2016-02-12 18:30
Category: news
Tags: WIP
Slug: work-in-progress
Authors: underto
Summary: Lavori in corso in Via Millio!


Stiamo ricostruendo l'hacklab (e il sito, e tutto quanto). Se vuoi darci una mano ci trovi al primo piano del CSOA Gabrio, in Via F. Millio 42 a Torino. Non c'è (ancora) un orario stabilito, la cosa migliore è contattarci con una mail a underscore[at]autistici[dot]org . A presto!
