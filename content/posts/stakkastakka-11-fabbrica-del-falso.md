Title: stakkastakka #11 - Fabbrica del Falso
Slug: stakkastakka-11-fabbrica-del-falso
Date: 2019-01-07 01:33:52
Tags: stakkastakka

![cybertonia](images/internet-fake.jpg)

#### POSTCAST
<audio controls>
  <source src='https://cdn.radioblackout.org/wp-content/uploads/2019/01/stakkastakka-11-la-fabbrica-del-falso.ogg' type='audio/mpeg'>
</audio>
<a href='https://cdn.radioblackout.org/wp-content/uploads/2019/01/stakkastakka-11-la-fabbrica-del-falso.ogg '/>Puntata completa</a>

#### La posta del cuore dell'INTERNET

- [Facebook vende più dati del previsto](https://motherboard.vice.com/it/article/9k4vda/inchiesta-new-york-times-facebook-accordi-commerciali-dati-utenti-spotify-netflix-amazon-microsoft-apple)
- [Facebook chiude account palestinesi](https://theintercept.com/2017/12/30/facebook-says-it-is-deleting-accounts-at-the-direction-of-the-u-s-and-israeli-governments/)
- [Esperti Machine Learning al MISE?](http://www.ansa.it/sito/notizie/tecnologia/hitech/2018/12/27/mise-nomina-esperti-blockchain-e-a.i._0fe985f9-26cf-44d8-9090-d77157967e81.html)
- [Truffa da 29M manipolando il BGP](https://arstechnica.com/information-technology/2018/12/how-3ves-bgp-hijackers-eluded-the-internet-and-made-29m/)

#### La Fabbrica del Falso

- [Quanto dell'INTERNET è falso?](http://nymag.com/intelligencer/2018/12/how-much-of-the-internet-is-fake.html)

