Title: stakkastakka #2 - Password
Slug: stakkastakka-2-password
Date: 2018-11-11 01:33:52
Tags: stakkastakka, password

#### POSTCAST
<audio controls>
  <source src='https://stakkastakka.fugadalcontrollo.org/stakka-stakka-2-29-10-18.ogg' type='audio/mpeg'>
</audio>
<a href='https://stakkastakka.fugadalcontrollo.org/stakka-stakka-2-29-10-18.ogg'/>Puntata completa</a>

<br/>
#### NEWS, abbiamo parlato di:
- [google abbandona berlino dopo due anni di
  #fuckoffgoogle](https://www.theguardian.com/technology/2018/oct/24/google-abandons-berlin-base-after-two-years-of-resistance)
- [hacking team e l'omicidio khashoggi](https://www.bbc.com/news/world-middle-east-45918610)
- [dopo microsoft che compra github, ibm compra redhat](https://www.redhat.com/en/about/press-releases/ibm-acquire-red-hat-completely-changing-cloud-landscape-and-becoming-world%E2%80%99s-1-hybrid-cloud-provider?fbclid=IwAR3B4TDmlpBIef4CNRNj5YXTI7j9Zh4-RvOYGDBTLQuVwnBVH_pjsMTsRFM)
- [il telefono di trump](https://www.nytimes.com/2018/10/24/us/politics/trump-phone-security.html)
  
#### APPROFONDIMENTO
Abbiamo parlato tanto di password, di quanto non siamo bravi a sceglierne
di buone, di leak, di [monitor.firefox.com](https://monitor.firefox.com) e del perche' bisogna usare dei password manager
intervistando un mantainer di [keepassxc](https://keepassxc.org/).






