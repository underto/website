Title: stakkastakka #29 transumanesimo ed eigenlab
Slug: stakkastakka-29-transumanesimo-ed-eigenlab
Date: 2019-05-06 19:10:42
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/05/stakkastakka-29-transumanesimo.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/05/stakkastakka-29-transumanesimo.ogg'/>Puntata completa</a>


#### Posta del cuore dell'INTERNET

- La
  [lettera](https://www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/9107773)
  del garante della privacy al parlamento ed al governo sull'uso dei
  captatori informatici

#### Transumanesimo

Abbiamo parlato con l'autore di "Critica al transumanesimo – Nautilus" che
sarà presentato a
[librincontro](https://librincontro.noblogs.org/programma/).

#### Eigenlab

Diretta con gli amici di [eigenlab](https://eigenlab.org/) di Pisa per
parlare della loro esperienza e del
[crowdfounding](https://eigenlab.org/pannellisolari/) per un bellissimo
progetto per la loro autosufficienza energetica.
