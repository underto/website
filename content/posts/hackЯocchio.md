Title: HackЯocchio - 9-10 Aprile @Edera Squat
Slug: hackЯocchio
Date: 2022-04-06 17:54:55

> <b><a href='hackrocchio.org'>HackЯocchio</a></b> <small><i>s. m. [der. di hackЯocchiare] (pl. -chi)</i></small>  
> <i>situazione o soluzione raffazzonata, raccogliticcia, approssimativa</i>

<center>![/image/thumb/hackrocchio.jpg]({static}/images/thumb/hackrocchio.jpg)</center>

<strong>
Sabato 9 Aprile dalle ore 14 e Domenica 10 Aprile 2022  
[Edera Squat](https://ederasquat.noblogs.org/), Via Pianezza 115, Lucento, Torino.  </strong>

Costruire comunità resistenti complici nelle lotte, saldare relazioni di fiducia tra le moltitudini sommerse nell'estrattivismo cognitivo, riuscire ad immaginare e a costruire tecnologie conviviali che sostituiscano le megamacchine digitali. Non promettiamo la rivoluzione ma il crepuscolo degli dei, per farlo inietteremo lo shellcode nel paese reale, andando nelle case ma sopratutto nei cuori di chi ancora ci crede, a decifrare le blockchain corrotte di un sistema che nulla ha più di umano se non la scalabilità del vostro frigorifero nel cloud, insomma, in una parola, quantum machine learning :)

se non ci hai capito molto, neanche noi, ma trovi tutte le informazioni e il [progamma](https://hackrocchio.org/programma) su [https://hackrocchio.org](https://hackrocchio.org)

