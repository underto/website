Title: stakkastakka #32
Slug: stakkastakka-32
Date: 2019-06-03 15:51:24
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/06/stakkastakka-32.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/06/stakkastakka-32.ogg'/>Puntata completa</a>

## Mostra Bin/Art!

- [Tape Mark](https://binart.eu/tape-mark-1/)
- [GE-115](https://binart.eu/ge-115-concerto/)
- [LSD Dream Emulator](https://binart.eu/lsd-dream-emulator/)

## Notizie

- [Googledepreca in Chrome le API usate dagli
  nadblocker](https://www.techradar.com/news/chrome-will-limit-full-ad-blocking-to-enterprise-users)
- [Gli Stati Uniti attaccano giornalisti e
  whistleblowers](https://www.techdirt.com/articles/20190526/08272942283/internal-report-says-doj-did-nothing-wrong-targeting-journalists-communications-to-hunt-down-leakers.shtml)
- [La polizia di San Francisco ha perquisito un giornalista che ha ricevuto
  un
  leak](https://edition.cnn.com/2019/05/25/us/san-francisco-police-chief-journalist-raid/index.html)
- [Le difficoltà degli agenti segreti di
  oggi](https://foreignpolicy.com/2019/04/27/the-spycraft-revolution-espionage-technology/)
- [Tutto questo condito con un sacco di mannaggia](https://www.ilpost.it/2019/06/01/spie-cambiamenti/)
