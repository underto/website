Title: stakkastakka #54 - Mailing list
Slug: stakkastakka-54-mailing-list
Date: 2020-01-29 15:28:17
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka54.mailinglist.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka54.mailinglist.ogg'/>Puntata completa</a>
<br/>

Nella prima parte della puntata abbiamo parlato di PoC||GTFO:

[https://www.alchemistowl.org/pocorgtfo/](https://www.alchemistowl.org/pocorgtfo/)

qui invece il pdf dell'NSA

[https://media.defense.gov/2020/Jan/14/2002234275/-1/-1/0/CSA-WINDOWS-10-CRYPT-LIB-20190114.PDF](https://media.defense.gov/2020/Jan/14/2002234275/-1/-1/0/CSA-WINDOWS-10-CRYPT-LIB-20190114.PDF)

Dal minuto `48 circa parliamo invece di Mailing list
