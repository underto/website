Title: stakkastakka #49 - No Tengo Dinero
Slug: stakkastakka-49-no-tengo-dinero
Date: 2019-12-12 17:26:35
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/12/stakkastakka-49-no-tengo-dinero.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/12/stakkastakka-49-no-tengo-dinero.ogg'/>Puntata completa</a>
<br/>

## Posta del ♥ dell'Internet

- L'aniterrorismo aiuterà la Federazione Italiana Calcio a combattere il
  razzismo con un radar sonoro. [GRANDE GIORNALISMO](https://www.calcionapoli1926.it/senza-categoria/figc-gravina-razzismo-radar-sonoro/)

- Lo [stesso personaggio](https://twitter.com/isislovecruft/status/1200547871252074496) che [provò a vendere i dati di tor2web](https://lists.torproject.org/pipermail/tor-project/2016-May/000355.html) all'INTERPOL è stato arrestato per aver provato a [spiegare la blockchain](https://www.nbcnews.com/news/us-news/cryptocurrency-expert-accused-assisting-north-korea-evading-sanctions-n1093461) ai Nord Coreani.

## Approfondimento Sistemi Bancari

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/12/stakkastakka-49-bits-no-tengo-dinero.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/12/stakkastakka-49-bits-no-tengo-dinero.ogg'/>Approfondimento</a>
<br/>

- [La rapina alla banca del Bangladesh](https://en.wikipedia.org/wiki/Bangladesh_Bank_robbery)
- [Payment Systems: From the Salt Mines to the Board Room](http://gen.lib.rus.ec/book/index.php?md5=2A605FFFECE9B0203DC0143A030BD5EB)
- [ISO20022](https://www.iso20022.org/)
- [Standard bancomat](https://www.cen.eu/work/Sectors/Digital_society/Pages/WSXFS.aspx)
- [Standard smartcard](https://www.emvco.com/wp-content/uploads/2017/05/A_Guide_to_EMV_Chip_Technology_v2.0_20141120122132753.pdf)
- [Esempio reverse engineering mifare](https://en.wikipedia.org/wiki/Crypto-1)

