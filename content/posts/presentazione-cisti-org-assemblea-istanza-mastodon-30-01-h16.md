Title: Presentazione cisti.org & assemblea istanza mastodon 30/01 h16
Slug: presentazione-cisti-org-assemblea-istanza-mastodon-30-01-h16
Date: 2022-01-29 22:30:41
Tags: presentazioni, eventi


<a href='https://gancio.cisti.org/event/presentazione-cistiorg-and-assemblea-istanza-mastodon'><img style='max-height: 90vh;' src="https://gancio.cisti.org/media/0e7926dd8d01758b842b3ce880b4ade8.jpg"/></a>


### Domenica 30 gennaio 2022 <a href='https://gabrio.noblogs.org/'>@CSOA Gabrio, via Millio 42, Torino</a>

# Presentazione cisti.org & assemblea istanza mastodon.cisti.org

<br/>
### h16
Presentazione e chiacchiere su [cisti.org](https://cisti.org), uno spazio digitale liberato, un server scapestrato e autogestito.

<br/>
### h18
assemblea dell'istanza [mastodon.cisti.org](https://mastodon.cisti.org)

a seguire cena condivisa e proiezione cortometraggi tematici a cura dell'hacklab underscore_to
