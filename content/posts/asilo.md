Title: In Solidarietà all'Asilo Occupato
Slug: asilo
Date: 2019-02-15 01:14:11

Non succedeva da anni che l'hacklab rimanesse chiuso il mercoledi' sera,
ma il vento di internet ci ha portato una notizia talmente brutta che
oggi non stiamo al pc, usciamo per strada: [hanno sgomberato l'asilo e
hanno arrestato 6 persone](https://www.autistici.org/macerie/?p=33314).

Da allora e' passata una densa settimana, tra strade piene di divise di
ogni colore, ma anche piene di nuove compagnie.  
Le emozioni sono tante e l'unico istinto è quello di unirci a questo
grande abbraccio di solidarietà che si stringe attorno all'Asilo, un 
luogo che, contrariamente a quanto dipinto dai pennivendoli de [La
Busiarda](https://pms.wikipedia.org/wiki/La_Stampa) -e non solo- negli ultimi venticinque anni, è stata una delle
poche realtà torinesi ad opporsi con coraggio ad un mondo di muri,
macerie, sfruttamento e solitudine.  
Ci sentiamo quindi di espiremere solidarieta', vicinanza e complicita'
con chi lotta contro lo sgombero dell'Asilo!

Ma insieme alla solidarietà vogliamo mettere le nostre conoscenze a
disposizione di chi, in questi giorni, sta mettendo in gioco la propria
libertà e creatività per una lotta che ha ormai superato la questione
specifica dello sgombero dell'Asilo.

Ci sembra utile quindi dare alcune dritte su come usare al meglio gli
strumenti tecnologici a nostra disposizione, cercando di limitare i
danni a noi e a chi ci sta intorno.

Queste scelte richiedono impegno, tempo e confronto per argomentare come
mai consigliamo questo o quello strumento, per questo stiamo preparando
delle giornate a tema dove affronteremo insieme questi temi, nel
frattempo considerateli come consigli ragionati.

### Scrivere un documento/manifesto/volantino
- Per scrivere un testo condiviso consigliamo [https://pad.riseup.net](https://pad.riseup.net)  
- Per scambiare documenti/volantini invece c'è [https://share.riseup.net](https://share.riseup.net)

### Partecipare a un corteo/assemblea/evento
- Prima di andare, elimina dal telefono dati compromettenti o sensibili.
- Metti un pin, una password o una sequenza al telefono. Quello che vuoi
ma fallo Davvero.
- Attiva la cifratura del telefono da Impostazioni > Sicurezza e Privacy.
- Usa Signal per comunicare, ha anche le chiamate, i messaggi vocali e
puoi impostare l'autodistruzione dei messaggi con chi vuoi tu.
- Valuta se ha senso fare delle foto, considera sempre che se le pubblichi
o le condividi queste si porteranno dietro i tuoi dati.
Usa [ObscuraCam](https://guardianproject.info/apps/obscuracam/) per oscurare i volti e [Scrambled Exif](https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif) per rimuovere i metadati.

### Organizzarsi
Lascia il telefono lontano da dove chiacchieri, possibilmente lascialo 
ACCESO a casa.

### Comunicare col mondo
- Usa i blog e il passaparola, funzionano.
- Per tenerti aggiornato, oltre ad ascoltare la radio puoi usare un
lettore RSS, che notifica quando ci sono aggiornamenti sui siti che
segui (ad esempio macerie o radio blackout).
- Consigliamo di usare Feeder sul telefono e l'estensione [Feedbro](https://nodetics.com/feedbro/) sul
computer.

### Social
L'ondata di repressione che ci ha travolti per le strade della città si
ripercuote anche sui social media: oggi [è stato oscurato il profilo
facebook di Macerie](https://it-it.facebook.com/macerie.torino/posts/1937691816352814?__xts__[0]=68.ARC5AWrmAM_GKlbCdHfWvHkqFC5PTx5gilnvBlyMBJM8wfYjSHT3XA6fadlZobYsoCrlyfGV1XbvCaXSzQWOEGaJ6oXcwhJ2lFg9efKDRAXwjxAfzcu_U6h1n7cgyZvt5zbFp2Orj-_mCBBVsEsgydv3JSXP4UmIZmyfJ171qcKC1K-AnxWQPlQo9Q74R9d29wESxxwFexpXWax89NXsJEHO8cxfQRuk5RatziPhwnYBaoHMg9EsVo03nDH5EjMwx6NCt3oGvS5cpxDtZOTxleyAIZ2puOKL_w1kJAqckxaRxvjvOyjV9E-iAegRqxsH8QnKUQwkxb4eLa0GzquQFA&__tn__=-R), che si è preoccupata, insieme ad altri collettivi,
di aggregare informazioni, testimonianze e immagini dello sgombero e
delle manifestazioni di solidarietà che ne sono seguite.

Che per Facebook, come per altre piattaforme corporative, informare e
organizzare la protesta sia una colpa è cosa nota, visto anche con che
facilità collaborano con le forze della repressione.
Sarebbe come aspettarsi di poter avere liberta' di parola su La Busiarda

Come hacklab underscore vogliamo dare un contributo in questa direzione,
sentiamo l’esigenza di creare spazi sociali “liberati”, anche nel
digitale, dove le persone possano sentirsi accolte e libere dalla
pressione della società della performance, creando strumenti per far
interagire comunità di sangue e carne, in grado di produrre 
alternative (anche) alle piattaforme del capitalismo.

Quindi noi ci stiamo organizzando in questa direzione e col medesimo
intento di aiutare le comunità ad organizzarsi nel territorio, stiamo
anche progettando un calendario di movimento condiviso, un qualcosa tra
l'agendona di radio blackout e la bellissima carta canta, ne scriveremo
quanto prima.

Nel mentre vi consigliamo delle letture di autodifesa digitale che
possano aiutare ad approfondire questi argomenti.

Per sapere come mettere in sicurezza i nostri dispositivi e le nostre
comunicazioni vi consigliamo le seguenti letture:  

- [ITA] [https://numerique.noblogs.org/](https://numerique.noblogs.org/)
- [ENG] [https://ssd.eff.org/en](https://ssd.eff.org/en)

Per una lettura piu' vicina alla questione perquisizioni e sequestri
ci sentiamo di consigliare il libricino "Stop Al Panico" (a cui abbiamo
collaborato come comunità hackmeeting alla parte digitale) e Vlad 
(vedemecum per gli abusi in divisa) [http://www.abusivlad.it](http://www.abusivlad.it)

Se avete domande, potete inviarcele via mail all'indirizzo:
underscore@autistici.org inoltre tutti i lunedì dalle 13 alle 15, potete
interagire con noi durante la trasmissione stakkastakka sulle libere
frequenze di [radio blackout](https://radioblackout.org).

Proveremo a rispondere ad ogni dubbio tecnologico (e non) al meglio
delle nostre capacità.

stay safe, fate cisti  
tutti liberi, tutte libere!


