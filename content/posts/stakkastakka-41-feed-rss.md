Title: stakkastakka #41 - Feed RSS
Slug: stakkastakka-41-feed-rss
Date: 2019-09-30 17:34:32
Tags: stakkastakka


#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/09/stakkastakka-41-feed-rss.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/09/stakkastakka-41-feed-rss.ogg'/>Puntata completa</a>
<br/>

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/09/stakkastakka-feed-rss.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/09/stakkastakka-feed-rss.ogg'/>Approfondimento sui Feed RSS</a>
<br/>

## Il consiglio del vostro aquer di fiducia

- [TextMarker Go](https://addons.mozilla.org/de/firefox/addon/textmarker-go/) è una
  piccola estensione che ci piace usare per evidenziare i testi nel web
  (niente di più, niente di meno!).

## Posta del ♥ dell'Internet

- [Attacchiamo gli aerei con le onde radio](https://blog.acolyer.org/2019/09/27/wireless-attacks-on-aircraft-instrument-landing-systems/)

- [Tre fermati per aver condiviso quotidiani su telegram e whatsapp](https://www.repubblica.it/cronaca/2019/09/21/news/giornali-236557592/)

- [HELPDESK A/I è tornato per notificare problemi, chiedere aiuto ed effettuare richieste](https://helpdesk.autistici.org/)

## Feed RSS


### programmi / software:

Esistono dei programmi che leggono i feed.

Se hai Android, ti consigliamo [Flym](https://www.f-droid.org/en/packages/net.frju.flym/) o [Feeder](https://f-droid.org/en/packages/com.nononsenseapps.feeder/).

Per iPhone/iPad puoi usare Feed4U

Per il computer fisso/portatile consigliamo
[Feedbro](https://nodetics.com/feedbro/), da installare all'interno di
[Firefox](https://addons.mozilla.org/en-GB/firefox/addon/feedbroreader/) o
di
[Chrome](https://chrome.google.com/webstore/detail/feedbro/mefgmmbdailogpfhfblcnnjfmnpnmdfa)
e compatibile con tutti i principali sistemi operativi, andando a visitare
un sito che non contiene l'iconcina arancione con i tre archetti c'e' un
comodo tasto "trova feed in questo sito".

- anche per i podcast ci sono delle applicazioni apposite sia su computer che su telefono, le trovate facilmente, alcuni programmi fanno entrambe le cose, cioe' sia da lettori di feed sia da lettori di podcast.
- [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox/) (podcast, ma scarica le puntate quindi occhio!)
- [Liferea](https://lzone.de/liferea/) (rss/podcast)


### LINK DEI PODCAST DA AGGIUNGERE AI VOSTRI PROGRAMMI:

- https://radioblackout.org/feed/ (info e podcast dell'info)
- https://radioblackout.org/podcast/feed/ (tutti i podcast della radio di ogni tramissione)

se non volete tutti i podcast di tutte le trasmissioni potete filtrare solo quelle delle trasmissioni che vi interessano, ad esempio:

- https://radioblackout.org/podcastfilter/stakka-stakka/feed/ (stakkastakka)
- https://radioblackout.org/podcastfilter/bello-come-una-prigione-che-brucia/feed/ (bello come una prigione che brucia)
- https://radioblackout.org/podcastfilter/anarres/feed/ (anarres)
- https://radioblackout.org/podcastfilter/la-perla-di-labuan/feed/ (la perla di labuan)
- https://radioblackout.org/podcastfilter/resetclub/feed/ (resetclub)
- https://radioblackout.org/podcastfilter/arsider/feed/ (arsider)
- https://radioblackout.org/podcastfilter/liberation-front/feed/ (liberation-front)
- https://radioblackout.org/podcastfilter/mala-femme/feed/ (mala-femme)
- https://radioblackout.org/podcastfilter/radio-kebab/feed/ (radio-kebab)
- https://radioblackout.org/podcastfilter/malormone/feed/ (malormone)
- https://radioblackout.org/podcastfilter/sayonara/feed/ (sayonara bankok)

insomma avete capito :)


#### FEED DEI SITI VOSTRI


- https://www.autistici.org/macerie/?feed=rss2
- https://anarresinfo.noblogs.org/feed/
- https://www.manituana.org/feed/
- https://www.mezcalsquat.net/feed/
- http://www.notav.info/feed/
- https://ahsqueerto.noblogs.org/feed/
- https://gabrio.noblogs.org/feed/
- https://roundrobin.info/feed/
- https://ederasquat.noblogs.org/feed/
- https://www.nautilus-autoproduzioni.org/feed/
- https://cavallette.noblogs.org/feed/atom
- https://autistici.org/underscore/atom.xml
- https://gancio.cisti.org/feed/rss
- https://cdlfelix.noblogs.org/feed/

se ci volete mandare i vostri feed preferiti non esitate :)

hackmeeting (feed di varie realta' del giro hackmeeting):  

- https://www.autistici.org/underscore/hackit/

