Title: stakkastakka #42 - Internet mon amour e limiti di fb
Slug: stakkastakka-42-ima-fb
Date: 2019-10-09 17:34:32
Tags: stakkastakka


#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/10/42.circe_.ima_.megamacchine-e-facebook-.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/10/42.circe_.ima_.megamacchine-e-facebook-.ogg'/>Puntata completa</a>
<br/>

Per questa puntata niente posta del cuore dell'internet, abbiamo parlato un po' di Torino citta' del futuro e poi dal minuto 11 circa, una bella intervista con Agnese e Karlessi di <a href="https://circex.org/">Circe</a> che ci parlano di  <a href="https://circex.org/it/ima/">Internet mon amour</a>, un libro di "cronache prima del crollo di ieri".

Dal minuto 58 circa invece rimandiamo una preparatissima puntata da <a href="https://www.ondarossa.info/trx/dita-nella-presa">le dita nella presa</a>, (tramissione sorella maggiore di stakkastakka in onda su <a href="https://www.ondarossa.info">radio onda rossa</a> a roma) riguardo "all'analisi della comunicazione su Facebook. Non per parlare di temi (pur interessanti) come privacy, controllo, profilazione, ma per valutare le possibilità che Facebook (<em>non</em>) offre a chi volesse utilizzarlo per fare una comunicazione politica."
