Title: stakkastakka #30 listino prezzi della sorveglianza
Slug: stakkastakka-30-listino-prezzi-della-sorveglianza
Date: 2019-05-20 17:03:59
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/05/stakkastakka-30.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/05/stakkastakka-30.ogg'/>Puntata completa</a>

## Posta del cuore dell'internet

- [USA si ricomprano Grindr](https://www.repubblica.it/tecnologia/social-network/2019/05/17/news/l_app_grindr_torna_in_mano_usa_troppo_pericoloso_lasciarla_ai_cinesi-226513020/)
- [John alla macchia](https://twitter.com/officialmcafee/status/1129068785762283521)

# Sorveglianza

[Baco di
uatsap](https://tinyletter.com/carolafrediani/letters/guerre-di-rete-newsletter-la-falla-whatsapp-gli-spyware-khashoggi-disinformazione-cyberspie-iraniane-e-altro)
e diretta con l'autore di ["Cosa c'è nel listino prezzi della Procura di
Milano per le
intercettazioni"](https://www.vice.com/it/article/43ja4q/listino-prezzi-intercettazioni-dispositivi-procura-milano-mercato-malware)
