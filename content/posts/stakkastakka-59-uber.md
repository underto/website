Title: stakkastakka #59 - Uber
Slug: stakkastakka-59-uber
Date: 2020-03-15 17:26:24
Tags: stakkastakka

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/03/stakkastakka-59.mp3' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/03/stakkastakka-59.mp3'/>Puntata completa</a>


## Posta del ♥ dell'Internet

 - [Come un ciclista è stato ingiustamente incriminato di un furto
   attraverso la geolocalizzazione di
   Google](https://www.nbcnews.com/news/us-news/google-tracked-his-bike-ride-past-burglarized-home-made-him-n1151761)
 - [La polizia europea vuole un database condiviso per il riconoscimento
   facciale](https://www.wired.it/attualita/tech/2020/03/02/europa-polizia-database-riconoscimento-facciale/)
 - [Seti@Home sarà dismesso a fine Marzo, e si porta via un'intera
   generazione di cacciatori di UFO (cosi ci
   nascondono?)](https://www.bleepingcomputer.com/news/software/seti-home-search-for-alien-life-project-shuts-down-after-21-years/)
 - [Addio alle lampadine smart: Philips taglia la connessione alle vecchie
   Hue Bridge (Addio e grazie per tutte le
   botnet!)](https://www.vice.com/en_us/article/jgead4/philips-internet-connected-lightbulbs-will-no-longer-connect-to-the-internet)
 - [Fawkies, l'app che rende irriconoscibili i tuoi immancabili selfie alle
   AI](https://www.vice.com/en_us/article/m7qxwq/researchers-want-to-protect-your-selfies-from-facial-recognition)

## Approfondimento su uber

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/03/stakkastakka-59-bits-uber.mp3' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/03/stakkastakka-59-bits-uber.mp3'/>Approfondimento su Uber</a>



Parliamo di Uber. Un applicazine che nasce nel 2010 che permette di
richiedere ad un altro utente di venirti a prendere dietro pagamento.
Parleremo di come Uber sia riuscita a creare un buco finanziario di
miliardi di dollari sostenuta da una prodigiosa propaganda basata su
finzione, confusione e produzione di articoli accademici che nessuno si è
premurato di rileggere.


Perchè uber, una compagnia che ha perso oltre venti miliardi di dollari da
quando ha incominciato ad operare nel 2010 e che non mostra alcun segno di
poter generare profitto viene ancora vista come una compagnia di successo
ed in crescita?  Perchè c'è la percezione che uber abbia portato notevoli
miglioramenti al sistema di trasporto urbano ed un generale miglioramento
della qualità del servizio per i passeggeri quando ha fallito ad avviare un
modello di azienda funzionante e cercando di prevaricare in pezzi del
mercato attraverso una politica semi-monopolistica?


Uber paradossalmente non è in grando di mantenere dei veicoli in modo più
efficiente o meno costose dei servizi di taxi che ha portato al fallimento.
Tutto il siccesso e la popolarità di uber può essere spiegata da miliardi
in investimenti predatori in strumenti di ottimizzazione del servizio.


Non c'è alcuna prova inidimendente che l'innovazione tecnolgia sventolata
da uber (ammesso e non concesso che sia vera) abbiamo mai avuto un vero
impatto materiale nell'organizzazione del mercato. Uber un azienda che ha
prodotto un livello di innovazione comparabile con quello del mio gatto.


Il vantaggio economico di uber nel mercato non deriva da qualche
sofisticato algoritmo di ottimizzazione, ma dalla constante capacità
dell'azienda di tagliare la "quota" riservata agli autisti. Dal 2016
parliamo di una contrazinoe del 40%. I tagli hanno portato gli stipendi
sotto il livello minimo in molti territori dove l'applicazione ha operato.
Questo ha permesso di trasferire circa tre miliardi dai lavorari al
capitale.


Ma come ha fatto uber a convincere (ehm fragare) i finanziatori?
Scimiottando quello che le GAFAM fanno da sempre. È andata in giro a
vantarsi del suo monopolio.


Amazon, Google e Facebook hanno lasciato agli startuppari di domani un
copione da seguire per chi vuole raggiungere la valutazione a nove cifre
del capitale. Queste aziende prima hanno basato le loro fondamenta su
prodotti che potremmo definire legittimi e portatori di una certa quantità
di innovazione che potremmo definire "genuina". Google effettivamente ha
fatto un motore di ricerca, Facebook un social network, Amazon i suoi
server. A quello punto hanno dimostrato che questi prodotti erano in grado
di reggere sul mercato, insomma fare soldi. Da questa posizione poi si sono
divertiti a costruire monopoli aquisendo tutti i possibili competitor.


Uber non è semplicemnete un altra bolla tech che beneficia di capitale a
buon mercato e la percezione diffusa che la "tecnologia disruptive" è in
grado di risolvere i problemi del mondo. La sua strategia si è basata su
tre tasselli che nessuno aveva mai messo in gioco in questo modo.


Il primo vantaggio strategico è stato quello di saltare a piè pari la
difficolta di dover costruire un prodotto che funzionasse. Colonna portante
del paradigma dell'unicorno. Sono partiti direttamente dai 13 migliardi
messi sul tavolo dai mitici investitori. 2300 volte la prima IPO richiesta
da Amazon.


Il secondo tassello è stato quello della "crescita a tutti i costi".
Giornalisti, avvocati, politici si sono fatti dettare l'agenda dai PR di
uber.


Il terzo è stato quello di guidare lo sviluppo dell'azienda come un
processo politico, usando tecniche importate dal mondo dei partiti. Gli
investitori di uber sapevano che gli sarebbe servito il potere politico per
accellerare la crescita e mantenere le loro promesse.


Amazon, Google e Facebook prima di avere il bisogno e la capacità di fare
lobby hanno atteso anni. Uber è stata la prima startup in cui questa
attività è stata in testa alla lista delle priorità dal giorno uno. Tanto
da assumere un ex considegliere del presidente (David Plouffe) o un
confidente del Primo Ministro Britannico (Rachel Whetstone) come dirigenti
delle relazioni pubbliche dell'azienda.


Questa strategia in tre parti ha sostenuto Uber in dieci anni di perdite da
capogiro che avrebbero facilmente distrutto qualisiasi altra startup. La
curiosità finale è che uber non è mai arrivata ad avere quella posizione di
monopolio nel mercato da poter attuare veramente le politiche delle sue
sorelle maggiori.


La narrative di uber è stata clamorosamente sbagliata dall'inizio, ma per
fortuna c'è qualcuno che è sempre stato al suo fianco. Senza il ruolo dei
media che hanno sistematicamente ignorato ogni prova di buon senso che gli
cascava sui piedi per il nostro unicorno sarebbe stato difficile spiccare
il volo.


Infatti sta nella costruzione di un impareggiabile e caleidoscopica
narrativa il metro con cui misuriamo il valore delle startup. Vediamo
qualche esempio sperando che possa stimolare l'appetito prima di pranzo.


Uber è cresciuta percchè gli utenti hanno liberamente scelto i suoi servizi
in mercati aperti e competitivi. Ignorate i grossi sussi che hanno
completemente distorto i prezzi.  La potenza tecnologica di Uber può
gestire ogni incombenza in qualsiasi mercato ovunque. Peccato che in cina
abbia fatto un buco nell'acqua al primo tentativo.  I problemi dei servizi
di taxi esistenti sono stati creati dai politici cattivi, gli stessi che
hanno provato ad impedire alla uber buona di creare posto di lavoro. Ma
porcoddio.  La legge non può essere applicata ad Uber perchè Uber è nuovo.
nuova ride-super-sharing-industry. Ignoriamo il fatto che i taxi e Uber
condividano la stessa identica struttura, scopi, etc.  Uber è tech,
super-tech. Ogni perdita diventerà un vantaggio finchè non riuscirà a
distruggere i proprietari di auto ed il trasporto pubblico. Ignoriamo i
costi, le perdite e i fallimenti. Per il resto tutto regolare.  Le leggi
sul lavoro no possono proteggere i lavoratori di Uber perchè perchè è una
compagnia di sviluppo software, non una di trasporti.


Il genio del suo programma di comunicazione è stato prorio quello di
distrarre dalla sua incapacità di reggersi sulle proprie gambe e convincere
le persone che tutti i problemi del mercato dei taxi è stato generato dalle
leggi. La combinazione della tecnologia super pettinata di uber e la sua
superiorità alle leggi invece erano gli ingredienti per superare questi
limiti.


Ma la storia di Uber rimane un piano aziendale da anni 90 portato avanti da
un gruppo di libertari corporativi guidati da Charles and David Kock. Il
loro programma continua a spingere ad eliminare ogni tipo di standard
pubblico di sicurezza per i trasporti.


L'abilità di uber negli ultimi dieci anni di manenere la sua immagine di
successo di azienda innovativa si è basata su tecniche di propaganda.


Il problema è stato che Uber ha supportota ricerche accademiche che
seguivano standard per essere indipendenti o finanziate dalle università,
ma che in realtà spingevano la causa della startup californiana. Uber ha
beneficiato enormemente non solo dagli articoli di giornale, ma anche dalle
versioni semplificate di articoli accademici, blog e think tanks.


Editori di prestigiosi giornali economici non sono stati in grado di
valutare in modo critico le proposte di articoli sul modello azienda di
Uber. Ma ogni tipo di review o risposte agli articoli originali sarebbe
stata impossibile dato che i dati necessari per svilupparli erano e sono
ancora di proprietà esclusiva di Uber.


Un esempio potrebbe essere l'articolo di Cramer e Krueger del 2015 dal
titoli "Cambiamenti distruttivi nel business dei taxi: Il caso di Uber".
Pubblicato inizialmente dal National Bureau o Economic Research e poi
dall'American Economic Review.


Il paper ha dato credibilità al millantato vantaggio economico di Uber (38%
nel mondo, 66% in alcune città). Il paper si è basato principalmente sullo
scontro Uber vs Taxi utilizzando misure incompatibili tra loro come il
totale di ore lavoro. Infatti i guidatori d taxi hanno grandi incentivi a
guidare tutto il giorno per dover coprire i costi del mezzo, la benzina,
assicurazione, etc. I guidatori di Uber hanno grossi incentivi a guidare
poco nei picchi di richiesta e non accendere l'applicazione a meno di
essere immediatamente pronti a guidare. Il paper continua senza presentare
mai un dato.


Ma il paper che mi è piaciuto di più si chiama "I guidatori di Uber
guadagnano più dei guidatori di taxi essendo più flessibili". Capolavoro.
Per incominciare notiamo come uno dei due autori al tempo della scrittura
dell'articolo era formalmente un dipendente di Uber, l'altro un consulente.
All'interno troviamo dati sul guadagno annuo dei guidatori che si sono
dimostrati interamente costruiti a tavolino dagli autori.


Ora passiamo a "Uber aumenta il benessere dei clienti" dove arrivano i big
data. Sorvoliamo su come uno studio condotto sulle quattro città più ricche
degli stati uniti possa essere rappresentativo per il mondo. Questo paper
non ha misurato il benessere degli utenti, ma una piccola contrazione del
prezzo in una forbice di pochi mesi.


"Il valore del lavoro flessibile: prove dai lavoratori di Uber" è stata la
risposta di Uber alle crescenti accuse di sfruttamento dei lavoratori e
delle difficili condizioni di stress a cui gli autisti sono stati
sottoposti. Un potente grimaldello di propaganda che ha permesso ad Uber di
tacciare i suoi detrattori di fare discorsi contro gli interessi dei
lavoratori.


Il paper parla di come il modello operativo di Uber non impone alcun tipo
di limitazione ai lavoratori. Sarebbe vero se non si ignorasse
completamente le politiche che forzano gli autisti a comprare veicoli
sempre più costosi per ottenere più corse dall'algoritmo, algoritmo che
provvede ad eliminare gli autisti dalla piattaforma qualora si osassero di
rifiutare una corsa.


Questi paper sono solo un esempio di come i giornalisti hanno felicemente
digerito tutto la propaganda della startup californiana facendosi megavono
per i loro investitori.
