Title: stakkastakka #48 - CyberSyn
Slug: stakkastakka-48-cybersyn
Date: 2019-12-05 12:21:18
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/12/stakkastakka-48.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/12/stakkastakka-48.ogg'/>Puntata completa</a>
<br/>

## Posta del ♥ dell'Internet

Cogliendo il suggerimento della redazione informativa abbiamo fatto qualche
accenno alla web-tax e [critica alle varie proposte di regolamentazione
delle
big-tech](https://www.theguardian.com/commentisfree/2019/nov/28/big-tech-populist-stance-big-money-big-state?CMP=fb_a-technology_b-gdntech).

## CyberSyn

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/12/stakkastakka-48-bits-cybersyn.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/12/stakkastakka-48-bits-cybersyn.ogg'/>Approfondimento CyberSyn</a>
<br/>


### Preambolo

L'undici Settembre del 1973 a Santiago il palazzo de La Moneda viene
bombardato dall'aviazione cilena, nel mentre il presidente Salvador Allende
rimane asserragliato nel primo piano del palazzo, da cui manda via radio il
suo ultimo discorso alla Nazione, interrotto solo dai bombardieri e
successivamente, dai militari guidati da Pinochet che segneranno la fine
del cosiddetto "esperimento Cileno", del primo governo socialista e di
chiara ispirazione Marxista formatosi vincendo le elezioni, senza passare
per la lotta armata.

Nel primo piano del palazzo, vicino l'ufficio presidenziale i militari
entrano in una stanza dalla forma esagonale, a ridosso dell'entrata si
trova un cucinino, nelle altre cinque pareti sono presenti schermi di varia
natura, al centro sono disposte circolarmente sette sedie in vetroresina,
ognuna con una pulsantiera al fianco, che rimandano alla scenografia di
Star Trek.  Nella parete centrale è presente un vero e proprio monitor di
un mainframe, un IBM 360/50, che fa susseguire una sfilza indecifrabile di
numeri, mentre ai lati sono presenti vari schermi che presentano funzioni,
grafici a flusso in evoluzioni.  I militari pensarono di essere
improvvisamente proiettati sul set di un film di fantascienza, e senza
sapere come comportarsi, iniziano a punzecchiare i monitor con le baionette
dei fucili, senza avere chiare di cosa si trattasse.

Questo segna la fine di Cybersyn, rinominato dal Guardian l'Internet
socialista di Allende, in particolare la visione di una rete che avrebbe
permesso di coordinare gli sforzi delle varie Cooperative, di orchestrare
la produzione delle singole fabbriche senza avere un apparato burocratico
verticale in stile sovietico e di soddisfarne la produzione senza un
mercato interno in senso capitalista.  E di Cybersyn andremo a parlare...

### Introduzione

Il progetto, chiamato Cybersyn in inglese e Proyecto Synco in Spagnolo, era
un ambizioso, molto ambizioso, tentativo di mettere in connessione
l'economia cilena.  Descritto dal Guardian come l'Internet socialista di
Allende, non senza una ragione, era sicuramente avanti anni luce nel
proprio periodo sia per la difficoltà tecnica, sia per l'acume critico
posto nella progettazione, infatti come vedremo ogni dettaglio, dal design
della stanza decisionale di cui parlavamo prima ecc. Aveva un preciso
significato politico e filosofico.  Anche in casi più a noi contigui, come
le varie sventure di Internyet, nello sviluppo di una qualsiasi tecnologia
raramente ne viene considerato il contenuto tecnopolitico e le implicazioni
sociali, se non in senso utilitaristico dalla classe dominante.

Rimasto fuori dalle grandi narrative per decenni, al 40esimo anniversario
del colpo organizzato da Pinochet ci fu un ritorno di interesse verso il
progetto da parte dei media mainstream con il New Yorker, grazie anche alla
pubblicazione del libro "Cybernetic Revolutionaries" della storica Eden
Medina.  Il progetto è interessante sia perché ricalca il progetto
contemporaneo degli Stati Uniti: ARPANET, sia perché è affascinante vedere
come sarebbe potuta nascere un infrastuttura informatica nel "Global
South".  L'argomento ha avuto un revival successivo quando è stato ripreso
da Srnicek e Williams in "Pretendi il futuro" come esempio di avanguardia
tecnica utilizzata da una forza rivoluzionaria.

La cosa interessante quindi è considerare Cybersyn come strumento politico,
con lo scopo specifico di aiutare a pianificare un'economia
decentralizzata.  Considerando quindi anche i vincoli della Guerra Fredda,
la nostra storia si svolge tra il Luglio 71 e il Settembre 73, con il
governo Allende messo sotto pressione sia dall'interno che dall'esterno,
cerchiamo di capire se Cybersyn possa veramente rappresentare quella terza
via tra libero mercato e pianificazione centralizzata (URSS)

### La Cibernetica come teoria del Controllo

#### Intro sulla situazione cilena

In 1970, the newly elected Popular Unity coalition government of Salvador
Allende found itself the coordinator of a messy jumble of factories, mines
and other workplaces that in some places had long been state-run, in others
were being freshly nationalized, while some were under worker occupation,
and others still remained under the control of their managers or owners.
The previous centrist administration of Christian Democrat Eduardo Frei had
already partially nationalized the copper mines, the producer of the
country’s largest export. Frei’s government had also developed a massive
public house-building program and significantly expanded public education,
all with substantial assistance from the United States. Washington was
fretful that if it did not pay for social reforms, it would witness social
revolution within the hemisphere that it viewed as its own. Thus,
substantial sections of Chile’s relatively small economy were already in
the public sector when the socialists took over, stretching the
bureaucracy’s management capability to its limit. A more efficient strategy
of coordination was required.

### Intro su Cybersyn

Il 29enne Ferdinando Flores, capo della CORFO (Chilean Production
Development Corporation), responsabile della gestione e coordinamento tra
stato Cileno e le compagnie statalizzate, rimase affascinato dagli scritti
di Stafford beer, studioso del neonato campo della cibernetica e consulente
di ricerca operativa per varie corporazioni, campo della matematica che
aveva interessato anche lo stesso Flores.

È  interessante notare, e forse dopo ci fermeremo un attimino in più, come
in particolare ai tempi della WWII divenne centrale una formulazione
matematica di vari problemi riguardanti l'allocazione ottimale delle
risorse, avendo ovviamente dei vincoli. Della cosidetta programmazione
Lineare furono due i "fondatori": l'olandese-americano T. C. Koopmans e il
sovietico Leonid Kantorovich, successimante in URSS la programmazione
lineare divenne centrale nei piani quinquennali ed in generale nella
pianificazione centralizzata. È interessante notare come questi metodi
siano poi centrali nella distrbuzione logistica delle grandi corporazioni
della vendita al dettaglio, oltre a tante altre similitudini interessanti
tra multinazionali ed URSS sono raccolte nel libro "The People's Republic
of Walmart", che fondamentalmente sostiene che corporazioni come Amazon e
Walmart non siano altro che delle economie centralizzatre di scala che sono
riuscite nell'obiettivo dell'Unione Sovietica, con tutti le
controindicazioni che conosciamo, ma forse avremo modo di approfondirlo
successivamente.

Nell'intervista di Medina a Flores questo riporta come fosse catturato
dalla coerenza filosofica dei sue scritti sul "management cybernetics"
potesse essere ben reciclata nella visione del socialismo democratico
antiburocratico di Allende, permettendo ai lavoratori di partecipare alla
gestione della produzione...

Il termine "cibernetica", specialmente affiancato da "management", a noi
oggi ci suona ridicolo, riporta a quelle buzzwords commerciali (anche se è
stato ampiamente surclassato da quantum) e mantiene un'aura che ci riporta
ad i nostri meravigliosi tecno-utopismi ingenui, o anzi a delle distopie di
serie B che si preferisca.  Ma in fondo il campo della "cibernetica" nei
primi anni '50 studiava come diversi sistemi- biologici, meccanici, sociali
- gestivano dinamicamente le comunicazioni, decisione e azione. Il primo
libro di Beer, CYbernetics and Management, uscito nel 1959, non faceva
nemmeno riferimento ai computer, anzi approfondiva in maniera maniacale il
funzionamento del sistema nervoso perifico tra varie specie biologiche, non
dobbiamo quindi pensare ad una specie di scienza della gestione
algoritmica, o a una teoria del taylorismo digitale.

Il campo della cibernetica iniziò a vedere la luce a cavallo della WWII,
quando Norbert Wiener iniziò a studiare sistemi antiaerei, in particolare
un sistema di feedback che permettesse all'utente umano di aggiustare il
tiro in modo automatico.  Fu rivelatoria l'utilità di progettare macchine
con sistemi di controllo lineare, "se faccio questo otterò quest'altro".

Riportiamo un pezzo su Wiener: As Richard Barbrook recounts in his 2007
history of the dawn of the computer age, Imaginary Futures, despite the
military engineering origins of the field, Wiener would go on to be
radicalized by the Cold War and the arms race, not only declaring that
scientists had a responsibility to refuse to participate in military
research, but asserting the need for a socialist interpretation of
cybernetics. “Large corporations depended upon a specialist caste of
bureaucrats to run their organisations,” Barbrook notes. “They ran the
managerial ‘Panopticon’ which ensured that employees obeyed the orders
imposed from above. They supervised the financing, manufacture, marketing
and distribution of the corporation’s products.” Wiener, and later Beer, on
the other hand, conceived of cybernetics as a mechanism of domination
avoidance: a major challenge that the managers of any sufficiently complex
system face, according to Beer, is that such systems are “indescribable in
detail.”

Dal canto suo Beer non credeva nella definzione comune di controllo come
dominio, visto come oppressivo e autoritario, anzi è anche fin troppo
ingenuo considerare ogni meccanismo sociale, biologico, econimico ecc. come
un sistema di feedback lineare, dove l'agente controllato è semplicemente
posto sotto stretta coercizione. Invece i suoi modelli spostavano lo
sgiardo dal controllo del mondo esterno a cercare di identificare le
condizioni di equilibrio tra i sottosistemi e di avere dei meccanismi di
feedback che ne aiutassero a raggiungere la stabilità. Per lui un sistema
può essere identificato da due dipoli (Deterministico/Probabilistico) e
(Semplice/Complesso).  Un sistema di comunicazione interno può quindi
facilitare l'auto-organizzazione delle componenti di un sistema, in
particolare un sistema di comunicazione ridondante, orizzontale e
multi-nodo (decentralizzato)...


Allende era attratto dall'idea di una direzione razionale dell'industria, e
sotto la raccomandazione di Flores, Beer venne assunto come consigliere.
Il suo lavoro sarebbe stato implementare una rete di comunicazione in tempo
reale, connessa da fabbrica a fabbrica, fino alla CORFO (simile al MISE),
in grado di comunicare sia verticalmente che orizzontalmente e eprmettendo
quindi di avere una risposta veloce dai vari nodi del sistema in tempi
brevi.  I dati raccolti sarebbero poi stati analizzati da un mainframe che
avrebbe prodotto delle proiezioni statistiche sul possibile comportamento
economico (es materie prime necessarie, qta prodotte ec.) In aggiunta il
sistema avrebbe implementato una simulazione informatica dell'intera
economia cilena, sistema soprannominato CHECO. Sfortunamente alla prima
visita a Santiago, Beer dovette confrontarsi con la dura realtà: il Cile
possedeva solo quattro mainframe IBM 360/50 acquistati nel '65, durante la
presidenza di Montalva, di proprietà National Computer Corporation (ECOM),
che ovviamente erano già usati da altri dipartimenti. A Beer venne affidato
un computer, che insomma su quattro non è così male, e il suo primo compito
fu di ingegnarsi per costruire una rete informatica, disseminata per tutto
il Cile, composta da un solo computer!

Ma ovviamente ciò che conta è costruire una rete, non le singole macchine
presenti ai nodi. Come soluzione, Beer decise di usare le telescriventi, o
telex, come possiamo fondalmente vedere come delle macchine da scrivere
collegate alla rete telefonica.  Beer pensò che come prototipo sarebbe
bastato collegare questi figliocci del telegrafo, al tempo in Cile più
diffusi dei telefoni, al mainframe IBM lasciato a Santiago, che avrebbe
continuato a fare da cervellone.  L'idea di Beer era creare un sistema di
feedback che collegasse i quadri di fabbrica e la CORFO: un gestore avrebbe
mandato le informazioni sullo stato della produzione attraverso le telex
alla National Computer Corporation. Lì un operatore avrebbe riportato
questi dati in delle schede performate che inserite nel mainframe, che
confrontando l'andamento con gli storici a dispodizioni, avrebbe cercato
delle anomalie attraverso un software statistico. Se fossero trovate delle
anomalie queste sarebbero state notificato indietro alla fabbrica e alla
CORFO. Quest'ultima avrebbe aspettato del tempo per intervenire, dando
quindi del margine di autonomia all'azienda, mettendo quindi anche il
governo in condizione il governo di adattarsi alle varie difficoltà e
necessità a mano che affioravano. In questo sistema di feedback circolare
Beer vedeva sia un'alternativa più morbida alla burocrazia sovietica, sia
un disincentivo a falsificare le effettive figure produttive come avveniva
continuamente in URSS.

Vediamo come questo sistema non sia esente da critiche: innanzitutto
bisogna considerare che il deficit strutturale portava comunque delle
relazioni di potere implicite: il fatto che esistesse una singola unità di
calcolo renedeva la rete dipendente dalla National Computer Corporation
(ECOM) e dalla CORFO. Oltretutto essendo solitamente la telex ad uso di una
singola figura dirigenziale nella fabbrica, questa non solo non avrebbe
dato alcun beneficio al singolo lavoratore, ma anzi avrebbe reso ancora
netta la condizione di alienazione della forza lavoro, rendendola succube
oltre che dei quadri dirigenziali anche delle direttive del ministero.
Questa visione va ridimensionata, dato che una parte della linfa del
movimento nato in Cile nella coalizione di Unità Popolare di Allende era
composto da movimenti sindacali e cooperative autogestite.  Oltretutto
bisogna considerare come la rete per funzionare chiedesse un'enorme
quantità di lavoro umano, e a ben leggere non rassomigliava ad una specie
di "piena automazione", anzi per l'appunto i dati che venivano macinati dal
software andavano prima inseriti in delle schede perforate, poi oltre il
monitor del mainframe la "decision room" era ricca di grafici che si
susseguivano, costituiti da cartonati pre-disegnati che rassomigliavano dei
flow chart ecc.  Insomma era comunque il 71, la NASA usava ancora
"calcolatori umani" almeno fino al '69

Lo stesso Allende, gli va riconosciuto, appena divenne familiare con il
funzionamento di Cybersyn, spinse Beer ad espandere le sue potenzialità
"decentralizzanti, partecipative ed anti-burocratiche". Il desiderio di
Allende per Projecto Synco che non fosse un'imitazione tecnocratica della
pianificazione economica simili-sovietica, ma anzi uno strumento
emancipatorio in mano ai lavoratori fino alla catena di montaggio per
prendere parte ai processi decisionali. Il suo entusiasmo impressionò Beer
che lo estese oltre le aziende nazionalizzate. 

Per spendere ancora due parole sulla situazione cilena, ancor prima della
elezione della coalizione di Unità Popolare, gli USA spesero milioni nella
propaganda contro la sinitra ed in support dei Democristiani. A seguito
della nazionalizzazione dell'industria del rame, supportata anche dai
Democristiani, gli USA non vedendo un indennizzo tagliarono le linee di
credito, le multinazionali a cui apparteveano le miniere cercano di
bloccarne l'export. I padroni delle fabbriche e delle terre si rivolsero ai
tribunali cercando di bloccare le riforme, la destra chiese apertamente ai
militari di prendere il controllo, opzione poi supportata dalla CIA.
Mentre un sostanziale aumento dei salari riuscì a diminuire la
disoccupazione e portò una crescita del reddito nazionale del 8%, questo
embargo de facto mise in difficoltà l'economia cilena e limitò la
disponibilità di beni di consumo.  La condizione politica andò a
radicalizzarsi, e questo portò delle notevoli difficoltà al gruppo di Beer.
I pochi ricercatori esperti in ricerca operativa dovettero studiare le
peculiarità di ogni singola azienda per comprendere quali indicatori il
software avrebbe dovuto traciare e quali no. Nonostante ciò il modello di
CHECO iniziò anche a tenere in considerazione fattori macroeconomici per
dettare la simulazione dell'economia cilena, come gli investimenti e
l'inflazione. Il team ad un certo punto si reso conto anche della
difficoltà nell'ottenere uno storico affidabile di informazioni, i dati
nell'industria mineraria tenevano traccia solo degli ultimi due anni,
nell'agricoltura erano pressoché inesistenti, in molte industrie gli
ingegneri erano resti a collaborare, vista anche una fedeltà politica
all'ancien régime.  Alla fine CHECO per simulare l'economia cilena
sfruttava solo i dati sull'inflazione, lo scambio di valuta estera e il
reddito nazionale, oltre ad alcuni modelli semplifcati specifici di alcuni
settori. 

Nel libro di Medina viene sottolineata più volte la propensione di molti
ingegneri a continuare a rispondere alla precedente gerarchia nella
fabbrica, anche se questa era stata nazionalizzata, ad esempio riferendo le
informazioni prima alla direzione, poi a manager o quadri di medio livello,
e successivamente ai responsabili della produzione.  Questo ovviamente è
stato un enorme ostacolo all'obiettivo, espresso sia da Beer che da
Allende, di rendere il sistema quanto possibile partecipativo,
decentralizzato e anti-burocratico, il ruolo dei lavoratori nel processo
decisionale diventava ovviamente trascurabile.  Oltretutto  dal libro di
Medina notiamo che proprio da una fascia di lavoratori altamente
specializzati, appunto ingegneri e direttori della produzione, venne
un'enorme resistenza durante il processo di modellazione delle fabbriche,
ma di crumiraggio in generale, quando era necessario per la CORFO conoscere
a quali metriche affidarsi, questo andava anche di passo con il fatto che
Beer fosse concentrato sulla ricerca operativa, materia assente dai corsi
universitari in Cile per cui quindi la formazione già scarseggiava.

Queste difficoltà comunque non impedirono a Cybersyn di dimostrare il suo
potenziale, permettendo, anche nella sua forma embrionale,a gruppi di
lavoratori e cooperativi di auto-organizzare la produzione e distribuzione
durante lo sciopero dei camionisiti del 15 Ottobre 1972, rilanciato dai
settori conservatori dell'industria e fiancheggiato dalla CIA, che avrebbe
altrimenti messo in ginocchio l'economia cilena e quindi il governo. 

### Lo sciopero del 72 e il ruolo di Cybersyn

Durante lo scioepro Cybersyn riuscì appunto a dimostrare il suo potenziale.
La rete permetteva al governo di sapere dovere la scarsità di un bene si
accentuava, dove si trovassero gli autotrasportari che non partecipavano al
boicottaggio, e in questo modo redigere la distribuzione per venire in
contro alle necessità. L'approccio non era verticale, dettato dai ministri
seduti a La Moneda, ma anzi i settori delle aziende nazionalizzate e delle
cooperative si organizzarono nei “cordónes industriales”, in modo da poter
coordinare mutualmente lo scambio di materie prime e carburante.

Breve nota sui cordones: The autonomous operation of these cordónes
mirrored forms of spontaneous worker and community self-direction that
appear to pop up regularly during times of revolutionary upheaval, or
otherwise at times of crisis or natural disaster, whether we call them
“councils,” “comités d’entreprises” (France), “soviets” (Russia),
“szovjeteket” (Hungary) or “shorai” (Iran). Liberal commentator Rebecca
Solnit describes in her social history of the extraordinary communities
that emerge at such extreme moments, A Paradise Built in Hell, how, far
from the chaotic, Hobbesian war of all against all of elite imagination, it
is calm, determined organization that on the whole prevails. She repeatedly
discovered how remarks by those attempting to survive through earthquakes,
great fires, epidemics, floods and even terrorist attacks that despite the
horrors experienced, reflect how truly alive, full of common purpose and
even joyful they felt. It is no wonder that a rich, long-lived stream of
libertarian socialist thought, running through the writings of the likes of
Rosa Luxemburg, Anton Pannekoek, and Paul Mattick emphasizes such
organization, such “councils,” as the foundation of the free society they
wish to build. The great challenge is the scaling-up of such democratic,
market-less organization. This is the distilled version of the economic
calculation debate: relatively flat hierarchies seem perfectly capable of
democratically coordinating production and distribution for a limited
number of goods and services, for a small number of people and over a
limited geography. But how could the myriad products needed by a modern,
national (or even global) economy—with its complex web of crisscrossing
supply chains, thousands of enterprises and millions of inhabitants
(billions, if we consider the global case)—be produced without vast,
metastasizing and inefficient bureaucracies? How are the interests of the
local production node integrated harmoniously with the interests of society
as a whole? What may be in the interest of a local enterprise may not be in
the interest of the country. What happened in Chile in October of 1972 may
not be the definitive answer to these questions, but it hints at some
possibilities.

Il 15 Ottobre, Flores si convise che avrebbero dovuto sfruttare ciò che
avevano imparato dallo sciopero per definire la struttura definitiva di
Cybersyn.  Il comando centrale restava inserito nel palazzo presidenziale,
connesso attraverso le telex a numerosi centri operativi specializzati nei
diversi settori.  Il governo avrebbe ricevuto ogni minuto un aggiornamento
dalle varie zone del paese, rispondendo alle richieste e mandando ordini.
La rete non doveva rimanere centralizzata, ma anzi permettere la
connessione tra i partecipanti, quando quindi un'azienda avrebbe avuto
bisogno di materiale o carburante la risposta sarebbe stata girata a chi
nelle vicinanze ne aveva a disposizione.  Senza eliminare quindi la
gerachia verticale, la rete avrebbe connesso il governo con le
organizzazioni orizzontali presenti nel paese.

Medina scrive: “The network offered a communications infrastructure to link
the revolution from above, led by Allende, to the revolution from below,
led by Chilean workers and members of grassroots organizations, and helped
coordinate the activities of both in a time of crisis.” She argues that
Cybersyn simply faded into the background, “as infrastructure often
does.”Il sistema non avrebbe quindi ordinato ai lavoratori cosa fare, ma
avrebbe permesso alle singole realtà radicali di gestire i propri bisogni.

La strategia di Flores ad ogni modo funzionò, le riserve di cibo si
mantennero a circa il 70% del normale, la distribuzione di materie prime
continuò normalmente fino al 95% del normale e di carburante al 90% per le
attività necessarie.  I report erano basati su dati raccolti nei tre giorni
precedenti, 15-17 Ottobre, mentre secondo Madina i report precedenti del
CORFO richiedevano 6 mesi per essere elaborati.  Per la fine del mese lo
sciopero era quasi concluso, senza essero riuscito a bloccare l'economia
cilena. In un intervista Beer riporta che un ministro gli riferì che senza
Cybersyn l'economia cilena sarebbe collassata il 17 Ottobre.


#### L'epilogo della carriera di Beer

The result inspired Beer to envision still-wider applications of
cybernetics to support worker participation. This former international
business consultant had moved in an almost anarcho-syndicalist direction
(anarcho-syndicalism is the political philosophy arguing for a
government-less society coordinated directly by workers through their trade
unions): “The basic answer of cybernetics to the question of how the system
should be organised is that it ought to organise itself.” Science and
technology could be tools used by workers to help democratically coordinate
society, from the bottom up, leaping over the
centralization/decentralization dichotomy. Instead of having engineers and
operations researchers craft the models of factories, programmers would be
under the direction of workers, embedding their deep knowledge of
production processes into the software. Instead of the Soviet model of
sending large quantities of data to a central command point, the network
would distribute, vertically and horizontally, only that amount of
information that was needed for decision making. For Beer, Medina writes,
Cybersyn offered “a new form of decentralised, adaptive control that
respected individual freedom without sacrificing the collective good.”


