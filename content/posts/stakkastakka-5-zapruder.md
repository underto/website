Title: stakkastakka #5 - Zapruder
Slug: stakkastakka-5-zapruder
Date: 2018-11-28 01:05:33
Tags: stakkastakka, zapruder

#### POSTCAST
<audio controls>
  <source src='https://stakkastakka.fugadalcontrollo.org/stakkastakka-5-zapruder.ogg' type='audio/mpeg'>
</audio>
<a href='https://stakkastakka.fugadalcontrollo.org/stakkastakka-5-zapruder.ogg'/>Puntata completa</a>

<br/>
#### Posta del cuore dell'Internet:
- [il sistema che predice il reato](https://www.repubblica.it/cronaca/2018/11/16/news/ecco_il_sistema_che_predice_il_reato_arrestato_un_ladro-211829383/)
- [BGP mitm](https://boingboing.net/2018/10/26/bgp-pop-mitm.html)
- [sicurezza dei
  bancomat](https://www.securityinfo.it/2018/11/16/hackerare-un-bancomat-bastano-20-minuti/)
  
#### APPROFONDIMENTO

Abbiamo parlato di [zapruder numero
45](http://storieinmovimento.org/2018/04/23/quarantacinquesimo-numero/)

