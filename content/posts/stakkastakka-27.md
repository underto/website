Title: stakkastakka #27
Slug: stakkastakka-27
Date: 2019-04-29 13:12
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/05/stakkastakka27.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/05/stakkastakka27.ogg'/>Puntata completa</a>

## Posta del cuore dell'internet

- [Amazon controlla la produttivita' delle persone in automatico e licenzia in base ad un algoritmo](https://www.businessinsider.com/amazon-system-automatically-fires-warehouse-workers-time-off-task-2019-4?IR=T)
- [Qualcuno sta spiando le vostre ovaie per misurare la produttivita'](https://d.repubblica.it/life/2019/04/26/news/app_sorveglianza_mestruale-4383334/)
- [Cambia il tuo fashion col machine learning : come generare toppe e certelloni che ingannano i sistemi di riconoscimento facciale](https://arxiv.org/abs/1904.08653)


### Eventi acari

- [call for papers hackmeeting](https://www.autistici.org/underscore/call-for-papers-hackit-2019-firenze-30-maggio-2-giugno.html)
- [brugole e merlelli](https://doityourtrash.noblogs.org/)
- [hackmeeting 0x16](https://hackmeeting.org/hackit19/)
- [connessioni caotiche](https://unit.abbiamoundominio.org/blog/2019-04-23-connessioni-caotiche-2019.html)