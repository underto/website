Title: stakkastakka #55 - Censura Dns e OONI
Slug: stakkastakka-55-censura-dns-ooni
Date: 2020-02-11 23:28:17
Tags: stakkastakka


### POSTCAST
Se notate dei buchi nell'audio lo sappiamo e mannaggiamo forte.
<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/02/stakkastakka55.censura-dns-e-ooni.ogg' type='audio/ogg'>
</audio>  
<a href='https://radioblackout.org/wp-content/uploads/2020/02/stakkastakka55.censura-dns-e-ooni.ogg'/>Puntata completa</a>
<br/><br/>



### Posta del ♥ dell'Internet

- Google Maps Hacks Performance  
[http://www.simonweckert.com/googlemapshacks.html](http://www.simonweckert.com/googlemapshacks.html)

- Google continua a fare grandi cappelle  
[https://twitter.com/jonoberheide/status/1224525738268905477](https://twitter.com/jonoberheide/status/1224525738268905477<Paste>)  
[https://www.theverge.com/2020/2/4/21122044/google-photos-privacy-breach-takeout-data-video-strangers](https://www.theverge.com/2020/2/4/21122044/google-photos-privacy-breach-takeout-data-video-strangers)

- Greenwald accuse stile Assange pero' in Brasile  
Il direttore del Intercept Greenwald, che a suo tempo aiuto' Edward Snowden
nel 2013 con l'importante leak fatto all'NSA, e' stato accusato ed e' sotto
processo a causa di una serie di leak avvenuti in Brasile che coinvolgono
il presidente Bolsonaro e il ministero della giustizia Moro.  
[https://www.valigiablu.it/brasile-bolsonaro-attacco-giornalisti/](https://www.valigiablu.it/brasile-bolsonaro-attacco-giornalisti/)


<br/>
### Approfondimento su DNS, censura, OONI (circa dal minuto 29)


- [symbolics.com](http://symbolics.com/museum/)
- [Open Observatory of Network Interference - OONI](https://ooni.org/)



