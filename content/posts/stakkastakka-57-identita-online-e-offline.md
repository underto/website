Title: stakkastakka #57 - Identita online e offline
Slug: stakkastakka-57-identita-online-e-offline
Date: 2020-02-27 14:43:05
Tags: stakkastakka


#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/02/stakkastakka-57.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/02/stakkastakka-57.ogg'/>Puntata completa</a>
<!--- <small> <a href="https://archive.org/details/stakkastakka-57">mirror1</a> <source src='https://archive.org/download/stakkastakka-57/stakkastakka-57.ogg' type='audio/ogg'></small> !-->
<br/>

## Posta del ♥ dell'Internet

1) storie di agenti russi che sii attaccano ai cavi sottomarini che fanno funzionare internet

- [thetimes - russian agent plunge to new ocean depths in ireland to crack transatlantic cables](https://www.thetimes.co.uk/article/russian-agents-plunge-to-new-ocean-depths-in-ireland-to-crack-transatlantic-cables-fnqsmgncz)
- [mappa dei cavi sottomarini che ci collegano all'internet](https://www.submarinecablemap.com/)

2) DDL 2394 - la grande ipocrisia della magistratura e del diritto di spionaggio verso chiunque sia ritenuto sospettabile

<blockquote>
Tutto è iniziato col decreto legislativo 216/2017 (in piena era
Renzi-Gentiloni). E oggi ci fermiamo a questa origine, per parlare nelle
successive puntate dei suoi sviluppi. Qui si è cominciato a parlare di
intercettazioni ad ampio raggio, di ‘captatori informatici’ (senza dire
chiaramente che cosa la legge intenda usando questo termine) e a stabilire che
possono essere sempre usati per tutti i reati di associazione a delinquere e
per attività terroristica; viceversa, per gli altri reati previsti dall’art.266
del Codice di Procedura penale, l’uso dei trojan e delle microspie
nell’abitazione privata dell’indagato è autorizzata solo se vi sono fondati
motivi che l’attività criminosa si svolga lì e l’autorizzazione ha un tempo
determinato.

Fermiamoci un attimo per contemplare l’ipocrisia di Stato.

Il legislatore sapeva o non sapeva nel 2017 che tutti i giovani procuratori
hanno ormai imparato, come tecnica investigativa da applicare contro un
cittadino di cui si sospetta qualcosa, ad aprire un fascicolo contro ignoti?
Dopo di che, la stessa persona può essere intercettata anche se non iscritta al
registro degli indagati, perché può essere ritenuto che la sua utenza possa
essere intercettata utilmente per l’accertamento di reati di cui si ha notizia
ma non si conosce il responsabile. Successivamente, lo stesso magistrato può
indagare lo stesso cittadino (dopo averlo intercettato per mesi in attesa del
reato) per associazione a delinquere, e per sei mesi non mandargli alcun avviso
di garanzia. Ma se anche poi indaga per un periodo più lungo e non lo avvisa,
quale è la sanzione? Nessuna. 
</blockquote>

- => [DDL 2394](https://www.sardegnaeliberta.it/photo/TestoC.2394GovernoapprovatodalSenato1.pdf)
- ==> [Intercettazioni: Il Pd usa il Coronavirus per varare la legge dello spionaggio generalizzato dei cittadini](https://www.sardegnaeliberta.it/intercettazioni-come-il-governo-e-il-parlamento-hanno-autorizzato-lo-spionaggio-generalizzato-dei-cittadini-1/)

3)  Internet of things Candle

una candela che si accende tramite un app, cosa mai potrebbe andare storto ?

- => [Internet of Things - candle](https://www.schneier.com/blog/archives/2020/02/internet_of_thi.html)

3) CryptoStory - Come CIA e servizi segreti tedeschi hanno spiato alleati e avversari vendendo macchine antispionaggio

*”It was the intelligence coup of the century,”   CIA report concludes. “Foreign governments were paying good money to the U.S. and West Germany for the privilege of having their most secret communications read by at least two (and possibly as many as five or six) foreign countries.”*

- news:en:ashigtonpost [‘The intelligence coup of the century’](https://www.washingtonpost.com/graphics/2020/world/national-security/cia-crypto-encryption-machines-espionage/)
- news:it:valigiablu [Come CIA e servizi segreti tedeschi hanno spiato vendendo macchine antispionaggio](https://www.valigiablu.it/cia-germania-spionaggio-alleati/)
- pdf:eng:archive [full story archived in pdf](https://archive.is/1w61P)

# Approfondimento 

## Gestione delle identità online e offline

![who-am-I](images/whoyouare.png)

La nostra vita online spesso assomiglia alla vita che abbiamo offline. Negli
ultimi 10 anni una spinta totale ci circonda e ci fa credere che l'unico modo
di esistere che abbiamo online sia quelle di chiamarci per il nostro nome e
cognome reale, ma questa realta' e' vera solo per chi ci crede. Esistono nei
mondi non troppo sotterranei del hacking e del attivismo un sacco di strumenti
per gestire l'identita' e l'anonimato ed in questa puntata andremo a scoprirne
alcuni per poi lasciare la curiosita' a chi vuole di cercare gli strumenti
migliori per ognuno, ma prima cerchiamo di decostruire la nostra identita'
cerchiamo di vedere che cosa e' successo negli ultimi 30 anni di internet con
un po' di storia e prenderemo pezzi di libri che ci possano raccontare che cosa
significava poter cambiare identita' ogni volta che volevamo.



### Book

*un estratto dal libro di Edward Snowden che rappresenta abbastanza bene l'immaginario nerd nella gestione delle identità*


<blockquote>
In un forum ho rivelato la mia età, ma non il mio nome, quello mai. Una delle
cose più belle di queste piattaforme era che non ero tenuto a svelare la mia
identità. Potevo essere chiunque. L’anonimato, tramite l’uso di pseudonimi,
garantiva l’equilibrio nei rapporti, correggendo eventuali discrepanze. Potevo
nascondermi dietro qualsiasi nome virtuale, o nym, come veniva chiamato,
diventando in un attimo la versione più alta e adulta di me stesso. Potevo
assumere tantissime identità. Ne approfittavo per fare le domande che reputavo
più ingenue, ogni volta usando un nome diverso. Le mie abilità informatiche
miglioravano così velocemente, che invece di essere fiero dei miei progressi,
mi vergognavo della mia precedente ignoranza e cercavo di prenderne le
distanze. Avrei voluto dissociare le mie identità. E criticare la stupidità di
[del mio alias precedente] squ33ker, quando aveva fatto quella domanda sulla
compatibilità del chipset, così tanto tempo fa (in realtà era stato solo il
mercoledì precedente).  Ovviamente anche in questo contesto così culturalmente
libero e collaborativo esisteva il senso di competizione, e accadeva anche che
qualcuno – di solito maschio, eterosessuale e sovreccitato – si alterasse per
motivi futili. Ma in assenza di nomi veri, le persone che dicevano di odiarti
non erano persone vere. Non sapevano niente di te, al di fuori di ciò di cui
discutevi e di come ne discutevi. Se venivi coinvolto in qualche lite, bastava
cambiare identità, e così avresti potuto addirittura inserirti nella
discussione, dando contro al vecchio te stesso come se fosse stato un estraneo.
Era un sollievo inimmaginabile.  Negli anni Novanta, però, Internet sarebbe
caduta vittima della più grande ingiustizia della storia digitale: il
tentativo, da parte della politica e delle aziende, di legare nel modo più
stretto possibile l’identità virtuale degli utenti con quella reale. Un tempo
qualsiasi ragazzino poteva dire su Internet la cosa più stupida senza doverne
rendere conto in futuro. In apparenza potrebbe non sembrare il contesto più
sano dove crescere, ma in realtà questo è l’unico contesto in cui ciò sia
davvero possibile. Il fatto che Internet, in origine, permettesse di
dissociarsi da se stessi, incoraggiava me e quelli della mia generazione a
cambiare le nostre opinioni, anche quelle che avevamo sostenuto con più
convinzione, invece di continuare a difenderle quando venivano messe in
discussione. La possibilità di reinventarsi ci permetteva di non chiuderci
sulle nostre posizioni per paura di arrecare danni alla reputazione. Gli errori
venivano puniti e immediatamente corretti, e questo permetteva a tutta la
comunità di avanzare. Per me, e per altri, era questa la libertà.  Immaginate
di potervi svegliare ogni mattina e di poter scegliere un nome nuovo e una
faccia nuova. Immaginate di poter scegliere una nuova voce e nuove parole con
cui parlare, come se il «pulsante di Internet» vi permettesse di resettare la
vostra vita. Nel nuovo millennio, Internet ha assunto scopi molto differenti:
incentiva l’attaccamento al ricordo, sostiene l’importanza di un’identità
coerente e promuove il conformismo ideologico. Ma allora, almeno per un certo
periodo, ci proteggeva, facendoci dimenticare le nostre trasgressioni e
perdonando i nostri peccati.  </blockquote>


### Link: alcuni link utili per comprendere come gestire le nostre identita' online

**Gendersec Zen** - Manuale della gestione delle identità e molto altro

- => [Gendersec::Zen: the art of making tech works for you](https://gendersec.tacticaltech.org/wiki/index.php/Complete_manual#Creating_and_managing_identities_online)

### Alcuni strumenti utili per gestire le nostre varie identita' o per non averne nessuna:

- [Fake Name generator](https://fakena.me)
- [Guerrilla Mail - Disposable Temporary E-Mail Address](https://www.guerrillamail.com/)
- [anonbox.net provides you free, completely anonymous one-time email address](https://anonbox.net/)
- [BugMeNot - find and share logins with each others](http://bugmenot.com)
- [me and my shadow - take control of your data](https://myshadow.org/)
- [TAILS - a live system that aims to preserve your privacy and anonymity ](https://tails.boum.org/)
- [Whonix - Anonymize Everything You Do Online](https://www.whonix.org/)
- [Qubes OS - A reasonably secure operating system](https://www.qubes-os.org/)



