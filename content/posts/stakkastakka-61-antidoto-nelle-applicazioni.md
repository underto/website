Title: Stakkastakka #61 - Antidoto nelle applicazioni
Slug: antidoto-nelle-applicazioni
Date: 2020-03-25 13:07:01
Tags: stakkastakka

<audio controls>
<source src='https://radioblackout.org/wp-content/uploads/2020/03/stakkastakka-61.mp3' type='audio/mp3'>
</audio>
<audio controls><a href='https://radioblackout.org/wp-content/uploads/2020/03/stakkastakka-61.mp3'/>Puntata completa</a>

## La statistica a servizio della qualunque

Abbiamo mandato un contributo della nostra trasmissione sorella **le dita
nella presa** su radio onda rossa.

Partendo da una statistica presentata dalla regione Lombardia che tende a
dimostrare l'ancora eccessiva mobilità della popolazione nonostante i
decreti, facciamo quattro conti per vedere quanto le conclusioni tratte
siano veramente supportate dai dati statistici, e in generale quanta
validità abbiano quei dati rispetto alla domanda posta.  Il nostro
risultato permette sostanzialmente di giungere alla conclusione.

<audio controls>
<source src='https://archive.org/download/ror-200322_2105-2255-ldnp/ror-200322_2105-2255-ldnp-statistica.ogg'type='audio/mp3'> </audio> <audio controls>
<a href='https://archive.org/download/ror-200322_2105-2255-ldnp/ror-200322_2105-2255-ldnp-statistica.ogg'/>La statistica al servizio della qualunque completa</a>

## L'antidoto delle applicazioni

- [Facebook moderazione coronavirus](https://www.valigiablu.it/facebook-moderazione-coronavirus/)
- [Coronavirus dati tecnologia](https://www.valigiablu.it/coronavirus-dati-tecnologia)
- [Emergenza e diritti fondamentali](https://www.ilpost.it/carloblengino/2020/03/19/emergenze-e-diritti-fondamentali/)
- [Task Force identificazione](https://www.ilfattoquotidiano.it/in-edicola/articoli/2020/03/19/tracciamento-degli-utenti-ce-la-task-force-ma-per-lidentificazione-serve-una-legge/5741602/)
- [Coronavirus app](https://www.wired.it/internet/web/2020/03/24/coronavirus-app-contact-tracing/)

