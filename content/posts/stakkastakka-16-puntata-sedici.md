Title: stakkastakka #16 Puntata Sedici
Slug: stakkastakka-16-puntata-sedici
Date: 2019-02-25 17:32:07
Tags: stakkastakka


#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/02/stakkastakka-16-puntata-sedici.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/02/stakkastakka-16-puntata-sedici.ogg'/>Puntata completa</a>


#### Posta del cuore dell'INTERNET

- [Google mette i microfoni nei prodotti senza dirlo](https://securityaffairs.co/wordpress/81516/digital-id/google-nest-microphone.html)
- [Alla silicon valley piace il riscaldamento globale](https://unearthed.greenpeace.org/2018/09/19/businesseurope-cbi-eu-climate-paris-agreement/)
- [Il sistemista di 'el chapo'](https://www.rollingstone.com/culture/culture-features/el-chapo-christian-rodriquez-secure-cell-phone-audio-evidence-777117/)
- [Firefox sotto attacco](https://www.eff.org/deeplinks/2019/02/cyber-mercenary-groups-shouldnt-be-trusted-your-browser-or-anywhere-else)
