Title: stakkastakka #39 - Credito Sociale
Slug: stakkastakka-39-creditosociale
Date: 2019-09-16 17:23:10
Tags: stakkastakka


#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/09/39.credito-sociale.stakkastakka.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/09/39.credito-sociale.stakkastakka.ogg'/>Puntata completa</a>
<br/>

## Posta del ♥ dell'Internet

- [camion IBM dei super hacker che viaggia  e sconfigge gli attacchi informatici](https://www.repubblica.it/tecnologia/sicurezza/2019/09/12/news/a_bordo_del_super_camion_che_ci_difendera_dagli_attacchi_informatici-235836469/)
- [Primo hack dallo spazio](https://www.techtimes.com/articles/245115/20190825/first-crime-in-space-nasa-astronaut-anne-mcclain-accused-of-hacking-bank-accounts-from-iss.htm)
- [simjacker](https://www.adaptivemobile.com/blog/simjacker-next-generation-spying-over-mobile)
- [israele spia la casa bianca](https://www.politico.com/story/2019/09/12/israel-white-house-spying-devices-1491351)
- [l'angolo AI a pedali](https://www.theverge.com/2019/8/14/20805676/engineer-ai-artificial-intelligence-startup-app-development-outsourcing-humans)