Title: stakkastakka #19 mastodon
Slug: stakkastakka-19-mastodon
Date: 2019-03-04 18:44:54
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source
  src='https://radioblackout.org/wp-content/uploads/2019/03/stakkastakka-19-mastodon.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/03/stakkastakka-19-mastodon.ogg'/>Puntata completa</a>


#### Posta del cuore dell'INTERNET

- Operai cinesi con il
  [cappellino](https://www.technologyreview.com/the-download/611052/with-brain-scanning-hats-china-signals-it-has-no-interest-in-workers-privacy/).
- Elon che fa [cose](https://motherboard.vice.com/en_us/article/qveedq/elon-musk-steps-down-from-open-source-ai-group).
- Report annuale dei servizi segreti al parlamento. Veramente? Si! La
  trovate [qua](http://www.sicurezzanazionale.gov.it/sisr.nsf/Relazione-2018.pdf).

#### Approfondimento mastodon

Abbiamo fatto una diretta con gli amici di [bida.im](http://bida.im/)
parlando della [loro istanza](http://mastodon.bida.im/). Ma era tutta una
scusa per lanciare la *nostra istanza* su
[mastodon.cisti.org](https://mastodon.cisti.org/)! Iscrivetevi in nuovo
nodo del fediverso!
