Title: stakkastakka #44 - Tails
Slug: stakkastakka-44-tails
Date: 2019-10-28 23:07:16
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/10/stakkastakka-44-tails.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/10/stakkastakka-44-tails.ogg'/>Puntata completa</a>
<br/>

## Posta del ♥ dell'Internet

- [Zuckemberg sculacciato dalla Ocasio-Cortez](https://www.theguardian.com/technology/2019/oct/23/mark-zuckerberg-alexandria-ocasio-cortez-facebook-cambridge-analytica)

- [Da influencer a mercatari](https://twitter.com/mbrennanchina/status/1185598598815174657?)

- [Tsunami Democratic](https://techcrunch.com/2019/10/17/catalan-separatists-have-tooled-up-with-a-decentralized-app-for-civil-disobedience/)

- [Phishing degli assistenti domestici](https://arstechnica.com/information-technology/2019/10/alexa-and-google-home-abused-to-eavesdrop-and-phish-passwords/)

- [La bolla di WeWork](https://linkmoltobelli.substack.com/?no_cover=true)

- [NordVPN owned](https://techcrunch.com/2019/10/21/nordvpn-confirms-it-was-hacked/)

## Tails

Abbiamo parlato un po' del nostro sistema amnesico preferito. [Tails](https://tails.boum.org/index.it.html)!

