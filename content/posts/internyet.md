Title: stakkastakka #1 - Internyet
Slug: internyet
Tags: stakkastakka, internyet
Date: 2018-11-10 10:20

![cybertonia](images/jazz-robot.img)

#### POSTCAST
<audio controls>
  <source src='https://stakkastakka.fugadalcontrollo.org/stakka-stakka-1-22-10-18.mp3' type='audio/mpeg'>
</audio>
<a href='https://stakkastakka.fugadalcontrollo.org/stakka-stakka-1-22-10-18.mp3'/>Puntata completa</a>

<audio controls>
  <source src='https://stakkastakka.fugadalcontrollo.org/stakka-stakka-Internyet.mp3' type='audio/mpeg'>
</audio>
<a href='https://stakkastakka.fugadalcontrollo.org/stakka-stakka-Internyet.mp3'/>Approfondimento Internyet</a>

<br/>
#### NEWS, abbiamo parlato di:
- gli sviluppatori di uno dei ransomware piu' redditizi del 2018 (GandCrab)
[hanno rilasciato le chiavi di
decifratura](https://www.bleepingcomputer.com/news/security/gandcrab-devs-release-decryption-keys-for-syrian-victims/) delle vittime siriane
- [0day](https://www.zdnet.com/article/zero-day-in-popular-jquery-plugin-actively-exploited-for-at-least-three-years/) dentro un plugin jquery
- [una fabbrica di false recensioni per prodotti
  amazon](https://www.theguardian.com/money/2018/oct/20/facebook-fake-amazon-review-factories-uncovered-which-investigation)
- [riconoscimento facciale di
  animali](https://www.ilpost.it/2018/10/21/riconoscimento-facciale-animali/)

#### APPROFONDIMENTO
- [Internyet](https://www.ilpost.it/2018/10/21/riconoscimento-facciale-animali/)



