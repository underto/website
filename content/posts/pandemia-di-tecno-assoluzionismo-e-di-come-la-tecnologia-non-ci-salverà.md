Title: Pandemia. Di tecno-assoluzionismo e di come la tecnologia non ci salverà
Slug: pandemia-di-tecno-assoluzionismo-e-di-come-la-tecnologia-non-ci-salverà
Date: 2020-05-07 14:34:25


In questi mesi abbiamo dovuto lavorare molto di più: sembra buffo, visto che
eravamo a casa. Nel frattempo tante cose sono successe su internet e, con
l'avvicinarsi di un "dopo" incerto, vorremmo dire la nostra sperando che queste
riflessioni servano ad aprire una discussione. O almeno chiarire un poco alcune
vicende fondamentali di queste lunghe giornate.

Se c'è qualcosa che l'hacking ci ha insegnato è che la tecnologia è un terreno
di dominio e come tale va scardinato. Oggi la soluzione tecnica viene
sbandierata come panacea, semplice, accessibile, ma è pura propaganda.

La tecnica asservita al potere economico e politico sembra avere il diritto di
parlare di tutto, proponendo soluzioni che vanno dalla sanità, alla formazione,
alla gestione dei flussi di persone, ma parla sempre da una posizione
disincarnata, senza l'esperienza diretta delle problematiche e delle risorse
fondamentali da preservare. Questo tipo di approccio alla tecnica è per noi
tossico e l'hacking continuerà a voler sollevare queste contraddizioni con i
suoi strumenti.

## La premessa

Le istituzioni hanno scelto di avere fin da subito un atteggiamento
paternalista, con l'obiettivo di scaricare il pesante impatto del virus sulla
"popolazione indisciplinata" che non rispetta i dettami della quarantena.  Come
se la limitatissima capacità di intervento non fosse dovuta alle condizioni
critiche della sanità pubblica, stremata da anni di tagli, aziendalizzazioni su
base regionale, privatizzazioni, accorpamenti e scelte sbagliate.

Invece di assumersi le responsabilità di una strategia che ha privilegiato i
grandi centri nevralgici ospedalieri (grandi centri che da soli sotto pressione
non avrebbero retto) a discapito di una sanità diffusa sul territorio, nelle
comunicazioni ufficiali abbiamo assistito sgomenti all'elezione quotidiana di
nemici pubblici, inviduati in categorie finora impensabili: il runner, il
genitore con passeggino, il ciclista. 

Si sono lasciate sole le persone anziane nelle RSA o nelle loro case,
incrociando le dita perché non si presentassero negli ospedali, nascoste sotto
a un grande tappeto mentre il problema del contenimento del virus veniva
trasformato, con atteggiamento ottuso e punitivo, nel contenimento/isolamento
della popolazione.

In cima a tutto questo spesso si è preferito dar seguito alla volontà di
confindustria e di molte aziende di tenere aperti i luoghi di lavoro a tutti i
costi, senza procedure di protezione verificate ed efficaci, sviando
l'attenzione grazie ad un'insostenibile retorica di guerra (il personale
medico-sanitario come "eroi in prima linea") a giustificazione dell'esistenza
della carne da cannone in corsia e nelle fabbriche così come in trincea. Il
costo del sacrificio è caduto sulle persone più vulnerabili.

## Tecno-buzzword e Covid-19

La prassi sanitaria è stata opportunamente confusa con la norma legislativa,
attivando spesso un completo nonsense. Si è operato uno spostamento del
problema: dal contenimento del virus si è passati ad un sistema di infrazioni
da sanzionare, traslando così l'attenzione su quest'ultimo (il runner come arma
di distrazione di massa).  Di nuovo, si prende un problema complesso e lo si
riduce a uno collegato, ma più semplice, illudendosi e lasciando intendere che
il secondo sia equivalente e risolva il primo. Si fa strada il sillogismo per
cui contrastare il virus significa sorvegliare le persone che zuzzurellano in
qua e in là.  A questo si aggiunge la più classica politica delle buzzword
(parole tecniche, usate spesso in modo improprio per impressionare/influenzare
chi ascolta con termini "alla moda"). Ci troviamo di fronte a un proliferare di
"tecno-buzzword": buzzword che presentano strumenti tecnologici come panacea di
tutti i mali. Questa è una forma di tecno-soluzionismo che non risolve
realmente i problemi e apre a una serie di ulteriori contraddizioni e
criticitá.

## Tecno-buzzword 1: drone

Prendiamo un esempio: i droni.  L'Enac ha dovuto effettuare una serie di
concessioni sull'utilizzo di questi giocattolini, perché i sindaci italiani più
"smart" avevano iniziato ad autorizzarne l'uso in autonomia.  L'ente
ministeriale ha dunque in fretta e furia liberato l'uso di droni nei controlli
legati alle ordinanze covid, prima fino al 3 aprile, poi nella paranoia
generalizzata dell'apocalittico weekend di pasquetta, l'ha rinnovata fino al 18
maggio.

L'utilizzo propagandistico, per quanto inquietante, di questi oggetti volanti è
chiaro: l'autorizzazione prevede la presenza di chi pilota sul posto, non in
remoto, e la guida a linea di vista; i droni possono solo segnalare la presenza
di persone da controllare, il materiale video registrato deve essere rimosso
dopo il controllo e le infrazioni contestate sul momento. I controlli con droni
sono stati effettuati in luoghi semi deserti, fluviali o marittimi. A conti
fatti sembra più un divertissement per non annoiarsi in quarantena, visto che
praticamente un vigile con un binocolo da 20 euro avrebbe avuto lo stesso
effetto.  Il salto di qualità avverrebbe con la guida da remoto e la
registrazione ed elaborazione automatica delle immagini. Ricordiamocelo bene e
non lasciamoci distrarre quando inevitabilmente qualcuno cercherà di far
passare inosservato qualche "temporaneo aggiustamento alla normativa", magari
per far fronte ad un'altra "emergenza".  Attualmente però i droni funzionano
solo da generico spauracchio, utile a terrorizzare le persone, o da spot per
sindaci sceriffi col pallino dell'innovazione in cerca di visibilità e
consenso. 

## Tecno-buzzword 2: app di tracciamento contatti

Altro esempio: la app per tracciare i contatti.

Non ci sembra interessante disquisire se il tracciamento avvenga con la
collaborazione degli operatori telefonici, o come sembra essere stato scelto,
con il bluetooth e le app sviluppate da google ed apple.  La pre-condizione per
questa fantomatica fase due è il ripristino di una sanità pubblica di
prossimità, colpevomente smantellata da scelte di governo bipartisan e risorsa
imprescindibile per contenere la pandemia. Servono assunzioni, formazione,
presìdi medici diffusi sui territori, capacità di analisi: eppure non se ne
sente parlare. Se non ci sono abbastanza laboratori d'analisi per fare un
tampone a una persona con la polmonite, se non c'è nessuna persona in grado di
andarglielo a fare a casa, se non ci si prende cura delle persone
capillarmente, a poco serviranno uno smartphone e una app. Al massimo una app
segnerebbe un numeretto, ma a leggere quel numeretto poi chi ci sarebbe?  Più
chiaramente: è come costruire una casa a partire dalla porta, rifinirla di
tutto punto con gli intarsi e lo spioncino a fotocamera, e poi chiamare tutti e
dire: "Ecco qui: la porta è fatta secondo standard europei, è molto innovativa
e rispettosissima della vostra privacy". È normale che poi ti si chieda: "Ok,
ma c'è solo la porta. La casa dov'è?" La app trasla ancora il problema da una
cosa difficile a una facile: in due settimane la app la fai.  Poi, tossendo, la
apri sul cellulare e scopri che non ha proprietà curative.

Affrontare il discorso in termini di privacy e di tecnologie, è esattamente il
terreno su cui ci vogliono portare, per attuare il giochino dello spostamento
del problema e puntarci contro un'altra ennesima grande arma di distrazione di
massa.

Non ci sono dubbi: preservare l'intimità digitale e la privacy è uno dei campi
di lotta di quest'epoca, il problema del controllo è connaturato al sistema in
cui viviamo e la raccolta massiva di dati è uno degli elementi fondamentali su
cui si basano abusi e repressione.  Immediatamente però, alle attuali
condizioni e per contrastare la diffusione del virus qui e ora, un'app è
semplicemente inutile e chi utilizza le buzzword app o innovazione sta
colpevolmente contribuendo a sviare l'attenzione da quelle che sono le reali
problematiche e a deresponsabilizzare chi ha realmente causato questa
catastrofe sanitaria.

## Tecno-buzzword 3: DAD - didattica a distanza

La didattica, nell'impossibilità di utilizzare piattaforme pubbliche, si è
frastagliata in mille rivoli e strumenti, pesando sulla buona volontà,
intraprendenza e connessione del corpo docente che, lasciato alla propria
iniziativa individuale, si getta a spegnere l'incendio che divampa grazie al
vuoto sociale. Navigando tra un google, zoom, teams, whatsapp, skype, facebook,
youtube, nella consapevolezza che l'esperienza didattica non sia riducibile
esclusivamente all'erogazione di contenuti.

Al netto di tutti i ragionamenti vi è la (banale?) constatazione che la
didattica a distanza non può essere sostitutiva e considerata equivalente della
didattica in presenza, sopratutto per la fascia di età 6-18 e che il motivo per
cui è stata imposta sono le carenze strutturali delle scuole che,
disorganizzate e sovraffollate, non permettono la didattica in aula
opportunamente distanziati.

Quindi si torna di nuovo alla questione principale: i problemi materiali si
spostano nel digitale, ma il digitale non può risolverli.

La scuola, nel vuoto del pensiero e delle risorse strategiche, è stata di fatto
consegnata in toto alle grosse piattaforme commerciali.  Ancora una volta, il
meccanismo è il solito: di fronte a una scuola trasformata in azienda, svilita,
dove mancano i soldi anche per il sapone, che andrebbe ripensata e
riorganizzata con affetto, ci si affida al presunto potere taumaturgico della
tecnologia. Non si può pensare che questa scelta non avrà ripercussioni sul
futuro. Né si può pensare che sia una scelta ovvia ed automatica, con buona
pace di tutti i discorsi sul free software nella pubblica amministrazione, che
si fanno da praticamente 20 anni.

Salvo poi scoprire che la tecnologia non è così accessibile, ma è invece
ulteriore fonte di diseguaglianza sociale. Perchè possiamo fare finta che non
sia vero che molte persone facciano teledidattica con i giga del proprio
cellulare, che il territorio italiano sia fatto di paesini sperduti e
nient'affatto connessi, che sfavillanti e velocissimi computer non siano
affatto in ogni casa, però, per l'appunto, stiamo facendo finta.

Quella che era già una tendenza problematica (una scuola fatta di didattica
frontale e di valutazioni basate sulla quantificazione) rischia ora di
diventare la norma perché "siamo in emergenza". L'emergenza di oggi porta al
pettine i nodi problematici della società che abitiamo.  Lo stato di crisi è
strutturale e rende evidenti vulnerabilità preesistenti che non si possono
risolvere normando l'emergenza ma solo in un processo di profondo cambiamento.

## Una tecno-buzzword non ci salverá

Amiamo gli enigmi e non ci spaventano le complessità dei problemi. Quello che
temiamo sono le false piste e gli specchietti per le allodole.  Ciò che stiamo
vedendo, e subendo, in questi giorni, non è altro che l'esasperata
manifestazione di una serie di nodi che vengono al pettine e nessuna bacchetta
magica smart basterà a scioglierli.

Quando la politica parla di tecnologia, spesso lo fa per sviare l'attenzione
dalle ingiustizie e problematiche sociali a cui ci chiede di rassegnarci.
Consapevoli che ogni piccolo spazio di libertà sacrificato non verrà restituito
ma dovrà essere duramente riconquistato, quando la parola chiave è "emergenza"
è ancora piu' importante svelare i meccanismi nascosti e leggere oltre la
propaganda.

Dobbiamo mantenere la concentrazione, scrollarci di dosso il ruolo di gregge e
ritrovare quello di comunità pensante, ricordarci ogni buzzword che è stata
utilizzata sulla nostra pelle, scartarla e continuare a guardare dritto
davanti, al cuore del problema.


- Av.A.Na. Avvisi Ai Naviganti - Roma
- Ifdo Hacklab - Firenze
- HacklabBo - Bologna
- underscore `_TO*` hacklab - Torino 
- Unit hacklab - Milano
- Associazione Culturale "Verde Binario" - Cosenza
- Freaknet Medialab Poetry Hacklab - Palazzolo Acreide (Siracusa)
- MSAck Hacklab - Napoli
- Fablab Buridda - Genova 
- ChthuluLab - Milano 
- e altre individualità hacker e cyberpunk
