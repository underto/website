Title: Hackmeeting 2021 - Call For Contributions
Slug: hackmeeting-2021-call-for-contributions
Date: 2021-08-11 10:57:00

__da [it.hackmeeting.org](https://it.hackmeeting.org)__

A 500 anni di sfruttamento delle Americhe, 300 di carbone, 27 anni di Internet, 21 dalle rivolte NoGlobal di Seattle, 20 dall’omicidio di Carlo Giuliani al G8 di Genova, 8 anni nell’era post Snowden, 1 anno e mezzo nella pandemia Covid, hackmeeting vi chiama: accorrete, o papere iconoclaste, acarə sediziosə, gatte furastiche, e altre creature mutanti e non, il 3/4/5 settembre 2021 alla Nuova Casa del Popolo di Ponticelli “La Casona” a Malalbergo (BO).

In questo emisfero, in questi mesi di clausura oltre che per comunicare abbiamo usato le tecnologie digitali per lavorare e consumare come mai prima d’ora. Nel mondo “a portata di click” si nota meno, ma la produzione industriale e il consumo di massa non si sono mai fermati, continuando a capillarizzare la distruzione ambientale, il lavoro alienato e la disgregazione sociale. Una infrastruttura e un’organizzazione logistica che dettano la forma al contenuto ed al contenitore, a noi e all’ambiente. La “storia” della merce e dell’informazione che ci viene recapitata direttamente alla porta di casa è sempre piu’ disarticolata e i processi che la generano e la fanno muovere sono frammentati in una miriade di passaggi di cui è sempre più difficile cogliere l’origine e lo sviluppo. Questi processi si muovono attraverso interazioni e automazioni digitali che ci offrono il mondo come “un servizio”, alienandoci come esseri umani. Quest’anno all’hackmeeting vorremmo (anche) investigare questo aspetto, comprenderlo per provare a trasformarlo, immaginandoci un rapporto tra infrastruttura, tecnologie -digitali e non-, e società che sia solidale, autogestito, piu vicino ai territori e a chi li abita.

Nella nostra esperienza e pratica di critica e riappropriazione delle tecnologie e delle tecniche, ogni granello di consapevolezza è prezioso. Dal rapporto con la terra e gli esseri che ci vivono, al caos dei saperi e delle relazioni che attraversano la rete, i tasselli mancanti alla “minima manutenzione” del pianeta Terra sono tanti, e tante sono le comunità e gli ambiti che li esplorano e se li scambiano. La curiosità è un ingranaggio collettivo: se hai scoperto uno di questi tasselli e te la senti di condividerlo ti invitiamo a venircelo a raccontare ad Hackmeeting.

Se vuoi proporre un seminario, iscriviti alla lista
[https://www.autistici.org/mailman/listinfo/hackmeeting](https://www.autistici.org/mailman/listinfo/hackmeeting) e invia un messaggio con oggetto "[talk] titolo_del_talk" con queste informazioni:

- Durata (un multiplo di 30 minuti, massimo due ore)
- Eventuali esigenze di giorno/ora
- Breve Spiegazione, eventuali link e riferimenti utili
- Nickname
- Lingua
- Necessità di proiettore
- Disponibilità a farti registrare (solo audio)
- Altre necessità

Ci saranno tre spazi coperti, dotati di amplificazione audio e videoproiettore.

Se pensi che la tua presentazione non abbia bisogno di tempi cosi lunghi, puoi proporre un “ten minute talk” di massimo 10 minuti.

Questi talk verranno tenuti nello spazio piu capiente a fine giornata; ci sarà una persona che ti avviserà quando stai per eccedere il tempo massimo. Puoi proporre il tuo talk in lista inviando una mail con soggetto "[ten] titolo_del_talk" oppure direttamente in loco.

Se invece vuoi condividere le tue scoperte e curiosità in modo ancora piu informale e caotico, potrai sistemarti con i tuoi ciappini, sverzillatori, ammennicoli ed altre carabattole sui tavoli collettivi del “LAN space”. Troverai curiosità morbosa, corrente alternata e rete via cavo (portati una presa multipla, del cavo di rete e quel che vuoi trovare).

Hai una domanda estemporanea o una timidezza? Scrivici a hackmeeting2021@totallynot.science


