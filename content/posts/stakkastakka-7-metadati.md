Title: stakkastakka #7 - Metadati
Slug: stakkastakka-7-metadati
Date: 2018-12-03 01:33:52
Tags: stakkastakka, metadati

#### POSTCAST
<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2018/12/stakka-stakka-7-03-12-18.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2018/12/stakka-stakka-7-03-12-18.ogg'/>Puntata completa</a>

<br/>

#### NEWS, abbiamo parlato di:

- [Googlers vs Google](https://medium.com/@googlersagainstdragonfly/we-are-google-employees-google-must-drop-dragonfly-4c8a30c5e5eb)
- [Progetto Dragon Fly di Google](https://theintercept.com/2018/11/29/google-china-censored-search/)
- [Chiara che brucia semafori con Sergio](https://www.quattroruote.it/news/cronaca/2018/11/28/guida_autonoma_torino_il_prototipo_di_livello_3_brucia_il_rosso_in_diretta.html)

#### Il trabiccolo della settimana:

Nuova rubrica di stakkastakka in cui presentiamo un cazzillo, aggeggio,
affare... Insomma un coso!

Questa settimana abbiamo parlato di [redshift](http://jonls.dk/redshift/).
Uno strumento che vi permette di tenere al sicuro le due palle umide che
avete attaccate alla scatola cranica.

Se volete approfondire l'argomento ecco un ottimo [punto di partenza](https://justgetflux.com/research.html)

#### APPROFONDIMENTO

Abbiamo parlato di metadati, quelle informazioni che caratterizzano le
vostre comunicazioni e descrivono con chi avete parlato.

Rifletteteci: a volte non è necessario sapere cosa avete detto, ma basta
anche solo sapere con chi avete parlato.



