Title: stakkastakka #52 - Delfini Killer sul Nilo
Slug: stakkastakka-52-delfini-killer-sul-nilo
Date: 2020-01-15 16:42:48
Tags: stakkastakka

#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka-52-delfini-killer.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka-52-delfini-killer.ogg'/>Puntata completa</a>
<br/>

## L'occhio sul Nilo

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka-52-bits-occhio-sul-nilo.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka-52-bits-occhio-sul-nilo.ogg'/>Approfondimento CyberSyn</a>
<br/>

Racconto di una [ricerca](https://research.checkpoint.com/2019/the-eye-on-the-nile/) di contro-spionaggio indipendente egiziano. [Diretta dal CCCC](https://media.ccc.de/v/36c3-10561-the_eye_on_the_nile).

## Delfini Agenti del Kgb

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka-52-bits-delfini-agenti-kgb.ogg' type='audio/ogg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2020/01/stakkastakka-52-bits-delfini-agenti-kgb.ogg'/>Approfondimento CyberSyn</a>
<br/>

[Storie di guerra fredda](https://www.military.com/off-duty/iran-may-have-fleet-communist-killer-dolphins.html).
