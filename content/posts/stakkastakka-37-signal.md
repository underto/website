Title: stakkastakka #37 - Signal
Slug: stakkastakka-36-signal
Date: 2019-07-15 17:23:10
Tags: stakkastakka


#### POSTCAST

<audio controls>
  <source src='https://radioblackout.org/wp-content/uploads/2019/07/36.signal.stakkastakka.15jul.ogg' type='audio/mpeg'>
</audio>
<a href='https://radioblackout.org/wp-content/uploads/2019/07/36.signal.stakkastakka.15jul.ogg'/>Puntata completa</a>


## Posta del cuore dell'Internet
- [google home manda gli audio a terzi](https://thenextweb.com/google/2019/07/10/google-contractors-are-secretly-listening-to-your-assistant-recordings/)
- [alexa stringe accordi con il sistema nazionale inglese](https://www.ilpost.it/2019/07/11/alexa-sistema-sanitario-nazionale/)
- [multa a fb](https://www.nytimes.com/2019/07/12/technology/facebook-ftc-fine.html)



## Approfondimento - Signal 

- [https://www.pindrop.com/blog/audit-of-signal-protocol-finds-it-secure-and-trustworthy/](https://www.pindrop.com/blog/audit-of-signal-protocol-finds-it-secure-and-trustworthy/)
- [A Formal Security Analysis of the Signal Messaging Protocol](https://eprint.iacr.org/2016/1013.pdf)
- [http://sec.cs.ucl.ac.uk/ace_csr/seminars_calendar/seminar_details/article/post_compromise_security_and_the_signal_protocol/](http://sec.cs.ucl.ac.uk/ace_csr/seminars_calendar/seminar_details/article/post_compromise_security_and_the_signal_protocol/)
- [https://proprivacy.com/guides/signal-private-messenger](https://proprivacy.com/guides/signal-private-messenger)
- [https://moxie.org/](moxie.org)
