#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'underto'
SITENAME = u'_TO * hacklab'
SITEURL = 'https://autistici.org/underscore'
#SITEURL = ''

PATH = 'content'
TIMEZONE = 'Europe/Rome'
DEFAULT_LANG = u'it'
#THEME = '../pelican-themes/monospace'
#THEME = '../pelican-themes/basic'
#THEME = '../pelican-themes/mg'
#THEME = '../pelican-themes/nest'
#THEME = '../pelican-themes/pelican-sober'
#THEME = '../pelican-themes/sundown'
THEME = 'themes/underscore'

CSS_FILE = 'style.css'
# Feed generation is usually not desired when developing
FEED_ATOM = u'atom.xml'
FEED_RSS = u'rss.xml'
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_CATEGORY = 'posts'
DISPLAY_CATEGORIES_ON_MENU = True

TYPOGRIFY = False

PHOTO_LIBRARY='./content/images/'
PHOTO_EXIF_REMOVE_GPS = True
PHOTO_GALLERY = (1240, 768, 95)
PHOTO_ARTICLE = (1240, 768, 95)
PHOTO_THUMB = (900, 600, 95)

PLUGIN_PATHS = ['pelican-plugins/']
PLUGINS = ['photos', 'related_posts']

SITEMAP = dict([('format', 'xml')])

PIWIK_URL = 'stats.autistici.org'
PIWIK_SITE_ID = '954'

# Blogroll
#LINKS = (
#	('Autistici/Inventati', 'https://autistici.org/'),
#)
#PELICAN_COMMENT_SYSTEM = True
#PELICAN_COMMENT_SYSTEM_IDENTICON_DATA = ('author',)

STATIC_PATHS = ['images', 'pages' ]

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = False
