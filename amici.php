<!DOCTYPE html>
<html lang="en">
<head>
  <title>_TO * hacklab</title>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="https://autistici.org/underscore/theme/css/style.css" type="text/css" />
  <link href="https://autistici.org/underscore/hackit/" type="application/rss+xml" rel="alternate" title="amici acari" />
</head>

<body>

    <div id="navbar">
      <h1 class='title'>_TO * hacklab</h1>
      <div>
        <a href="https://autistici.org/underscore/"><img src="https://autistici.org/underscore/theme/logo.png" class='logo'/></a>
        <a href="https://autistici.org/underscore/">home</a> | 
        <a href="https://www.autistici.org/underscore/tag/stakkastakka.html">stakkastakka</a> | 
        <a href="https://autistici.org/underscore/amici.php">amici</a> |
        <a href="https://autistici.org/underscore/pages/eventi.html">eventi</a> |
        <a href="https://autistici.org/underscore/atom.xml">feed</a> |
      </div>
    </div> 

    <p>Ci sentiamo parte della comunità acara italiana che da venti anni
      autoorganizza annualmente l'<a href='https://hackmeeting.org'>hackmeeting</a>.</p>

<h1 class='title'>amici</h1>
<section id="article-list">
<?php
  $items = json_decode(file_get_contents('hackit/feed.json'));
  foreach($items as $item) {
    if ($item->feed_title === '_TO * hacklab') continue;
    echo "<div class='article-item'>";
    echo "<span class='date'>[" . $item->date . "]</span> ";
    echo "<a class='tag' href='#'>" . $item->feed_title . "</a>";
    echo "<a href='$item->link' rel='bookmark'>$item->title</a></div>";
  }
?>
</section>
</body>
</html>
